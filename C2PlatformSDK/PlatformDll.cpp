/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 12/03/2019  18:50 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"

#include <iostream>

#include "PlatformSDK.h"

#define USE_SDK_EXPORT_API

#if defined(USE_SDK_EXPORT_API)
	#define SDK_PLATFORM_API __declspec(dllexport)
#else 
	#define SDK_PLATFORM_API __declspec(dllimport)
#endif

static bool bIsInitialized = false;
static C2::PlatformSDK* pPlatformSDK = nullptr;

extern "C"
{
	SDK_PLATFORM_API bool InitializeSDK()
	{
		if(!bIsInitialized)
		{
			//Do init.
			bIsInitialized = true;
			
			return true;
		}
		else
		{
			//Ignor the second initializetion.
			return false;
		}
		
		return false;
	}

	SDK_PLATFORM_API void ShutDownSDK()
	{
		delete pPlatformSDK;
		pPlatformSDK = nullptr;
	}
	
	SDK_PLATFORM_API C2::IPlatformSDK* GetPlatformSDK()
	{
		if (!pPlatformSDK)
		{
			pPlatformSDK = new C2::PlatformSDK();
			return pPlatformSDK;
		}
		else
		{
			return nullptr;
		}
	}
}

