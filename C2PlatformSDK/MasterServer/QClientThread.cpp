/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 11/05/2019  16:41 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "QClientThread.h"

#include <QtCore/QCoreApplication>

#include "NetSerialization/Packet.pb.h"

namespace C2
{
	QClientThread::QClientThread()
	{
	}

	QClientThread::~QClientThread()
	{
	}

	void QClientThread::Update()
	{
		int argc = 0;
		QCoreApplication app(argc, 0);

		m_pSocket = new QTcpSocket(this);

		connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(OnReadReady()));
		connect(m_pSocket, SIGNAL(disconnected()), this, SLOT(OnDisconnected()));
		connect(m_pSocket, SIGNAL(connected()), this, SLOT(OnConneted()));
		connect(m_pSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(OnSocketError(QAbstractSocket::SocketError)), Qt::DirectConnection);

		m_pSocket->connectToHost(m_ip, m_port);

		m_pSocket->waitForConnected();

		app.exec();
	}
	void QClientThread::OnConneted()
	{
		for (auto& it : m_listener)
		{
			it->OnConnected(true);
		}
	}
	void QClientThread::OnDisconnected()
	{
		for (auto& it : m_listener)
		{
			it->OnDisconnect();
		}

		m_pSocket->deleteLater();
	}
	void QClientThread::OnReadReady()
	{
		while (m_pSocket->bytesAvailable() > 0)
		{
			if (m_ParsingTry == 5)
			{
				m_ParsingTry = 0;
				m_bComplexeData = false;
				printf("Parsing full failed... Disconnecting\n");
				m_pSocket->deleteLater();
			}

			if (!m_bComplexeData)
				m_pBufferData = m_pSocket->readAll();
			else
				m_pBufferData.append(m_pSocket->readAll());

			MasterServer::Packet packet;

			if (packet.ParseFromArray(m_pBufferData.data(), m_pBufferData.size()))
			{
				for (auto& it : m_listener)
				{
					it->OnIncomingMessage(&packet);
				}

				m_ParsingTry = 0;
				m_bComplexeData = false;
			}
			else
			{
				printf("Parse error... Trying to more...\n");
				m_ParsingTry++;
				m_bComplexeData = true;
				OnReadReady();
			}
		}
	}

	void QClientThread::OnSocketError(QAbstractSocket::SocketError error)
	{
		switch (error)
		{
		case QAbstractSocket::ConnectionRefusedError:
		{
			break;
		}
		case QAbstractSocket::RemoteHostClosedError:
			break;
		case QAbstractSocket::HostNotFoundError:
			break;
		case QAbstractSocket::SocketAccessError:
		{
			break;
		}
		case QAbstractSocket::SocketResourceError:
			break;
		case QAbstractSocket::SocketTimeoutError:
		{
			for (auto& it : m_listener)
			{
				it->OnConnected(false);
			}
			break;
		}
		case QAbstractSocket::DatagramTooLargeError:
			break;
		case QAbstractSocket::NetworkError:
			break;
		case QAbstractSocket::AddressInUseError:
			break;
		case QAbstractSocket::SocketAddressNotAvailableError:
			break;
		case QAbstractSocket::UnsupportedSocketOperationError:
			break;
		case QAbstractSocket::UnfinishedSocketOperationError:
			break;
		case QAbstractSocket::ProxyAuthenticationRequiredError:
			break;
		case QAbstractSocket::SslHandshakeFailedError:
			break;
		case QAbstractSocket::ProxyConnectionRefusedError:
			break;
		case QAbstractSocket::ProxyConnectionClosedError:
			break;
		case QAbstractSocket::ProxyConnectionTimeoutError:
			break;
		case QAbstractSocket::ProxyNotFoundError:
			break;
		case QAbstractSocket::ProxyProtocolError:
			break;
		case QAbstractSocket::OperationError:
			break;
		case QAbstractSocket::SslInternalError:
			break;
		case QAbstractSocket::SslInvalidUserDataError:
			break;
		case QAbstractSocket::TemporaryError:
			break;
		case QAbstractSocket::UnknownSocketError:
			break;
		default:
			break;
		}

		printf("[QSocket] Connection error<%u>\n", (int)error);
	}

	void QClientThread::ReceiveConnectInfo(QString ip, qint32 port)
	{
		m_ip = ip;
		m_port = port;
	}


	void QClientThread::SendData(const MasterServer::Packet& data)
	{
		size_t packetSize = data.ByteSizeLong();
		char* pBuffer = (char*)malloc(packetSize);

		if (data.SerializeToArray(pBuffer, packetSize))
		{
			m_pSocket->flush();
			m_pSocket->write(pBuffer, packetSize);

			while (m_pSocket->bytesToWrite() > 0)
			{
				if (!m_pSocket->waitForBytesWritten())
				{
					printf("waitForBytesWritten error...\n");
				}
			}
		}
	}

	void QClientThread::ReceiveEventListener(QList<IMasterServerListener*> data)
	{
		m_listener = data;
	}
}
