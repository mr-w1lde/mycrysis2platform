/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 05/04/2019  22:54 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _MASTERSERVER_API_H__
#define _MASTERSERVER_API_H__


#include <C2PlatfromSDK/MasterServer/IMasterServer.h>
#include <vector>

#include <qtcpserver.h>
#include <qtcpsocket.h>

#include <qthread>

#include <qlist.h>

namespace C2
{
	class CMasterServer 
		: public QObject
		, public IMasterServer
	{
		Q_OBJECT

	public:
		CMasterServer() {}
		~CMasterServer() {}
	public:
		// IMasterServer
		virtual bool ConnectToMS(const char* address, int port) override;
		virtual bool SendMessageToServer(const MasterServer::Packet& packet) override;
		virtual void CloseConnection() override;


		virtual void AddListener(IMasterServerListener* listener) override;
		virtual void RemoveListener(IMasterServerListener* listener) override;
		//~IMasterServer
	signals:
		void SendEventListener(QList<IMasterServerListener*>);
		void SendConnectInfo(QString, qint32);
		void SendData(const MasterServer::Packet&);
	private:
		QList<IMasterServerListener*> m_listeners;
		bool m_bSignalsAreRegistered = false;
		QTcpSocket* m_pServerSocket = nullptr;
	};
}

#endif