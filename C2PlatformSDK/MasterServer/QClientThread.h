/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 11/05/2019  16:41 : Created by Stanislav Migunov.
*************************************************************************/

#ifndef _Q_MS_CLIENT_THREAD_H__
#define _Q_MS_CLIENT_THREAD_H__

#include <qthread.h>

#include <qtcpserver.h>
#include <qtcpsocket.h>

#include <C2PlatfromSDK/MasterServer/IMasterServer.h>

#include <qlist.h>

namespace C2
{
	class QClientThread : public QObject
	{
		Q_OBJECT

	public:
		QClientThread();
		~QClientThread();
	public slots:
		void Update();
	public slots:
		void OnConneted();
		void OnDisconnected();
		void OnReadReady();
		void OnSocketError(QAbstractSocket::SocketError error);
	public slots:
		void ReceiveEventListener(QList<IMasterServerListener*> data);
		void ReceiveConnectInfo(QString ip, qint32 port);
		void SendData(const MasterServer::Packet& data);

	private:
		QTcpSocket*		m_pSocket;
		QByteArray		m_pBufferData;
		QList<IMasterServerListener*> m_listener;
		QString m_ip;
		qint32 m_port;
	private:
		int m_ParsingTry = 0;
		bool m_bComplexeData = false;
	};
}

#endif