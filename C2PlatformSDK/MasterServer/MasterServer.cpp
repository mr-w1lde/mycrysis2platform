/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 14/03/2019  16:51 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "MasterServer.h"

#include "NetSerialization/Packet.pb.h"
#include <thread>

#include "QClientThread.h"

#include <QtCore/QCoreApplication>

bool C2::CMasterServer::ConnectToMS(const char * address, int port)
{
	SConnectionInfo info(address, port);

	QThread* pThread = new QThread();
	QClientThread* pClientThread = new QClientThread();

	pClientThread->moveToThread(pThread);

	connect(pThread, SIGNAL(started()), pClientThread, SLOT(Update()));
	connect(this, SIGNAL(SendEventListener(QList<IMasterServerListener*>)), pClientThread, SLOT(ReceiveEventListener(QList<IMasterServerListener*>)), Qt::DirectConnection);
	connect(this, SIGNAL(SendConnectInfo(QString, qint32)), pClientThread, SLOT(ReceiveConnectInfo(QString, qint32)), Qt::DirectConnection);
	connect(this, SIGNAL(GetServerSocket()), pClientThread, SLOT(EventOnSendServerSocket()), Qt::DirectConnection);
	connect(this, SIGNAL(SendData(const MasterServer::Packet&)), pClientThread, SLOT(SendData(const MasterServer::Packet&)), Qt::DirectConnection);
	connect(pClientThread, SIGNAL(SendServerSocket(QTcpSocket*)), this, SLOT(ReceiveServerSocket(QTcpSocket*)), Qt::DirectConnection);
	
	m_bSignalsAreRegistered = true;

	emit SendEventListener(m_listeners);
	emit SendConnectInfo(address, port);

	pThread->start();

	return true;
}

bool C2::CMasterServer::SendMessageToServer(const MasterServer::Packet& packet)
{
	emit SendData(packet);
	return true;
}


void C2::CMasterServer::CloseConnection()
{
	
}

void C2::CMasterServer::AddListener(IMasterServerListener * listener)
{
	m_listeners.push_back(listener);

	if(m_bSignalsAreRegistered)
		emit SendEventListener(m_listeners);
}

void C2::CMasterServer::RemoveListener(IMasterServerListener * listener)
{
	util::find_and_erase(m_listeners, listener);

	if (m_bSignalsAreRegistered)
		emit SendEventListener(m_listeners);
}


