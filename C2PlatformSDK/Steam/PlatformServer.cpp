/*************************************************************************
/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  14:40 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformServer.h"

namespace C2
{
	PlatformServer::PlatformServer(bool bIsLocal, int port, bool bIsDedicated)
		: m_bLocal(bIsLocal)
		, m_port(port)
	{
		if (!SteamGameServer_Init(0, 8766, port, 27016, eServerModeAuthentication, GAME_VERSION))
		{
			//TODO ERROR : SteamGameServer_Init call failed!
			return;
		}

		if (ISteamGameServer* pGameServer = SteamGameServer())
		{
			pGameServer->SetKeyValue("version", GAME_VERSION);

			//char str[CRYFILE_MAX_PATH];
			//CryGetCurrentDirectory(CRYFILE_MAX_PATH, str);

			pGameServer->SetProduct(PRODUCT_NAME);
			pGameServer->SetGameDescription(PRODUCT_DESC);

			pGameServer->SetDedicatedServer(bIsDedicated);

			pGameServer->LogOnAnonymous();

			pGameServer->EnableHeartbeats(true);
		}
	}
	PlatformServer::~PlatformServer()
	{
		if (ISteamGameServer* pGameServer = SteamGameServer())
		{
			pGameServer->EnableHeartbeats(false);

			// Disconnect from the steam servers
			pGameServer->LogOff();
		}

		// release our reference to the steam client library
		SteamGameServer_Shutdown();
	}
	uint64 PlatformServer::GetIdentifier() const
	{
		if (ISteamGameServer* pGameServer = SteamGameServer())
			return pGameServer->GetSteamID().ConvertToUint64();

		return 0;
	}
	uint32 PlatformServer::GetPublicIP() const
	{
		if (ISteamGameServer* pGameServer = SteamGameServer())
			return pGameServer->GetPublicIP();

		return 0;
	}
	const char * PlatformServer::GetPublicIPString() const
	{
		int32 publicIP = GetPublicIP();

		const int NBYTES = 4;
		uint8 octet[NBYTES];
		char* ipAddressFinal = new char[15];
		for (int i = 0; i < NBYTES; i++)
		{
			octet[i] = publicIP >> (i * 8);
		}
		sprintf(ipAddressFinal, "%d.%d.%d.%d", octet[3], octet[2], octet[1], octet[0]);

		string sIP = string(ipAddressFinal);
		delete[] ipAddressFinal;

		return sIP.c_str();
	}
	uint16 PlatformServer::GetPort() const
	{
		return m_port;
	}
	bool PlatformServer::IsLocal() const
	{
		return m_bLocal;
	}
	bool PlatformServer::AuthenticateUser(uint32 clientIP, char * szAuthData, int authDataLength, uint64 & userId)
	{
		if (ISteamGameServer* pGameServer = SteamGameServer())
		{
			CSteamID steamUserId;
			if (pGameServer->SendUserConnectAndAuthenticate(clientIP, szAuthData, authDataLength, &steamUserId))
			{
				userId = steamUserId.ConvertToUint64();
				return true;
			}
			else
			{
				//TODO WARNING: Steam authentication failure!
				return false;
			}
		}
		else
		{
			//TODO WARNING: No game server found, returning authentication failure!
			return false;
		}

		return false;
	}
	void PlatformServer::SendUserDisconnect(uint64 userId)
	{
		if (ISteamGameServer* pGameServer = SteamGameServer())
			pGameServer->SendUserDisconnect(CSteamID(userId));
	}
}