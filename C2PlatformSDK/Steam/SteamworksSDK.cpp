/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:23 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"

#include "PlatformUser.h"
#include "PlatformMatchmaking.h"
#include "PlatformServer.h"
#include "PlatformNetworking.h"

#include "SteamworksSDK.h"


namespace C2
{
	SteamworksSDK::SteamworksSDK()
		: m_callbackGameOverlayActivated(this, &SteamworksSDK::OnGameOverlayActivated)
		, m_pServer(nullptr)
		, m_authTicketHandle(k_HAuthTicketInvalid)
	{
		m_isInitialized = SteamAPI_Init();

		if (!m_isInitialized)
		{
			throw std::runtime_error("SteamApi wasn't init...");
			return;
		}


		m_pMatchmaking = std::make_shared<PlatformMatchmaking>();
		m_pNetworking = std::make_shared<PlatformNetworking>();
	}
	SteamworksSDK::~SteamworksSDK()
	{
		SteamAPI_Shutdown();
	}
	int SteamworksSDK::GetBuildIdentifier() const
	{
		if (ISteamApps* pSteamApps = SteamApps())
		{
			return pSteamApps->GetAppBuildId();
		}
		return 0;
	}

	void SteamworksSDK::AddListener(IListener & listener)
	{
		m_listeners.push_back(&listener);
	}

	void SteamworksSDK::RemoveListener(IListener & listener)
	{
		util::find_and_erase(m_listeners, &listener);
	}

	IPlatformUser * SteamworksSDK::GetLocalClient()
	{
		if (ISteamUser* pSteamUser = SteamUser())
		{
			return TryGetUser(pSteamUser->GetSteamID());
		}

		return nullptr;
	}

	IPlatformUser * SteamworksSDK::GetUserById(uint64 id)
	{
		return TryGetUser(id);
	}

	void SteamworksSDK::UpdateFriendList()
	{
	}

	bool SteamworksSDK::IsFriendWith(uint64 otherUserId) const
	{
		if (auto* pSteamFriends = SteamFriends())
		{
			return pSteamFriends->HasFriend(otherUserId, k_EFriendFlagImmediate);
		}

		return false;
	}

	IPlatformServer * SteamworksSDK::CreateServer(bool bLocal)
	{
		if (m_pServer == nullptr)
		{
			m_pServer = std::make_shared<PlatformServer>(bLocal, 64800, false);
		}

		return m_pServer.get();
	}

	IPlatformServer * SteamworksSDK::GetLocalServer() const
	{
		return m_pServer.get();
	}

	IPlatformMatchmaking * SteamworksSDK::GetMatchmaking() const
	{
		return m_pMatchmaking.get();
	}

	IPlatformNetworking * SteamworksSDK::GetNetworking() const
	{
		return m_pNetworking.get();
	}

	bool SteamworksSDK::OwnsApplication(ApplicationIdentifier id) const
	{
		if (ISteamApps* pSteamApps = SteamApps())
		{
			return pSteamApps->BIsSubscribedApp(id);
		}

		return false;
	}

	ApplicationIdentifier SteamworksSDK::GetApplicationIdentifier() const
	{
		if (ISteamUtils* pSteamUtils = SteamUtils())
		{
			return pSteamUtils->GetAppID();
		}

		return 0;
	}

	bool SteamworksSDK::OpenDialog(EDialog dialog) const
	{
		switch (dialog)
		{
		case EDialog::Friends:
			return OpenDialog("Friends");
		case EDialog::Community:
			return OpenDialog("Community");
		case EDialog::Players:
			return OpenDialog("Players");
		case EDialog::Settings:
			return OpenDialog("Settings");
		case EDialog::Group:
			return OpenDialog("OfficialGameGroup");
		case EDialog::Achievements:
			return OpenDialog("Achievements");
		}

		return false;
	}

	bool SteamworksSDK::OpenDialog(const char * szPage) const
	{
		if (ISteamFriends* pSteamFriends = SteamFriends())
		{
			pSteamFriends->ActivateGameOverlay(szPage);
			return true;
		}

		return false;
	}

	bool SteamworksSDK::OpenBrowser(const char * szURL) const
	{
		if (ISteamFriends* pSteamFriends = SteamFriends())
		{
			pSteamFriends->ActivateGameOverlayToWebPage(szURL);
			return true;
		}

		return false;
	}

	bool SteamworksSDK::CanOpenPurchaseOverlay() const
	{
		if (ISteamUtils* pSteamUtils = SteamUtils())
		{
			return pSteamUtils->IsOverlayEnabled();
		}

		return false;
	}

	bool SteamworksSDK::GetAuthToken(std::string & tokenOut, int & issuerId)
	{
		char rgchToken[2048];
		uint32 unTokenLen = 0;
		ISteamUser* pSteamUser = SteamUser();
		if (!pSteamUser)
		{
			//Warning: Steam user service not available
			return false;
		}
		m_authTicketHandle = (uint32)pSteamUser->GetAuthSessionTicket(rgchToken, sizeof(rgchToken), &unTokenLen);

		const char hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		for (int i = 0; i < unTokenLen; ++i)
		{
			char const byte = rgchToken[i];

			tokenOut += hex_chars[(byte & 0xF0) >> 4];
			tokenOut += hex_chars[(byte & 0x0F) >> 0];
		}

		return true;
	}

	EConnectionStatus SteamworksSDK::GetConnectionStatus() const
	{
		return EConnectionStatus::Connected;
	}


	void SteamworksSDK::OnGameOverlayActivated(GameOverlayActivated_t* x)
	{
		for (const auto& it : m_listeners)
		{
			it->OnOverlayActivated(x->m_bActive);
		}
	}

	IPlatformUser * SteamworksSDK::TryGetUser(CSteamID id)
	{
		uint64 cleanId = id.ConvertToUint64();

		for (const std::unique_ptr<IPlatformUser>& pUser : m_users)
		{
			if (pUser->GetIdentifier() == cleanId)
			{
				return pUser.get();
			}
		}

		m_users.emplace_back(util::make_unique<PlatformUser>(id));
		return m_users.back().get();

		return nullptr;
	}
}