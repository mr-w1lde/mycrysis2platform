/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformNetworking.h"

namespace C2
{
	PlatformNetworking::PlatformNetworking()
		: m_callbackP2PSessionRequest(this, &PlatformNetworking::OnSessionRequest)
		, m_callbackP2PSessionConnectFail(this, &PlatformNetworking::OnSessionConnectFail)
	{
	}
	bool PlatformNetworking::SendPacket(uint64 remoteUser, void * pData, uint32 dataLength)
	{
		ISteamNetworking* pSteamNetworking = GetSteamNetworking();
		if (!pSteamNetworking)
		{
			return false;
		}
		return pSteamNetworking->SendP2PPacket(remoteUser, pData, dataLength, k_EP2PSendReliable);
	}
	bool PlatformNetworking::CloseSession(uint64 remoteUser)
	{
		ISteamNetworking* pSteamNetworking = GetSteamNetworking();
		if (!pSteamNetworking)
		{
			return false;
		}
		return pSteamNetworking->CloseP2PSessionWithUser(remoteUser);
	}
	bool PlatformNetworking::IsPacketAvailable(uint32 * pPacketSizeOut) const
	{
		ISteamNetworking* pSteamNetworking = GetSteamNetworking();
		if (!pSteamNetworking)
		{
			return false;
		}
		return pSteamNetworking->IsP2PPacketAvailable(pPacketSizeOut);
	}
	bool PlatformNetworking::ReadPacket(void * pDest, uint32 destLength, uint32 * pMessageSizeOut, uint64 * pRemoteIdOut)
	{
		ISteamNetworking* pSteamNetworking = GetSteamNetworking();
		if (!pSteamNetworking)
		{
			return false;
		}
		return pSteamNetworking->ReadP2PPacket(pDest, destLength, pMessageSizeOut, reinterpret_cast<CSteamID*>(pRemoteIdOut));
	}
	ISteamNetworking * PlatformNetworking::GetSteamNetworking() const
	{
#ifdef DEDICATED_SERVER
		return SteamGameServerNetworking();
#else
		return SteamNetworking();
#endif
	}

	// Steam callbacks
	void PlatformNetworking::OnSessionRequest(P2PSessionRequest_t* pP2PSessionRequest)
	{
		// always accept packets
		ISteamNetworking* pSteamNetworking = GetSteamNetworking();
		if (!pSteamNetworking)
		{
			return;
		}
		pSteamNetworking->AcceptP2PSessionWithUser(pP2PSessionRequest->m_steamIDRemote);
	}

	void PlatformNetworking::OnSessionConnectFail(P2PSessionConnectFail_t* pP2PSessionConnectFail)
	{
	}
}