/*************************************************************************
/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  14:40 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformMatchmaking.h"

#include "PlatformLobby.h"

namespace C2
{
	PlatformMatchmaking::PlatformMatchmaking()
		: m_callbackJoinRequested(this, &PlatformMatchmaking::OnJoinRequested)
		, m_callbackGameServerChangeRequested(this, &PlatformMatchmaking::OnGameServerChangeRequested)
		, m_callbackInvited(this, &PlatformMatchmaking::OnInvited)
	{
	}
	void PlatformMatchmaking::AddListener(IListener & listener)
	{
		m_listeners.push_back(&listener);
	}
	void PlatformMatchmaking::RemoveListener(IListener & listener)
	{
		util::find_and_erase(m_listeners, &listener);
	}
	void PlatformMatchmaking::CreateLobby(IPlatformUserLobby::EVisbility visibility, int maxMemberCount)
	{
		if (ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking())
		{
			SteamAPICall_t hSteamAPICall = pSteamMatchmaking->CreateLobby((ELobbyType)visibility, maxMemberCount);
			m_callResultCreateLobby.Set(hSteamAPICall, this, &PlatformMatchmaking::OnCreateLobby);
		}
	}
	IPlatformUserLobby * PlatformMatchmaking::GetUserLobby(uint64 user) const
	{
		for (const std::unique_ptr<PlatformUserLobby>& pLobby : m_lobbies)
		{
			int numMembers = pLobby->GetNumMembers();
			for (int i = 0; i < numMembers; i++)
			{
				CSteamID memberId = pLobby->GetMemberAtIndex(i);
				if (memberId == user)
				{
					return pLobby.get();
				}
			}
		}

		return nullptr;
	}
	IPlatformUserLobby * PlatformMatchmaking::GetLobbyById(uint64 id)
	{
		return TryGetLobby(id);
	}
	void PlatformMatchmaking::QueryLobbies()
	{
		string versionStr = GAME_VERSION;

		// Always query lobbies for this version only
		//TODO LOG: Looking for lobby with version: %s
#ifdef RELEASE
		AddLobbyQueryFilterString("version", versionString.c_str(), IUserLobby::EComparison::Equal);
#endif
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			SteamAPICall_t hSteamAPICall = pSteamMatchmaking->RequestLobbyList();
			m_callResultLobbyMatchList.Set(hSteamAPICall, this, &PlatformMatchmaking::OnLobbyMatchList);
		}
		else
		{
			//TODO WARNING: Steam matchmaking service not available
			LobbyMatchList_t temp;
			temp.m_nLobbiesMatching = 0;
			OnLobbyMatchList(&temp, true);
		}
	}
	uint64 PlatformMatchmaking::GetQueriedLobbyIdByIndex(int index) const
	{
		CSteamID result = k_steamIDNil;
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			result = pSteamMatchmaking->GetLobbyByIndex(index);
		}
		return result.ConvertToUint64();
	}
	void PlatformMatchmaking::AddLobbyQueryFilterString(const char * szKey, const char * szValue, IPlatformUserLobby::EComparison comparison)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			pSteamMatchmaking->AddRequestLobbyListStringFilter(szKey, szValue, static_cast<ELobbyComparison>(comparison));
		}
		else
		{
			//TODO WARNING: Steam matchmaking service not available
			return;
		}
	}
	void PlatformMatchmaking::JoinLobby(uint64 lobbyId)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			SteamAPICall_t hSteamAPICall = pSteamMatchmaking->JoinLobby(lobbyId);
			m_callResultLobbyEntered.Set(hSteamAPICall, this, &PlatformMatchmaking::OnJoin);
		}
		else
		{
			//TODO WARNING: Steam matchmaking service not available
			LobbyEnter_t temp;
			temp.m_EChatRoomEnterResponse = k_EChatRoomEnterResponseError;
			temp.m_ulSteamIDLobby = k_steamIDNil.ConvertToUint64();
			OnJoin(&temp, true);
		}
	}
	void PlatformMatchmaking::InviteFriendToLobby(uint64 lobbyId, uint64 friendId)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			bool result = pSteamMatchmaking->InviteUserToLobby(lobbyId, friendId);

			if (!result)
			{
				//TODO WARNGIN: Can't send invite to lobby <%llu> to friend <%llu>;
			}
			else
			{
				//TODO LOG: Friend <%llu> invited to lobby <%llu>"
			}
		}
	}

	void PlatformMatchmaking::OnLobbyRemoved(PlatformUserLobby* pLobby)
	{
		for (auto it = m_lobbies.begin(); it != m_lobbies.end(); ++it)
		{
			if (it->get() == pLobby)
			{
				m_lobbies.erase(it);
				return;
			}
		}
	}

	PlatformUserLobby* PlatformMatchmaking::TryGetLobby(uint64 id)
	{
		for (const std::unique_ptr<PlatformUserLobby>& pLobby : m_lobbies)
		{
			if (pLobby->GetIdentifier() == id)
			{
				return pLobby.get();
			}
		}

		m_lobbies.emplace_back(util::make_unique<PlatformUserLobby>(id));
		return m_lobbies.back().get();
	}

	// Steam callbacks
	void PlatformMatchmaking::OnCreateLobby(LobbyCreated_t* pResult, bool bIOFailure)
	{
		if (pResult->m_eResult == k_EResultOK)
		{
			string versionString = GAME_VERSION;

			PlatformUserLobby* pLobby = TryGetLobby(pResult->m_ulSteamIDLobby);
			pLobby->SetData("version", versionString.c_str());

			//TODO LOG: Creating lobby with game version <%s>

			for (IListener* pListener : m_listeners)
			{
				pListener->OnCreatedLobby(pLobby);
				pListener->OnJoinedLobby(pLobby);
			}
		}
	}

	void PlatformMatchmaking::OnLobbyMatchList(LobbyMatchList_t* pLobbyMatchList, bool bIOFailure)
	{
		for (IListener* pListener : m_listeners)
		{
			pListener->OnLobbyQueryComplete(pLobbyMatchList->m_nLobbiesMatching);
		}
	}

	void PlatformMatchmaking::OnJoinRequested(GameLobbyJoinRequested_t* pCallback)
	{
		//TODO LOG: Accept invite from friend <%llu> to lobby <%llu> by steam dialog
		JoinLobby(pCallback->m_steamIDLobby.ConvertToUint64());
	}

	void PlatformMatchmaking::OnJoin(LobbyEnter_t* pCallback, bool bIOFailure)
	{
		if (pCallback->m_EChatRoomEnterResponse == k_EChatRoomEnterResponseSuccess)
		{
			// Make sure CUserLobby object exists on this client
			PlatformUserLobby* pLobby = TryGetLobby(pCallback->m_ulSteamIDLobby);

			for (IListener* pListener : m_listeners)
			{
				pListener->OnJoinedLobby(pLobby);
			}
		}
		else
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnLobbyJoinFailed(pCallback->m_ulSteamIDLobby);
			}
		}
	}

	void PlatformMatchmaking::OnGameServerChangeRequested(GameServerChangeRequested_t* pCallback)
	{
		string serverStr = string(pCallback->m_rgchServer);
		auto iDivider = serverStr.find_first_of(':');
		// IP address
		if (iDivider != string::npos)
		{
			string ipStr = serverStr.substr(0, iDivider);
			string portStr = serverStr.substr(iDivider + 1, serverStr.length() - iDivider);

			//gEnv->pConsole->ExecuteString(string("connect ").append(ipStr).append(" ").append(portStr).c_str());
		}
		else // Web address
		{
			//gEnv->pConsole->ExecuteString(string("connect ").append(serverStr));
		}
	}

	void PlatformMatchmaking::OnInvited(LobbyInvite_t* pInvite)
	{
		ISteamUtils* pSteamUtils = SteamUtils();
		if (!pSteamUtils || pInvite->m_ulGameID != pSteamUtils->GetAppID())
			return;

		for (IListener* pListener : m_listeners)
		{
			pListener->OnLobbyInviteReceived(pInvite->m_ulSteamIDLobby, pInvite->m_ulSteamIDUser);
		}
	}
}