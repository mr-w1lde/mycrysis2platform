/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformLobby.h"

#include "PlatformMatchmaking.h"
#include "PlatformServer.h"
#include "SteamworksSDK.h"


namespace C2
{
	/* Convert the character string in "ip" into an unsigned integer.

	This assumes that an unsigned integer contains at least 32 bits.*/
	//TNX for Crytek
	unsigned int ip_to_int(const char*  ip)
	{
		/* The return value.*/
		unsigned v = 0;
		/* The count of the number of bytes processed.*/
		int i;
		/* A pointer to the next digit to process.*/
		const char*  start;

		start = ip;
		for (i = 0; i < 4; i++) {
			/* The digit being processed.*/
			char c;
			/* The value of this byte.*/
			int n = 0;
			while (1) {
				c = *start;
				start++;
				if (c >= '0' && c <= '9') {
					n *= 10;
					n += c - '0';
				}
				/* We insist on stopping at "." if we are still parsing
				   the first, second, or third numbers. If we have reached
				   the end of the numbers, we will allow any character.*/
				else if ((i < 3 && c == '.') || i == 3) {
					break;
				}
				else {
					return -1;
				}
			}
			if (n >= 256) {
				return -1;
			}
			v *= 256;
			v += n;
		}
		return v;
	}

	PlatformUserLobby::PlatformUserLobby(uint64 lobbyId)
		: m_steamLobbyId(lobbyId)
		, m_callbackChatDataUpdate(this, &PlatformUserLobby::OnLobbyChatUpdate)
		, m_callbackDataUpdate(this, &PlatformUserLobby::OnLobbyDataUpdate)
		, m_callbackChatMessage(this, &PlatformUserLobby::OnLobbyChatMessage)
		, m_callbackGameCreated(this, &PlatformUserLobby::OnLobbyGameCreated)
		, m_serverIP(0)
		, m_serverPort(0)
		, m_serverId(0)
	{
	}
	PlatformUserLobby::~PlatformUserLobby()
	{
		Leave();
	}
	void PlatformUserLobby::AddListener(IListener & listener)
	{
		m_listeners.push_back(&listener);
	}
	void PlatformUserLobby::RemoveListener(IListener & listener)
	{
		util::find_and_erase(m_listeners, &listener);
	}
	bool PlatformUserLobby::HostServer(const char * szLevel, bool isLocal)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return false;
		}
		CSteamID ownerId = pSteamMatchmaking->GetLobbyOwner(m_steamLobbyId);
		// Only owner can decide to host
		ISteamUser* pSteamUser = SteamUser();
		if (!pSteamUser || ownerId != pSteamUser->GetSteamID())
			return false;

		IPlatformServer* pPlatformServer = pSteam->CreateServer(isLocal);
		if (pPlatformServer == nullptr)
		{
			//TODO LOG: Failed to create server!
			return false;
		}

		//gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this, "SteamUserLobby");

		//gEnv->pConsole->ExecuteString(string().Format("map %s s", szLevel));

		return true;
	}
	int PlatformUserLobby::GetMemberLimit() const
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			return pSteamMatchmaking->GetLobbyMemberLimit(m_steamLobbyId);
		}
		return 0;
	}
	int PlatformUserLobby::GetNumMembers() const
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			return pSteamMatchmaking->GetNumLobbyMembers(m_steamLobbyId);
		}
		return 0;
	}
	uint64 PlatformUserLobby::GetMemberAtIndex(int index) const
	{
		CSteamID result = k_steamIDNil;
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			result = pSteamMatchmaking->GetLobbyMemberByIndex(m_steamLobbyId, index);
		}
		return result.ConvertToUint64();
	}
	bool PlatformUserLobby::IsInServer() const
	{
		return m_serverIP != 0;
	}
	void PlatformUserLobby::Leave()
	{
		constexpr uint64 invalidLobby = 0;
		if (m_steamLobbyId != invalidLobby)
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnLeave();
			}

			ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
			const bool isShuttingDown = pSteamMatchmaking == nullptr;
			if (!isShuttingDown)
			{
				pSteamMatchmaking->LeaveLobby(m_steamLobbyId);
			}
			m_steamLobbyId = invalidLobby;

			ISteamUser* pSteamUser = SteamUser();
			if (pSteamUser && m_serverIP != 0)
			{
				pSteamUser->TerminateGameConnection(m_serverIP, m_serverPort);
			}

			if (!isShuttingDown)
			{
				if (PlatformMatchmaking* pMatchmaking = static_cast<PlatformMatchmaking*>(pSteam->GetMatchmaking()))
				{
					pMatchmaking->OnLobbyRemoved(this);
				}
			}
		}
	}
	uint64 PlatformUserLobby::GetOwnerId() const
	{
		CSteamID result = k_steamIDNil;
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			result = pSteamMatchmaking->GetLobbyOwner(m_steamLobbyId);
		}
		return result.ConvertToUint64();
	}
	uint64 PlatformUserLobby::GetIdentifier() const
	{
		return m_steamLobbyId;
	}
	bool PlatformUserLobby::SendChatMessage(const char * szMessage) const
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (pSteamMatchmaking)
		{
			return pSteamMatchmaking->SendLobbyChatMsg(m_steamLobbyId, szMessage, strlen(szMessage) + 1);
		}
		return false;
	}
	void PlatformUserLobby::ShowInviteDialog() const
	{
		if (ISteamFriends* pSteamFriends = SteamFriends())
		{
			pSteamFriends->ActivateGameOverlayInviteDialog(m_steamLobbyId);
		}
	}
	bool PlatformUserLobby::SetData(const char * szKey, const char * szValue)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return pSteamMatchmaking->SetLobbyData(m_steamLobbyId, szKey, szValue);
		}
		return false;
	}
	const char * PlatformUserLobby::GetData(const char * szKey) const
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return pSteamMatchmaking->GetLobbyData(m_steamLobbyId, szKey);
		}
		return nullptr;
	}
	void PlatformUserLobby::SetMemberLimit(int limit)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return;
		}

		pSteamMatchmaking->SetLobbyMemberLimit(m_steamLobbyId, limit);
	}
	void PlatformUserLobby::SetPrivate(bool _private)
	{
		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return;
		}

		pSteamMatchmaking->SetLobbyType(m_steamLobbyId, _private ? ELobbyType::k_ELobbyTypeFriendsOnly : ELobbyType::k_ELobbyTypePublic);
	}

	void PlatformUserLobby::OnLobbyChatUpdate(LobbyChatUpdate_t* pCallback)
	{
		if (pCallback->m_ulSteamIDLobby != m_steamLobbyId)
			return;

		switch (pCallback->m_rgfChatMemberStateChange)
		{
		case k_EChatMemberStateChangeEntered:
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnPlayerEntered(pCallback->m_ulSteamIDUserChanged);
			}
		}
		break;
		case k_EChatMemberStateChangeLeft:
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnPlayerLeft(pCallback->m_ulSteamIDUserChanged);
			}
		}
		break;
		case k_EChatMemberStateChangeDisconnected:
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnPlayerDisconnected(pCallback->m_ulSteamIDUserChanged);
			}
		}
		break;
		case k_EChatMemberStateChangeKicked:
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnPlayerKicked(pCallback->m_ulSteamIDUserChanged, pCallback->m_ulSteamIDMakingChange);
			}
		}
		break;
		case k_EChatMemberStateChangeBanned:
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnPlayerBanned(pCallback->m_ulSteamIDUserChanged, pCallback->m_ulSteamIDMakingChange);
			}
		}
		break;
		}
	}

	void PlatformUserLobby::OnLobbyDataUpdate(LobbyDataUpdate_t* pCallback)
	{
		if (!pCallback->m_bSuccess)
			return;

		//if (m_serverIP == 0 || m_serverPort == 0 || m_serverId == 0)
		//{
		//	const char* ipString = GetData("ipaddress");
		//	const char* portString = GetData("port");
		//	const char* serverIdString = GetData("serverid");
		//
		//	if (strlen(ipString) != 0 && strlen(portString) != 0 && strlen(serverIdString) != 0)
		//	{
		//		ConnectToServer(atoi(ipString), atoi(portString), atoi(serverIdString));
		//	}
		//}

		for (IListener* pListener : m_listeners)
		{
			pListener->OnDataUpdate(pCallback->m_ulSteamIDMember);
		}
	}

	void PlatformUserLobby::OnLobbyChatMessage(LobbyChatMsg_t* pCallback)
	{
		if (pCallback->m_ulSteamIDLobby != m_steamLobbyId)
			return;

		CSteamID userId;
		EChatEntryType entryType;
		char data[2048];
		int cubData = sizeof(data);

		ISteamMatchmaking* pSteamMatchmaking = SteamMatchmaking();
		if (!pSteamMatchmaking)
		{
			return;
		}
		int numBytes = pSteamMatchmaking->GetLobbyChatEntry(m_steamLobbyId, pCallback->m_iChatID, &userId, data, cubData, &entryType);
		if (entryType == k_EChatEntryTypeChatMsg)
		{
			string message(data, numBytes);

			if (message.size() > 0)
			{
				for (IListener* pListener : m_listeners)
				{
					pListener->OnChatMessage(userId.ConvertToUint64(), message.c_str());
				}
			}
		}
	}

	void PlatformUserLobby::OnLobbyGameCreated(LobbyGameCreated_t* pCallback)
	{
		ISteamUser* pSteamUser = SteamUser();
		if (pSteamUser)
		{
			for (IListener* pListener : m_listeners)
			{
				pListener->OnGameCreated(pCallback->m_ulSteamIDGameServer, pCallback->m_unIP, pCallback->m_usPort, pSteamUser->GetSteamID() == GetOwnerId());
			}
		}
		else
		{
			//CryWarning(VALIDATOR_MODULE_SYSTEM, VALIDATOR_WARNING, "Steam user service not available");
			//TODO WARNGIN : Steam user service not available
		}

		/*if (!gEnv->bServer)
		{
			ConnectToServer(pCallback->m_unIP, pCallback->m_usPort, pCallback->m_ulSteamIDGameServer);
		}*/
	}
}