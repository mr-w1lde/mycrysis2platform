/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformUser.h"

namespace C2
{
	PlatformUser::PlatformUser(CSteamID id)
		: m_id(id)
	{
	}

	PlatformUser::~PlatformUser()
	{
		m_id.Clear();

		// Clear friends
		{
			for (auto &it : m_friends)
			{
				SAFE_DELETE(it);
			}

			m_friends.clear();
		}
	}

	const char * PlatformUser::GetNickname() const
	{
		ISteamFriends* pSteamFriends = SteamFriends();
		if (!pSteamFriends)
		{
			return nullptr;
		}

		return pSteamFriends->GetFriendPersonaName(m_id);
	}

	uint64 PlatformUser::GetIdentifier() const
	{
		return m_id.ConvertToUint64();
	}

	bool PlatformUser::SetStatus(const char * key, const char * value)
	{
		ISteamUser* pSteamUser = SteamUser();
		ISteamFriends* pSteamFriends = SteamFriends();
		if (!pSteamFriends || !pSteamUser || m_id != pSteamUser->GetSteamID())
		{
			return false;
		}

		bool result = pSteamFriends->SetRichPresence(key, value);

		return result;
	}

	const char * PlatformUser::GetStatus() const
	{
		ISteamFriends* pSteamFriends = SteamFriends();
		if (!pSteamFriends)
		{
			return nullptr;
		}
		return pSteamFriends->GetFriendRichPresence(m_id, "status");
	}

	const std::vector<IPlatformUser*>& PlatformUser::GetFriendList()
	{
		if (m_friends.empty())
		{
			ISteamFriends* pSteamFriends = SteamFriends();
			if (pSteamFriends)
			{
				int count = pSteamFriends->GetFriendCount(EFriendFlags::k_EFriendFlagImmediate);

				for (int i = 0; i < count; i++)
				{
					CSteamID friendSteamID = pSteamFriends->GetFriendByIndex(i, EFriendFlags::k_EFriendFlagImmediate);

					m_friends.push_back(new PlatformUser(friendSteamID));
				}
			}
		}

		return m_friends;
	}
}