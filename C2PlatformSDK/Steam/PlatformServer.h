/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  14:40 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_SERVER_H__
#define _STEAMWORKS_SERVER_H__

#include <steam_api.h>
#include <vector>
#include <C2PlatfromSDK/ISteam/IPlatformServer.h>

namespace C2
{
	class PlatformServer : public IPlatformServer
	{
	public:
		PlatformServer(bool bIsLocal, int port, bool bIsDedicated);
		~PlatformServer();
	public:
		// IPlatformServer
		virtual uint64 GetIdentifier() const override;
		virtual uint32 GetPublicIP() const override;
		virtual const char* GetPublicIPString() const override;
		virtual uint16 GetPort() const override;
		virtual bool IsLocal() const override;
		virtual bool AuthenticateUser(uint32 clientIP, char* szAuthData, int authDataLength, uint64& userId) override;
		virtual void SendUserDisconnect(uint64 userId) override;
		//~IPlatformServer
	private:
		bool m_bLocal;
		int m_port;
	};
}

#endif