/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  14:40 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_MATCHMAKING_H__
#define _STEAMWORKS_MATCHMAKING_H__

#include <steam_api.h>
#include <vector>
#include <C2PlatfromSDK/ISteam/IPlatformMatchmaking.h>

namespace C2
{
	class PlatformUserLobby;

	class PlatformMatchmaking : public IPlatformMatchmaking
	{
	public:
		PlatformMatchmaking();
		virtual ~PlatformMatchmaking() = default;
	public:
		// IPlatformMatchmaking
		virtual void AddListener(IListener& listener) override;
		virtual void RemoveListener(IListener& listener) override;
		virtual void CreateLobby(IPlatformUserLobby::EVisbility visibility, int maxMemberCount) override;
		virtual IPlatformUserLobby* GetUserLobby(uint64 user) const override;
		virtual IPlatformUserLobby* GetLobbyById(uint64 id) override;
		virtual void AddLobbyQueryFilterString(const char* szKey, const char* szValue, IPlatformUserLobby::EComparison comparison) override;
		virtual void QueryLobbies() override;;
		virtual uint64 GetQueriedLobbyIdByIndex(int index) const override;
		virtual void JoinLobby(uint64 lobbyId) override;
		virtual void InviteFriendToLobby(uint64 lobbyId, uint64 friendId) override;
		//~IPlatformMatchmaking
		void OnLobbyRemoved(PlatformUserLobby* pLobby);
	protected:
		PlatformUserLobby* TryGetLobby(uint64 id);

	protected:
		void OnCreateLobby(LobbyCreated_t* pResult, bool bIOFailure);
		CCallResult<PlatformMatchmaking, LobbyCreated_t> m_callResultCreateLobby;

		void OnLobbyMatchList(LobbyMatchList_t* pLobbyMatchList, bool bIOFailure);
		CCallResult<PlatformMatchmaking, LobbyMatchList_t> m_callResultLobbyMatchList;

		void OnJoin(LobbyEnter_t* pLobbyMatchList, bool bIOFailure);
		CCallResult<PlatformMatchmaking, LobbyEnter_t> m_callResultLobbyEntered;

		STEAM_CALLBACK(PlatformMatchmaking, OnJoinRequested, GameLobbyJoinRequested_t, m_callbackJoinRequested);
		STEAM_CALLBACK(PlatformMatchmaking, OnGameServerChangeRequested, GameServerChangeRequested_t, m_callbackGameServerChangeRequested);
		STEAM_CALLBACK(PlatformMatchmaking, OnInvited, LobbyInvite_t, m_callbackInvited);

	protected:
		std::vector<IPlatformMatchmaking::IListener*> m_listeners;
		std::vector<std::unique_ptr<PlatformUserLobby>> m_lobbies;
	};
}

#endif