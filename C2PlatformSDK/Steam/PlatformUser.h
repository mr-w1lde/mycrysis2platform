/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_USER_H__
#define _STEAMWORKS_USER_H__

#include <steam_api.h>
#include <C2PlatfromSDK/ISteam/IPlatformUser.h>

namespace C2
{
	class PlatformUser : public IPlatformUser
	{
	public:
		PlatformUser(CSteamID id);
		~PlatformUser();
	public:
		// IPlatformUser
		virtual const char* GetNickname() const override;
		virtual uint64 GetIdentifier() const override;
		virtual bool SetStatus(const char* key, const char* value) override;
		virtual const char* GetStatus() const override;
		virtual const std::vector<IPlatformUser*>& GetFriendList() override;
		//~IPlatformUser
	private:
		std::vector<IPlatformUser*>               m_friends;
		CSteamID								  m_id;
	};
}

#endif