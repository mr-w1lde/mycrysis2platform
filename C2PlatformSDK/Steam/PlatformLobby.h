/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_LOBBY_H__
#define _STEAMWORKS_LOBBY_H__

#include <steam_api.h>
#include <C2PlatfromSDK/ISteam/IPlatformLobby.h>

namespace C2
{
	class PlatformUserLobby : public IPlatformUserLobby
	{
	public:
		PlatformUserLobby(uint64 lobbyId);
		virtual ~PlatformUserLobby();
	public:
		// IPlatformUserLobby
		virtual void AddListener(IListener& listener) override;
		virtual void RemoveListener(IListener& listener) override;
		virtual bool HostServer(const char* szLevel, bool isLocal) override;
		virtual int GetMemberLimit() const override;
		virtual int GetNumMembers() const override;
		virtual uint64 GetMemberAtIndex(int index) const override;
		virtual bool IsInServer() const override;
		virtual void Leave() override;
		virtual uint64 GetOwnerId() const override;
		virtual uint64 GetIdentifier() const override;
		virtual bool SendChatMessage(const char* szMessage) const override;
		virtual void ShowInviteDialog() const override;
		virtual bool SetData(const char* szKey, const char* szValue) override;
		virtual const char* GetData(const char* szKey) const override;
		virtual void SetMemberLimit(int limit) override;
		virtual void SetPrivate(bool _private) override;
		//~IPlatformUserLobby

		void ConnectToServer(uint32 ip, uint16 port, uint64 serverId);

	protected:
		STEAM_CALLBACK(PlatformUserLobby, OnLobbyChatUpdate, LobbyChatUpdate_t, m_callbackChatDataUpdate);
		STEAM_CALLBACK(PlatformUserLobby, OnLobbyDataUpdate, LobbyDataUpdate_t, m_callbackDataUpdate);

		STEAM_CALLBACK(PlatformUserLobby, OnLobbyChatMessage, LobbyChatMsg_t, m_callbackChatMessage);

		STEAM_CALLBACK(PlatformUserLobby, OnLobbyGameCreated, LobbyGameCreated_t, m_callbackGameCreated);

		std::vector<IListener*> m_listeners;
		uint64 m_steamLobbyId;

		uint32 m_serverIP;
		uint16 m_serverPort;

		uint64 m_serverId;
	};
}

#endif