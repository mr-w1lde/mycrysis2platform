/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:23 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_SDK_H__
#define _STEAMWORKS_SDK_H__

#define APP_STEAM_ID 108800 // Crysis 2 Maximum Edition SteamID

#include <steam_api.h>
#include <C2PlatfromSDK/ISteam/ISteamworksSDK.h>

namespace C2
{
	class SteamworksSDK : public ISteamworksSDK
	{
	public:
		SteamworksSDK();
		~SteamworksSDK();
	public:
		// ISteamworksSDK
		virtual int GetBuildIdentifier() const override;
		virtual void AddListener(IListener& listener) override;
		virtual void RemoveListener(IListener& listener) override;
		virtual IPlatformUser* GetLocalClient() override;
		virtual IPlatformUser* GetUserById(uint64 id) override;
		virtual void   UpdateFriendList() override;
		virtual bool IsFriendWith(uint64 otherUserId) const override;
		virtual IPlatformServer* CreateServer(bool bLocal) override;
		virtual IPlatformServer* GetLocalServer() const override;
		virtual IPlatformMatchmaking* GetMatchmaking() const override;
		virtual IPlatformNetworking* GetNetworking() const override;
		virtual bool OwnsApplication(ApplicationIdentifier id) const override;
		virtual ApplicationIdentifier GetApplicationIdentifier() const override;
		virtual bool OpenDialog(EDialog dialog) const override;
		virtual bool OpenDialog(const char* szPage) const override;
		virtual bool OpenBrowser(const char* szURL) const override;
		virtual bool CanOpenPurchaseOverlay() const override;
		virtual bool GetAuthToken(std::string& tokenOut, int& issuerId) override;
		virtual EConnectionStatus GetConnectionStatus() const override;
		//~ISteamworksSDK
	protected:
		STEAM_CALLBACK(SteamworksSDK, OnGameOverlayActivated, GameOverlayActivated_t, m_callbackGameOverlayActivated);

	protected:
		IPlatformUser* TryGetUser(CSteamID id);
	private:
		bool m_isInitialized = false;

		std::shared_ptr<IPlatformMatchmaking> m_pMatchmaking;

		std::shared_ptr<IPlatformNetworking> m_pNetworking;

		std::shared_ptr<IPlatformServer> m_pServer;

		std::vector<std::unique_ptr<IPlatformUser>> m_users;

		int m_awaitingCallbacks;
		std::vector<IListener*> m_listeners;

		uint32 m_authTicketHandle;
	};
}

extern C2::SteamworksSDK* pSteam;

#endif