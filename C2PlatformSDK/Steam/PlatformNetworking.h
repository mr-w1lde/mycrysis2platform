/*************************************************************************
Copyright (C), null, 2019.
-------------------------------------------------------------------------
History:
- 13/03/2019  6:30 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _STEAMWORKS_NETWORK_H__
#define _STEAMWORKS_NETWORK_H__

#include <steam_api.h>
#include <C2PlatfromSDK/ISteam/IPlatformNetworking.h>

namespace C2
{
	class PlatformNetworking : public IPlatformNetworking
	{
	public:
		PlatformNetworking();
		virtual ~PlatformNetworking() = default;
	public:
		// IPlatformNetworking
		virtual bool SendPacket(uint64 remoteUser, void* pData, uint32 dataLength) override;
		virtual bool CloseSession(uint64 remoteUser) override;
		virtual bool IsPacketAvailable(uint32* pPacketSizeOut) const override;
		virtual bool ReadPacket(void* pDest, uint32 destLength, uint32* pMessageSizeOut, uint64* pRemoteIdOut) override;
		//~IPlatformNetworking

		ISteamNetworking* GetSteamNetworking() const;

		STEAM_CALLBACK(PlatformNetworking, OnSessionRequest, P2PSessionRequest_t, m_callbackP2PSessionRequest);
		STEAM_CALLBACK(PlatformNetworking, OnSessionConnectFail, P2PSessionConnectFail_t, m_callbackP2PSessionConnectFail);
	};
}

#endif