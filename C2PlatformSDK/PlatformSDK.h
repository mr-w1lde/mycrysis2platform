/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 13/03/2019  5:40 : Created by Stanislav Migunov
*************************************************************************/

#ifndef _PLATFORM_SDK_H__
#define _PLATFORM_SDK_H__

#include <C2PlatfromSDK/IPlatformSDK.h>
#include <C2PlatfromSDK/MasterServer/IMasterServer.h>


namespace C2
{
	class PlatformSDK : public IPlatformSDK
	{
	public:
		PlatformSDK();
		~PlatformSDK();
	public:
		// IPlatformSDK
		virtual bool Initialize() override;
		virtual IMasterServer* GetMasterServer() override;
		//~IPlatformSDK
	public:

	private:
		IMasterServer* m_pMasterServer = nullptr;
	};
}

#endif