/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 13/03/2019  5:40 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "PlatformSDK.h"
#include "MasterServer/MasterServer.h"

#include <qobject.h>
#include <qthread.h>

namespace C2
{
	PlatformSDK::PlatformSDK()
	{
	}

	PlatformSDK::~PlatformSDK()
	{
		SAFE_DELETE(m_pMasterServer);
	}

	bool PlatformSDK::Initialize()
	{
		CMasterServer* pMasterServer = new CMasterServer();
		m_pMasterServer = pMasterServer;


		return true;
	}

	IMasterServer * PlatformSDK::GetMasterServer()
	{
		return m_pMasterServer;
	}
}