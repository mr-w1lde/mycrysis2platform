/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 14/03/2019  16:51 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _PLATFORM_SDK_STDAFX_H__
#define _PLATFORM_SDK_STDAFX_H__

#include <qvariant.h>

struct SConnectionInfo
{
	const char* m_ip;
	int m_port;

	SConnectionInfo(const char* ip, int port)
		: m_ip(ip)
		, m_port(port)
	{}
};


#define SAFE_DELETE(x) { if(x) { delete x; } }
#define SAFE_UPDATE(x) { if(x) { x->Update(); } }

#define GAME_VERSION "Beta 5620"
#define PRODUCT_NAME "Crysis 2"
#define PRODUCT_DESC ""

#ifdef _MSC_VER
typedef signed __int8  int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;

typedef unsigned __int8  uint8;
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;
#endif

#include <Windows.h>

#include <stdexcept>

#include <string>

using string = std::string;

namespace util
{
	template<class C, class V>
	static inline void find_and_erase(C& c, const V& v)
	{
		typename C::iterator it = std::find(c.begin(), c.end(), v);
		if (it != c.end())
		{
			c.erase(it);
		}
	}

}


#endif