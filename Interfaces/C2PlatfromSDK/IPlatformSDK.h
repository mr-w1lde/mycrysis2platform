/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 13/03/2019  5:29 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _INTERFACE_PLATFORM_SDK_H__
#define _INTERFACE_PLATFORM_SDK_H__

//TODO INTERFACE DESCRIPTION.

namespace C2
{
	struct ISteamworksSDK;
	struct IMasterServer;

	struct IPlatformSDK
	{
		//<*
		virtual ~IPlatformSDK() {}
		//*>

		virtual bool Initialize() = 0;
		virtual IMasterServer* GetMasterServer() = 0;
	};
}

#endif // !INTERFACE_PLATFORM_SDK_H__
