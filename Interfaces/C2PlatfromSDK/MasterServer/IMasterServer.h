/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 05/04/2019  22:55 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _INTERFACE_MASTERSERVER_SDK_H__
#define _INTERFACE_MASTERSERVER_SDK_H__

//TODO INTERFACE DESCRIPTION.


namespace MasterServer
{
	class Packet;
}

namespace C2
{
	struct IMasterServerListener
	{
		virtual void OnConnected(const bool& status) = 0;
		virtual void OnIncomingMessage(MasterServer::Packet* packet) = 0;
		virtual void OnDisconnect() = 0;
		virtual ~IMasterServerListener() {}
	};

	struct IMasterServer
	{
		virtual bool ConnectToMS(const char* address, int port) = 0;
		virtual bool SendMessageToServer(const MasterServer::Packet& packet) = 0;
		virtual void CloseConnection() = 0;
		

		virtual void AddListener(IMasterServerListener* listener) = 0;
		virtual void RemoveListener(IMasterServerListener* listener) = 0;
		
		virtual ~IMasterServer() {}
	};
}

#endif // !INTERFACE_PLATFORM_SDK_H__
