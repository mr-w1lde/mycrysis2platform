/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 12/03/2019  21:18 : Created by Stanislav Migunov
*************************************************************************/
#include "PlatformIntegration.h"
#include <stdexcept>

namespace C2
{
	PlatformIntegration::PlatformIntegration()
	{
		hPlatformDll = nullptr;
	}

	bool PlatformIntegration::PlatformStartup()
	{
		if (!hPlatformDll)
		{
			hPlatformDll = LoadLibraryA("C2PlatformSDK.dll");

			if (!hPlatformDll)
			{
				MessageBoxA(NULL, "FATAL ERROR: C2PlatfromSDK.dll wasn't load!", "C2PlatformSDK", MB_OK | MB_ICONERROR);
				throw std::runtime_error("hPlatformDll is Null");
			}

			using FPlatformInit = bool(*)();
			FPlatformInit fPlatfromInit = (FPlatformInit)(GetProcAddress(hPlatformDll, "InitializeSDK"));

			if (!fPlatfromInit)
			{
				return false;
				//TODO RuntimeError...
			}
			else
			{
				return fPlatfromInit();
			}

		}
		else
		{
			MessageBoxA(NULL, "FATAL ERROR: C2PlatfromSDK.dll was load!", "C2PlatformSDK", MB_OK | MB_ICONERROR);
			throw std::runtime_error("hPlatformDll is not Null");

			return false;
		}

		return false;
	}

	void PlatformIntegration::PlatformShutdown()
	{
		if (hPlatformDll)
		{
			using FPlatformShutdown = void(*)();
			FPlatformShutdown fPlatfromShutdown = (FPlatformShutdown)(GetProcAddress(hPlatformDll, "ShutDownSDK"));

			if (!fPlatfromShutdown)
			{
				//TODO RuntimeError...
				return;
			}

			fPlatfromShutdown();

			FreeLibrary(hPlatformDll);
			hPlatformDll = nullptr;
		}
		else
		{
			//TODO Error...
		}
	}

	IPlatformSDK * PlatformIntegration::GetPlatformSDK()
	{
		using FPlatformGetSDK = IPlatformSDK*(*)();
		FPlatformGetSDK fGetSDK = (FPlatformGetSDK)GetProcAddress(hPlatformDll, "GetPlatformSDK");

		if (fGetSDK)
		{
			return fGetSDK();
		}

		return nullptr;
	}

}