/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 12/03/2019  21:18 : Created by Stanislav Migunov
*************************************************************************/
#ifndef _INTEGRATION_PLATFORM_SDK_H__
#define _INTEGRATION_PLATFORM_SDK_H__

#include <C2PlatfromSDK/IPlatformSDK.h>
#include <Windows.h>

struct IPlatformSDK;

namespace C2
{
	class PlatformIntegration
	{
	public:
		PlatformIntegration();
	public:
		bool PlatformStartup();
		void PlatformShutdown();
	public:
		IPlatformSDK* GetPlatformSDK();

	private:
		HMODULE hPlatformDll;
	};
}

#endif // !INTERFACE_PLATFORM_SDK_H__
