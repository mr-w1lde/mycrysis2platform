/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 22/03/2019  18:15 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "CryGameClient.h"
#include "CryCustomLobby.h"

#include <ILocalizationManager.h>

CCryProfiles* pCryProfilesManager = nullptr;
CCryGameClient* pGameClientInstance = nullptr;

uint32 gGamesCounter = 0;
bool isDataSend = false;

CrySessionHandle cGlobalSessionH;

void OnUserJoinInLobby(UCryLobbyEventData eventData, void *userParam)
{
	CryUserAuth currUser = pGameClientInstance->GetCurrentAuthoUser();
	
	if (string(currUser.m_nickname) == string(eventData.pSessionUserData->data.m_userName))
	{
		/*CryProto::CryToken token;
		token.set_token(currUser.m_userToken);

		cGlobalSessionH = eventData.pSessionUserData->session;

		ECryLobbyError error = pGameClientInstance->GetPacketSendler()->SendProtoDataToServer(token, eventData.pSessionUserData->session, ELobbyPacketType::eLPT_UserTokenAuthorization);
		if (error != eCLE_Success)
			printf("[CryProto::CryToken] Couldn't send token to server. Error<%u>", error);*/
	}
}

void OnSessionUserLeave(UCryLobbyEventData eventData, void *userParam)
{
	SCryLobbySessionUserData* pUserData = eventData.pSessionUserData;
	//gEnv->pSystem->Quit();
}

void OnIncomingPacket(UCryLobbyEventData eventData, void* userParam)
{
	CCryLobbyPacket* pLobbyPacket = eventData.pUserPacketData->pPacket;
	uint32 packetId = pLobbyPacket->StartRead();

	printf("Incoming Packet with id = %u\n", packetId);

}

void OnForcedFromRoom(UCryLobbyEventData eventData, void *userParam)
{
	printf("Forced event %u.\n", eventData.pForcedFromRoomData->m_why);
}

void MatchmakingSessionJoinFromConsoleCallback(CryLobbyTaskID taskID, ECryLobbyError error, CrySessionHandle h, uint32 ip, uint16 port, void* pArg)
{
	if (error == eCLE_Success)
	{
		char command[128];
		IConsole* pConsole = gEnv->pConsole;

		sprintf(command, "connect <session>%08X,%u.%u.%u.%u:%u", h, ((uint8*)&ip)[0], ((uint8*)&ip)[1], ((uint8*)&ip)[2], ((uint8*)&ip)[3], port);

		if (pConsole)
		{
			pConsole->ExecuteString(command, false, true);
		}
	}
	else if (error == eCLE_ConnectionFailed)
	{
		//TODO COM ERROR.
		//gEnv->pSystem->Quit();
	}
}

CCryGameClient::CCryGameClient(CryUserAuth userAuth)
	: m_cryUserAuth(userAuth)
{
	if (!gEnv)
	{
		return;
	}

	m_pGameFramework = gEnv->pGame->GetIGameFramework();
	m_pCryLobby = gEnv->pNetwork->GetLobby();

	m_pCryProfiles = new CCryProfiles();
	pCryProfilesManager = m_pCryProfiles;


	if (m_pGameFramework)
	{
		m_pGameFramework->RegisterListener(this, "ExtensionDll", FRAMEWORKLISTENERPRIORITY_DEFAULT);
		m_pGameFramework->GetIGameplayRecorder()->RegisterListener(this);
	}

	m_pCryLobbyPacketSendler = new CCryLobbyPacketSendler(m_pCryLobby);

	m_connectionIp = gEnv->pConsole->GetCVar("mycrysis_connectIP")->GetString();
	m_serverConnTime = gEnv->pConsole->GetCVar("mycrysis_connectionTimeOut")->GetFVal();

	//eCLSE_SessionUserLeave
	m_pCryLobby->RegisterEventInterest(eCLSE_SessionUserJoin, OnUserJoinInLobby, NULL);
	m_pCryLobby->RegisterEventInterest(eCLSE_SessionUserLeave, OnSessionUserLeave, NULL);
	m_pCryLobby->RegisterEventInterest(eCLSE_UserPacket, OnIncomingPacket, NULL);
	m_pCryLobby->RegisterEventInterest(eCLSE_ForcedFromRoom, OnForcedFromRoom, NULL);

	pGameClientInstance = this;
}

CCryGameClient::~CCryGameClient()
{
	pPlatformSDK->GetMasterServer()->RemoveListener(this);

	m_pCryLobby->UnregisterEventInterest(eCLSE_SessionUserJoin, OnUserJoinInLobby, NULL);
	m_pCryLobby->UnregisterEventInterest(eCLSE_SessionUserLeave, OnSessionUserLeave, NULL);
	m_pCryLobby->UnregisterEventInterest(eCLSE_UserPacket, OnIncomingPacket, NULL);
	m_pCryLobby->UnregisterEventInterest(eCLSE_ForcedFromRoom, OnForcedFromRoom, NULL);

	m_pGameFramework->GetIGameplayRecorder()->UnregisterListener(this);
	m_pGameFramework->UnregisterListener(this);

	SAFE_DELETE(m_pCryLobbyPacketSendler);
}

void CCryGameClient::OnPostUpdate(float fDeltaTime)
{
	if (m_bIsSMultiplayer && m_bConnectToServer == false)
	{
		//gEnv->pConsole->ExecuteString("connect 127.0.0.1");
		//ConnectToServer(m_cryUserAuth.m_ipServer);
		m_bConnectToServer = true;
	}

	if (!m_bIsSMultiplayer)
	{
		pPlatformSDK->GetMasterServer()->AddListener(this);

		string masterIP = gEnv->pConsole->GetCVar("ms_globalIP")->GetString();
		int globalPort = gEnv->pConsole->GetCVar("ms_globalPort")->GetIVal();

		pPlatformSDK->GetMasterServer()->ConnectToMS(masterIP, globalPort);
		

		m_bIsSMultiplayer = true;
	}

	if (GetAsyncKeyState(VK_F5))
	{
		if (!m_bIsSended)
		{
			CryUserAuth currUser = pGameClientInstance->GetCurrentAuthoUser();

			const char* userId = m_pCryProfiles->GetProfileMgr()->GetCurrentUser();
			IPlayerProfile* pProfile = m_pCryProfiles->GetProfileMgr()->GetCurrentProfile(userId);

			CrysisProto::CryOnlineAttributes protoData;

			if (pProfile)
				protoData = m_pCryProfiles->GetOnlineAttributes(pProfile);

			CrysisProto::CryUser* pUser = protoData.mutable_user();
			string nickname = gEnv->pConsole->GetCVar("cl_nickname")->GetString();

			pUser->set_token(nickname);

			MasterServer::Packet packet;
			packet.set_type(MasterServer::Packet::USER_SAVE_DATA);

			uint32 protoLen = protoData.ByteSizeLong();

			packet.set_data(protoData.SerializeAsString());
			packet.set_len(protoLen);

			if (!pPlatformSDK->GetMasterServer()->SendMessageToServer(packet))
				printf("[MasterServer] Couldn't send user data to server.\n");
			else
				printf("[MasterServer] Sending data to master server...\n");

			m_bIsSended = true;
		}
	}
	
	if (GetAsyncKeyState(VK_F6))
	{
		system("cls");
		const char* user = m_pCryProfiles->GetProfileMgr()->GetCurrentUser();
		IPlayerProfile* pLocalProfile = m_pCryProfiles->GetProfileMgr()->GetCurrentProfile(user);

		IAttributeEnumeratorPtr pAtr = pLocalProfile->CreateAttributeEnumerator();
		
		IAttributeEnumerator::SAttributeDescription desc;

		while (pAtr->Next(desc))
		{
			string val;
			pLocalProfile->GetAttribute(desc.name, val);
			printf("%s = %s\n", desc.name, val.c_str());
		}
	}

	if (m_bStartTimerForConnection && ((std::clock() - m_connectionTime) / (double)CLOCKS_PER_SEC > m_serverConnTime))
	{
		ConnectToServer(m_connectionIp);
		m_bStartTimerForConnection = false;
	}
}

void CCryGameClient::OnActionEvent(const SActionEvent & event)
{
	if (event.m_event == eAE_channelCreated)
	{
		m_pLocalNetChannel = m_pGameFramework->GetClientChannel();
		isDataSend = false;
	}
	else if (event.m_event == eAE_channelDestroyed)
	{
		m_pLocalNetChannel = nullptr;

		
		if (isDataSend == false)
		{
			CryUserAuth currUser = pGameClientInstance->GetCurrentAuthoUser();

			const char* userId = m_pCryProfiles->GetProfileMgr()->GetCurrentUser();
			IPlayerProfile* pProfile = m_pCryProfiles->GetProfileMgr()->GetCurrentProfile(userId);

			CrysisProto::CryOnlineAttributes protoData;

			if (pProfile)
				protoData = m_pCryProfiles->GetOnlineAttributes(pProfile);

			CrysisProto::CryUser* pUser = protoData.mutable_user();
			string nickname = gEnv->pConsole->GetCVar("cl_nickname")->GetString();

			pUser->set_token(nickname);

			MasterServer::Packet packet;
			packet.set_type(MasterServer::Packet::USER_SAVE_DATA);

			uint32 protoLen = protoData.ByteSizeLong();

			packet.set_data(protoData.SerializeAsString());
			packet.set_len(protoLen);

			if (!pPlatformSDK->GetMasterServer()->SendMessageToServer(packet))
				printf("[MasterServer] Couldn't send user data to server.\n");
			else
				printf("[MasterServer] Sending data to master server...\n");
		}
	}

	if (eAE_serverName == event.m_event)
	{
		printf("Servername: %s\n", event.m_description);
	}
}

void CCryGameClient::OnGameplayEvent(IEntity * pEntity, const GameplayEvent & event)
{
	if (event.event == eGE_GameEnd)
	{
		
	}
}

void CCryGameClient::OnConnected(const bool& status)
{
	if (status == true)
	{
		gEnv->pConsole->ExecuteString("menu_cmd switch_game multiplayer");

		printf("Connected to MS \n");

		//AUTH
		CryUserAuth currUser = pGameClientInstance->GetCurrentAuthoUser();
		currUser.m_userToken = gEnv->pConsole->GetCVar("cl_nickname")->GetString();

		MasterServer::AuthorizationPacket authPacket;

		authPacket.set_token(currUser.m_userToken);
		authPacket.set_type(MasterServer::AuthorizationPacket::CLIENT);

		MasterServer::Packet packet;

		packet.set_type(MasterServer::Packet::AUTH_USER);

		packet.set_len(authPacket.ByteSizeLong());
		packet.set_data(authPacket.SerializeAsString());

		if (!pPlatformSDK->GetMasterServer()->SendMessageToServer(packet))
			printf("[MasterServer] Couldn't send message");
	}
	else
	{
		gEnv->pConsole->ExecuteString("menu_addWarning ConnectionNotAvailable");
	}
}

void CCryGameClient::OnIncomingMessage(MasterServer::Packet* packet)
{
	switch (packet->type())
	{
	case MasterServer::Packet::USER_SAVE_DATA:
	{
		CrysisProto::CryOnlineAttributes onlineAttributs;
		if (onlineAttributs.ParseFromArray(packet->data().c_str(), packet->len()))
		{
			printf("[MasterServer] Parsing CryProto Data...\n");
			onlineAttributs.PrintDebugString();

			const char* userId = m_pCryProfiles->GetProfileMgr()->GetCurrentUser();
			IPlayerProfile* pProfile = m_pCryProfiles->GetProfileMgr()->GetCurrentProfile(userId);

			if (pProfile)
			{
				m_pCryProfiles->SetPlayerProgression(pProfile, onlineAttributs);
				m_connectionTime = std::clock();
				m_bStartTimerForConnection = true;
			}
			else
				printf("Couldn't set player stats!\n");
		}
	}
	break;
	case MasterServer::Packet::DISCONNECT:
	{
		printf("[MasterServer] Disconnected from server.\n");
		pPlatformSDK->GetMasterServer()->CloseConnection();

		gEnv->pSystem->FatalError("Master server has been shuted down.");
		gEnv->pSystem->Quit();
	}
	break;
	case MasterServer::Packet::CLIENT_WARNING:
	{
		gEnv->pConsole->ExecuteString("menu_addWarning ConnectionNotAvailable");
	}
	break;
	}
}

void CCryGameClient::OnDisconnect()
{
	gEnv->pConsole->ExecuteString("menu_addWarning ConnectionNotAvailable");
	printf("Disconnected form MS\n");
}


bool CCryGameClient::ConnectToServer(const char * ip)
{
	if (!gEnv->bServer)
	{
		if (INetChannel* pCh = m_pGameFramework->GetClientChannel())
			pCh->Disconnect(eDC_UserRequested, "User left the game");
	}

	m_pGameFramework->EndGameContext();

	// set cl_serveraddr
	gEnv->pConsole->GetCVar("cl_serveraddr")->Set(ip);
	// set cl_serverport
	gEnv->pConsole->GetCVar("cl_serverport")->Set(64087);

	string tempHost = gEnv->pConsole->GetCVar("cl_serveraddr")->GetString();

	if (tempHost.find("<session>") == tempHost.npos)
	{
		ICryMatchMaking* pMatchMaking = m_pCryLobby->GetMatchMaking();
		if (pMatchMaking)
		{
			CrySessionID session = pMatchMaking->GetSessionIDFromConsole();
			if (session != CrySessionInvalidID)
			{
				m_pGameFramework->GetIGameSessionHandler()->JoinSessionFromConsole(session);
			}
			else
			{
				return false;
			}

			return false;
		}
	}
	
	SGameStartParams params;
	params.flags = eGSF_Client /*| eGSF_BlockingClientConnect*/;
	params.flags |= eGSF_ImmersiveMultiplayer;
	params.hostname = tempHost.c_str();
	params.pContextParams = nullptr;
	params.port = gEnv->pConsole->GetCVar("cl_serverport")->GetIVal();
	params.flags |= eGSF_NoGameRules;

	m_pGameFramework->StartGameContext(&params);
	return true;
}
