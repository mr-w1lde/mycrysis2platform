/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 15/12/2018  22:27 : Created by Stanislav Migunov
*************************************************************************/
#pragma once
#include <IPlayerProfiles.h>

typedef IPlayerProfileManager::EProfileOperationResult EProfileOperationResult;


class CCryProfiles
{
public:
	CCryProfiles();
	~CCryProfiles();

public:
	bool CreateProfile(const char* nick);
	void SetActiveProfile();
	bool SaveCurrentProfile();
	IPlayerProfile *GetCurrentProfile();
	IPlayerProfileManager* GetProfileMgr();
public:
	void SetPlayerProgression(IPlayerProfile* pPlayerProfile, const CrysisProto::CryOnlineAttributes& protoData);
public:
	CrysisProto::CryOnlineAttributes GetOnlineAttributes(IPlayerProfile* pPlayerProfile);
public:
	void ReloadProfile();
private:
	IPlayerProfileManager* m_pProfileManager;
	string m_currentUser;
	string m_profileName;
};

