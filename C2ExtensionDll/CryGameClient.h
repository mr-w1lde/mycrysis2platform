/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 22/03/2019  18:15 : Created by Stanislav Migunov
*************************************************************************/
#pragma once

#include <IGameFramework.h>
#include <ICryLobby.h>
#include <IGameplayRecorder.h>

#include "CryCustomLobby.h"
#include "CryProfiles.h"

#include <C2PlatfromSDK/MasterServer/IMasterServer.h>

#include <ctime>

typedef C2Startup::SCryUserAutho CryUserAuth;

class CCryGameClient 
	: public IGameFrameworkListener
	, public IGameplayListener
	, public C2::IMasterServerListener
{
public:
	CCryGameClient(CryUserAuth userAuth);
	~CCryGameClient();
public:
	// IGameFrameworkListener
	virtual void OnPostUpdate(float fDeltaTime) override;
	virtual void OnSaveGame(ISaveGame* pSaveGame) {}
	virtual void OnLoadGame(ILoadGame* pLoadGame) {}
	virtual void OnLevelEnd(const char* nextLevel) {}
	virtual void OnActionEvent(const SActionEvent& event) override;
	//~IGameFrameworkListener
public:
	// IGameplayListener
	virtual void OnGameplayEvent(IEntity *pEntity, const GameplayEvent &event) override;
	//~IGameplayListener
public:
	// C2::IMasterServerListener
	virtual void OnConnected(const bool& status) override;
	virtual void OnIncomingMessage(MasterServer::Packet* packet) override;
	virtual void OnDisconnect() override;
	//~C2::IMasterServerListener
public:
	CryUserAuth GetCurrentAuthoUser() { return m_cryUserAuth; }
	ICryLobby* GetCryLobby() { return m_pCryLobby ; }
	CCryLobbyPacketSendler* GetPacketSendler() { return m_pCryLobbyPacketSendler; }
protected:
	bool ConnectToServer(const char* ip);
private:
	IGameFramework* m_pGameFramework = nullptr;
	ICryLobby* m_pCryLobby = nullptr;
	INetChannel* m_pLocalNetChannel = nullptr;
private:
	CCryProfiles* m_pCryProfiles = nullptr;
	CCryLobbyPacketSendler* m_pCryLobbyPacketSendler = nullptr;
private:
	bool m_bConnectToServer = false;
	bool m_bIsSMultiplayer = false;
private:
	CryUserAuth m_cryUserAuth;
	bool m_bIsSended = false;
	bool m_bStartTimerForConnection = false;
	
	std::clock_t m_connectionTime;
private:
	string m_connectionIp;
	float m_serverConnTime;
};