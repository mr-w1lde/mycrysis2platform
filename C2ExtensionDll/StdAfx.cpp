#include "StdAfx.h"
#include <platform_impl.h>
#include "CryProfiles.h"
#include <ctime>

#include <IFlashUI.h>
#include "PlayerProgression.h"

#include <CryAction.h>
#include <IEntitySystem.h>
#include <IGameplayRecorder.h>

#include "CryGameClient.h"
#include "CryDedicated.h"

#include <IGameFramework.h>

#include <PoolAllocator.h>
#include <IGameRulesSystem.h>
#include "CryProfiles.h"

#include "GameRules/CryBattleRoyale.h"

#pragma comment(lib, "ws2_32.lib")


C2::IPlatformSDK* pPlatformSDK = nullptr;
C2::PlatformIntegration* pPlatformIntegration = nullptr;
sql::Connection* pSQLconnection = nullptr;

CCryGameClient* pCryGameClient = nullptr;
CCryDedicated* pCryDedicated = nullptr;

void CreateConsole()
{
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	freopen("CON", "w", stdout);
}

#define CGAMELOBBY_PTR_UPDATE DWORD(0x394BED30)

CCryProfiles* pCryProfilesSystem = nullptr;


void SetClientUsername(const char* nick)
{
	DWORD ptr = ReadPtr((DWORD)GetModuleHandleA("CryGameCrysis2.dll"), USERN_PTR_OFFSET);
	char* pClientNickname = reinterpret_cast<char*>(ptr);
	strcpy(pClientNickname, nick);

	gEnv->pConsole->GetCVar("cl_nickname")->Set(pClientNickname);
}

void ConnectToSQL()
{
	sql::Driver* pDriver = get_driver_instance();
	if (pDriver)
	{
		try
		{
			pSQLconnection = pDriver->connect(sqlServer, sqlId, sqlPassword);
			pSQLconnection->setSchema(sqlDatabase);
		}
		catch (sql::SQLException& exception)
		{
			gEnv->pLog->LogError("[SQL] error code %u", exception.getErrorCode());
			MessageBoxA(NULL, "Couldn't connect to the DB mycrysis.online. Server aren't avaiable.", "ERROR", MB_OK);
			gEnv->pSystem->Quit();
		}
	}
}

DWORD ReadPtr(DWORD baseAddr, DWORD offset)
{
	return (DWORD)(baseAddr + offset);
}


DWORD* TestPointerBack(DWORD p, DWORD u)
{
	return *(DWORD**)(p + u);
}


C2_API void LoadC2Extencion(SSystemGlobalEnvironment* pVar, C2Startup& startup)
{
	gEnv = pVar;

	IGameFramework* pGameFramework = gEnv->pGame->GetIGameFramework();
	IGameRulesSystem* pGameRulesSystem = pGameFramework->GetIGameRulesSystem();
	IGameObjectSystem* pGameObjectSystem = pGameFramework->GetIGameObjectSystem();

	CCryProfiles* pProfileMgr = new CCryProfiles();
	
	pPlatformIntegration = new C2::PlatformIntegration();

	pPlatformIntegration->PlatformStartup();
	pPlatformSDK = pPlatformIntegration->GetPlatformSDK();
	pPlatformSDK->Initialize();

	gEnv->pConsole->RegisterString("ms_globalIP", "127.0.0.1", VF_CHEAT, "IP Connection for custom MasterServer.");
	gEnv->pConsole->RegisterInt("ms_globalPort", 31000, VF_CHEAT, "Port number for custom MasterServer.");

	gEnv->pConsole->RegisterString("mycrysis_connectIP", "mycrysis.online", VF_CHEAT);
	gEnv->pConsole->RegisterFloat("mycrysis_connectionTimeOut", 3.0f, VF_CHEAT);

	if (startup.m_bClient)
	{
		string nickname = gEnv->pConsole->GetCVar("cl_nickname")->GetString();

		startup.m_userAutho.m_userToken = nickname;
		startup.m_userAutho.m_nickname = nickname;

		SetClientUsername(startup.m_userAutho.m_nickname);
		pCryGameClient = new CCryGameClient(startup.m_userAutho);
		

		CreateConsole();
	}
	else if (startup.m_bServer)
	{
		//ConnectToSQL();
		pCryDedicated = new CCryDedicated();
	}
}

C2_API void UnloadC2Extencion()
{
	pPlatformIntegration->PlatformShutdown();

	SAFE_DELETE(pPlatformIntegration);
	SAFE_DELETE(pCryGameClient);
	SAFE_DELETE(pCryDedicated);
}
