/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 15/12/2018  22:27 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "CryProfiles.h"
#include "CGameFramework.h"

#include <fstream>
#include <ostream>

CCryProfiles::CCryProfiles()
	: m_pProfileManager(nullptr)
{
	IGameFramework* pGameFramework = (IGameFramework*)gEnv->pGame->GetIGameFramework();

	if (!pGameFramework)
	{
		return;
	}

	m_pProfileManager = pGameFramework->GetIPlayerProfileManager();

	if (!m_pProfileManager)
	{
		return;
	}

}


CCryProfiles::~CCryProfiles()
{
}

bool CCryProfiles::CreateProfile(const char * nick)
{
	bool bCreated = false;

	EProfileOperationResult result;
	bCreated = m_pProfileManager->CreateProfile(m_currentUser, nick, false, result);

	if (bCreated)
	{
		printf("[CryProfile] Profile<%s> was created!\n", nick);
	}

	m_profileName = nick;

	return bCreated;
}

void CCryProfiles::SetActiveProfile()
{
	IPlayerProfile* pPlayerProfile = m_pProfileManager->ActivateProfile(m_currentUser, m_profileName.c_str());

	if (pPlayerProfile)
	{
		printf("[CryProfile] Profile<%s> was loaded!\n", pPlayerProfile->GetName());
		SaveCurrentProfile();
	}
	else
	{
		printf("[ERROR]Profile wasn't loaded!\n");
	}
}

bool CCryProfiles::SaveCurrentProfile()
{ 
	EProfileOperationResult result;
	return m_pProfileManager->SaveProfile(m_currentUser, result, EProfileReasons::ePR_All);
}

IPlayerProfile * CCryProfiles::GetCurrentProfile()
{
	return m_pProfileManager->GetCurrentProfile(m_currentUser.c_str());;
}

IPlayerProfileManager * CCryProfiles::GetProfileMgr()
{
	return m_pProfileManager;
}

void CCryProfiles::SetPlayerProgression(IPlayerProfile * pPlayerProfile, const CrysisProto::CryOnlineAttributes& protoData)
{
	// <!-- Player Progression -->
	{
		pPlayerProfile->SetAttribute("MP/PlayerProgression/XP", protoData.player_progression().xp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/LifetimeXP", protoData.player_progression().livetimexp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/Reincarnate", protoData.player_progression().reincarnate());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/DefaultXP", protoData.player_progression().defaultlifetimexp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/ArmorXP", protoData.player_progression().armorxp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/StealthXP", protoData.player_progression().stealthxp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/DefaultLifetimeXP", protoData.player_progression().defaultlifetimexp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/ArmorLifetimeXP", protoData.player_progression().armorlifetimexp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/StealthLifetimeXP", protoData.player_progression().stealthlifetimexp());
		pPlayerProfile->SetAttribute("MP/PlayerProgression/SkillRank", protoData.player_progression().skillrank());
	}

	//<!-- Persistant Stats -->
	{
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_3DogtagsFrom1Player", protoData.persistant_stats().eips_3dogtagsfrom1player());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_3FastGrenadeKills", protoData.persistant_stats().eips_3fastgrenadekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_5HealthRestoresInOneLife", protoData.persistant_stats().eips_5healthrestoresinonelife());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AcceleratorKills", protoData.persistant_stats().eips_acceleratorkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AirDeathKills", protoData.persistant_stats().eips_airdeathkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AirHeadshots", protoData.persistant_stats().eips_airheadshots());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AirKillKills", protoData.persistant_stats().eips_airkillkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AlienEnergyCollected", protoData.persistant_stats().eips_alienenergycollected());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AlienGunship", protoData.persistant_stats().eips_aliengunship());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AlienGunshipKills", protoData.persistant_stats().eips_aliengunshipkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AlienTicksExtracted", protoData.persistant_stats().eips_alienticksextracted());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AllEnemyDogTagsGot", protoData.persistant_stats().eips_allenemydogtagsgot());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ArmorActivations", protoData.persistant_stats().eips_armoractivations());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ArmorEfficiencyKills", protoData.persistant_stats().eips_armorefficiencykills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ArmourHits", protoData.persistant_stats().eips_armourhits());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AssaultDefendingKills", protoData.persistant_stats().eips_assaultdefendingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AssaultKillLastAttack5pc", protoData.persistant_stats().eips_assaultkilllastattack5pc());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_AssaultScopeKills", protoData.persistant_stats().eips_assaultscopekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_BlindKills", protoData.persistant_stats().eips_blindkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_BlindSelf", protoData.persistant_stats().eips_blindself());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_BlindingKills", protoData.persistant_stats().eips_blindingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_BombTheBase", protoData.persistant_stats().eips_bombthebase());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_BulletPenetrationKills", protoData.persistant_stats().eips_bulletpenetrationkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CaptureObjectives", protoData.persistant_stats().eips_captureobjectives());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CarryObjectives", protoData.persistant_stats().eips_carryobjectives());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CloakAwarenessKills", protoData.persistant_stats().eips_cloakawarenesskills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CloakedReloads", protoData.persistant_stats().eips_cloakedreloads());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CloakedWatchNearbyKill", protoData.persistant_stats().eips_cloakedwatchnearbykill());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CompleteOnlineMatches", protoData.persistant_stats().eips_completeonlinematches());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ConcentratedFireKills", protoData.persistant_stats().eips_concentratedfirekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CrashSiteAttackingKills", protoData.persistant_stats().eips_crashsiteattackingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CrashSiteDefendingKills", protoData.persistant_stats().eips_crashsitedefendingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CrouchedKills", protoData.persistant_stats().eips_crouchedkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CrouchedMeleeKills", protoData.persistant_stats().eips_crouchedmeleekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_CrouchingOverCorpses", protoData.persistant_stats().eips_crouchingovercorpses());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DeathsNoSuit", protoData.persistant_stats().eips_deathsnosuit());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DeathsSuitArmor", protoData.persistant_stats().eips_deathssuitarmor());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DeathsSuitDefault", protoData.persistant_stats().eips_deathssuitdefault());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DeathsSuitStealth", protoData.persistant_stats().eips_deathssuitstealth());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DetonationDelays", protoData.persistant_stats().eips_detonationdelays());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DogtagsCollected", protoData.persistant_stats().eips_dogtagscollected());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DogtagsUnlocked", protoData.persistant_stats().eips_dogtagsunlocked());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DoubleKills", protoData.persistant_stats().eips_doublekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_DualWeaponKills", protoData.persistant_stats().eips_dualweaponkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ECMKills", protoData.persistant_stats().eips_ecmkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_EnergyLeechKills", protoData.persistant_stats().eips_energyleechkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_EnhancedVisorKills", protoData.persistant_stats().eips_enhancedvisorkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ExtractionDefendingKills", protoData.persistant_stats().eips_extractiondefendingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FinalIntel5SecRemaining", protoData.persistant_stats().eips_finalintel5secremaining());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FirstBlood", protoData.persistant_stats().eips_firstblood());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FlagCaptures", protoData.persistant_stats().eips_flagcaptures());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FlagCarrierKills", protoData.persistant_stats().eips_flagcarrierkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FlushedKills", protoData.persistant_stats().eips_flushedkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_FriendlyFires", protoData.persistant_stats().eips_friendlyfires());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GameComplete", protoData.persistant_stats().eips_gamecomplete());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GameOverAfter2Before4", protoData.persistant_stats().eips_gameoverafter2before4());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GotYourBackKills", protoData.persistant_stats().eips_gotyourbackkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GrabAndThrow", protoData.persistant_stats().eips_grabandthrow());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GrenadeLauncherKills", protoData.persistant_stats().eips_grenadelauncherkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GrenadeSurvivals", protoData.persistant_stats().eips_grenadesurvivals());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GuardianKills", protoData.persistant_stats().eips_guardiankills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_GunslingerKills", protoData.persistant_stats().eips_gunslingerkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_HeadShotKills", protoData.persistant_stats().eips_headshotkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_HealthRestore", protoData.persistant_stats().eips_healthrestore());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_HeavyArmsKills", protoData.persistant_stats().eips_heavyarmskills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_HologramKills", protoData.persistant_stats().eips_hologramkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_InAirDeaths", protoData.persistant_stats().eips_inairdeaths());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_InAirGrenadeKills", protoData.persistant_stats().eips_inairgrenadekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_InterventionKills", protoData.persistant_stats().eips_interventionkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KickedCarKills", protoData.persistant_stats().eips_kickedcarkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillAllAssaultAttackers", protoData.persistant_stats().eips_killallassaultattackers());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillAssists", protoData.persistant_stats().eips_killassists());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillJoyKills", protoData.persistant_stats().eips_killjoykills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KilledAllEnemies", protoData.persistant_stats().eips_killedallenemies());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KilledAllEnemiesNotDied", protoData.persistant_stats().eips_killedallenemiesnotdied());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillsNoSuit", protoData.persistant_stats().eips_killsnosuit());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillsSuitArmor", protoData.persistant_stats().eips_killssuitarmor());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillsSuitDefault", protoData.persistant_stats().eips_killssuitdefault());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillsSuitStealth", protoData.persistant_stats().eips_killssuitstealth());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_KillsWithoutAssist", protoData.persistant_stats().eips_killswithoutassist());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_LaserSightKills", protoData.persistant_stats().eips_lasersightkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_LedgeGrabs", protoData.persistant_stats().eips_ledgegrabs());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_LeetHeadshots", protoData.persistant_stats().eips_leetheadshots());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_LightShotgunKills", protoData.persistant_stats().eips_lightshotgunkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_LoneWolfKills", protoData.persistant_stats().eips_lonewolfkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MaxFocusKills", protoData.persistant_stats().eips_maxfocuskills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MeleeDeaths", protoData.persistant_stats().eips_meleedeaths());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MeleeTakeDownKills", protoData.persistant_stats().eips_meleetakedownkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MeleeTakeDownKillsNoAmmo", protoData.persistant_stats().eips_meleetakedownkillsnoammo());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MicrowaveBeam", protoData.persistant_stats().eips_microwavebeam());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MicrowaveBeamKills", protoData.persistant_stats().eips_microwavebeamkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MobilityKills", protoData.persistant_stats().eips_mobilitykills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_MountedKills", protoData.persistant_stats().eips_mountedkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_NearDeathExperienceKills", protoData.persistant_stats().eips_neardeathexperiencekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_NumCloakedVictimKills", protoData.persistant_stats().eips_numcloakedvictimkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ObjectiveScore", protoData.persistant_stats().eips_objectivescore());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_OneHitArmorMeleeKills", protoData.persistant_stats().eips_onehitarmormeleekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_PhantomKills", protoData.persistant_stats().eips_phantomkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_PiercingKills", protoData.persistant_stats().eips_piercingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ProximityKills", protoData.persistant_stats().eips_proximitykills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_QuadKills", protoData.persistant_stats().eips_quadkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_QuinKills", protoData.persistant_stats().eips_quinkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RadarJammer", protoData.persistant_stats().eips_radarjammer());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RecoveryKills", protoData.persistant_stats().eips_recoverykills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ReflexSightKills", protoData.persistant_stats().eips_reflexsightkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RetaliationKills", protoData.persistant_stats().eips_retaliationkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RipOffMountedWeapon", protoData.persistant_stats().eips_ripoffmountedweapon());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RumbledKills", protoData.persistant_stats().eips_rumbledkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_RunOver", protoData.persistant_stats().eips_runover());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SafetyInNumbersKills", protoData.persistant_stats().eips_safetyinnumberskills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ScanObject", protoData.persistant_stats().eips_scanobject());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SlidingKills", protoData.persistant_stats().eips_slidingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ShotInBack", protoData.persistant_stats().eips_shotinback());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ShotsInMyBack", protoData.persistant_stats().eips_shotsinmyback());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SidePackKills", protoData.persistant_stats().eips_sidepackkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SilentFeetKills", protoData.persistant_stats().eips_silentfeetkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SingleExplosionKills", protoData.persistant_stats().eips_singleexplosionkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SkillKills", protoData.persistant_stats().eips_skillkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SnipedFoot", protoData.persistant_stats().eips_snipedfoot());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SniperScopeKills", protoData.persistant_stats().eips_sniperscopekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_StealthActivations", protoData.persistant_stats().eips_stealthactivations());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_StealthKills", protoData.persistant_stats().eips_stealthkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_Suicides", protoData.persistant_stats().eips_suicides());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuicidesByFalling", protoData.persistant_stats().eips_suicidesbyfalling());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuicidesByFallingCollided", protoData.persistant_stats().eips_suicidesbyfallingcollided());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuicidesByFrag", protoData.persistant_stats().eips_suicidesbyfrag());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuitBoost", protoData.persistant_stats().eips_suitboost());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuitBoostKills", protoData.persistant_stats().eips_suitboostkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuitDisruptorAssists", protoData.persistant_stats().eips_suitdisruptorassists());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuitDisruptor", protoData.persistant_stats().eips_suitdisruptor());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_SuppressorKills", protoData.persistant_stats().eips_suppressorkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TagAssist", protoData.persistant_stats().eips_tagassist());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TaggedAndBagged", protoData.persistant_stats().eips_taggedandbagged());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TaggedEntities", protoData.persistant_stats().eips_taggedentities());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TeamRadar", protoData.persistant_stats().eips_teamradar());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ThreatDetectorKills", protoData.persistant_stats().eips_threatdetectorkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_ThrownObjectKill", protoData.persistant_stats().eips_thrownobjectkill());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TrackerKills", protoData.persistant_stats().eips_trackerkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_TripleKills", protoData.persistant_stats().eips_triplekills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_UnderTheRadarKills", protoData.persistant_stats().eips_undertheradarkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_UnmountedKills", protoData.persistant_stats().eips_unmountedkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_UnsilencedShots", protoData.persistant_stats().eips_unsilencedshots());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_VictimOnFinalKillcam", protoData.persistant_stats().eips_victimonfinalkillcam());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WarBirdKills", protoData.persistant_stats().eips_warbirdkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WeaponsTrainingKills", protoData.persistant_stats().eips_weaponstrainingkills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WinningKill", protoData.persistant_stats().eips_winningkill());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WinningKillAndFirstBlood", protoData.persistant_stats().eips_winningkillandfirstblood());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WonCTFWithoutGivingUpAScore", protoData.persistant_stats().eips_wonctfwithoutgivingupascore());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EIPS_WonExtractDefendingNoGiveUp", protoData.persistant_stats().eips_wonextractdefendingnogiveup());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_CloakedNearEnemy", protoData.persistant_stats().efps_cloakednearenemy());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_CrashSiteHeldTime", protoData.persistant_stats().efps_crashsiteheldtime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DamageDelt", protoData.persistant_stats().efps_damagedelt());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DamageTaken", protoData.persistant_stats().efps_damagetaken());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DistanceAir", protoData.persistant_stats().efps_distanceair());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DistanceRan", protoData.persistant_stats().efps_distanceran());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DistanceSlid", protoData.persistant_stats().efps_distanceslid());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DistanceSprint", protoData.persistant_stats().efps_distancesprint());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_DistanceSwumPier", protoData.persistant_stats().efps_distanceswumpier());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_EnergyUsed", protoData.persistant_stats().efps_energyused());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_FallDistance", protoData.persistant_stats().efps_falldistance());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_FlagCarriedTime", protoData.persistant_stats().efps_flagcarriedtime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_IntelCollectedTime", protoData.persistant_stats().efps_intelcollectedtime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_KillCamTime", protoData.persistant_stats().efps_killcamtime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_LobbyTime", protoData.persistant_stats().efps_lobbytime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_NoSuitTime", protoData.persistant_stats().efps_nosuittime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_Overall", protoData.persistant_stats().efps_overall());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_SuitArmorTime", protoData.persistant_stats().efps_suitarmortime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_SuitDefaultTime", protoData.persistant_stats().efps_suitdefaulttime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_SuitStealthTime", protoData.persistant_stats().efps_suitstealthtime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_TimeCrouched", protoData.persistant_stats().efps_timecrouched());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_TimePlayed", protoData.persistant_stats().efps_timeplayed());
		pPlayerProfile->SetAttribute("MP/PersistantStats/EFPS_VisorActiveTime", protoData.persistant_stats().efps_visoractivetime());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESFPS_DistanceAir", protoData.persistant_stats().esfps_distanceair());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESFPS_HeightOnRooftops", protoData.persistant_stats().esfps_heightonrooftops());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESFPS_TimeAlive", protoData.persistant_stats().esfps_timealive());

		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_AssaultAttackersKilled", protoData.persistant_stats().esips_assaultattackerskilled());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_Deaths", protoData.persistant_stats().esips_deaths());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_GameOverLateAndLowScore", protoData.persistant_stats().esips_gameoverlateandlowscore());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_Headshots", protoData.persistant_stats().esips_headshots());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_HeadshotsPerLife", protoData.persistant_stats().esips_headshotsperlife());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_HealthRestoresPerLife", protoData.persistant_stats().esips_healthrestoresperlife());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_Kills", protoData.persistant_stats().esips_kills());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_KillsNoReloadWeapChange", protoData.persistant_stats().esips_killsnoreloadweapchange());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_Lose", protoData.persistant_stats().esips_lose());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_MultiKillStreak", protoData.persistant_stats().esips_multikillstreak());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_OnlineRankedWin", protoData.persistant_stats().esips_onlinerankedwin());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_Win", protoData.persistant_stats().esips_win());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_MeleeKillsThisSession", protoData.persistant_stats().esips_meleekillsthissession());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_HeadshotKillsPerLife", protoData.persistant_stats().esips_headshotkillsperlife());
		pPlayerProfile->SetAttribute("MP/PersistantStats/ESIPS_HeadshotKillsPerMatch", protoData.persistant_stats().esips_headshotkillspermatch());

		//Gamemodes	EMPS_Gamemodes
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/AllOrNothing", protoData.persistant_stats().emps_gamemodes().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/Assault", protoData.persistant_stats().emps_gamemodes().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/BombTheBase", protoData.persistant_stats().emps_gamemodes().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/CaptureTheFlag", protoData.persistant_stats().emps_gamemodes().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/Countdown", protoData.persistant_stats().emps_gamemodes().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/CrashSite", protoData.persistant_stats().emps_gamemodes().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/Extraction", protoData.persistant_stats().emps_gamemodes().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/InstantAction", protoData.persistant_stats().emps_gamemodes().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/PowerStruggleLite", protoData.persistant_stats().emps_gamemodes().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_Gamemodes/TeamInstantAction", protoData.persistant_stats().emps_gamemodes().teaminstantaction());
		}
		//Gamemodes	EMPS_GamemodesTime
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/AllOrNothing", protoData.persistant_stats().emps_gamemodestime().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Assault", protoData.persistant_stats().emps_gamemodestime().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/BombTheBase", protoData.persistant_stats().emps_gamemodestime().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/CaptureTheFlag", protoData.persistant_stats().emps_gamemodestime().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Countdown", protoData.persistant_stats().emps_gamemodestime().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/CrashSite", protoData.persistant_stats().emps_gamemodestime().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Extraction", protoData.persistant_stats().emps_gamemodestime().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/InstantAction", protoData.persistant_stats().emps_gamemodestime().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/PowerStruggleLite", protoData.persistant_stats().emps_gamemodestime().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesTime/TeamInstantAction", protoData.persistant_stats().emps_gamemodestime().teaminstantaction());
		}
		//Gamemodes	EMPS_GamemodesMVP
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/AllOrNothing", protoData.persistant_stats().emps_gamemodesmvp().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Assault", protoData.persistant_stats().emps_gamemodesmvp().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/BombTheBase", protoData.persistant_stats().emps_gamemodesmvp().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/CaptureTheFlag", protoData.persistant_stats().emps_gamemodesmvp().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Countdown", protoData.persistant_stats().emps_gamemodesmvp().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/CrashSite", protoData.persistant_stats().emps_gamemodesmvp().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Extraction", protoData.persistant_stats().emps_gamemodesmvp().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/InstantAction", protoData.persistant_stats().emps_gamemodesmvp().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/PowerStruggleLite", protoData.persistant_stats().emps_gamemodesmvp().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/TeamInstantAction", protoData.persistant_stats().emps_gamemodesmvp().teaminstantaction());
		}
		//Gamemodes	EMPS_GamemodesHighscore
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/AllOrNothing", protoData.persistant_stats().emps_gamemodeshighscore().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Assault", protoData.persistant_stats().emps_gamemodeshighscore().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/BombTheBase", protoData.persistant_stats().emps_gamemodeshighscore().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/CaptureTheFlag", protoData.persistant_stats().emps_gamemodeshighscore().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Countdown", protoData.persistant_stats().emps_gamemodeshighscore().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/CrashSite", protoData.persistant_stats().emps_gamemodeshighscore().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Extraction", protoData.persistant_stats().emps_gamemodeshighscore().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/InstantAction", protoData.persistant_stats().emps_gamemodeshighscore().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/PowerStruggleLite", protoData.persistant_stats().emps_gamemodeshighscore().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/TeamInstantAction", protoData.persistant_stats().emps_gamemodeshighscore().teaminstantaction());
		}
		//Gamemodes	EMPS_GamesDrawn
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/AllOrNothing", protoData.persistant_stats().emps_gamesdrawn().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Assault", protoData.persistant_stats().emps_gamesdrawn().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/BombTheBase", protoData.persistant_stats().emps_gamesdrawn().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/CaptureTheFlag", protoData.persistant_stats().emps_gamesdrawn().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Countdown", protoData.persistant_stats().emps_gamesdrawn().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/CrashSite", protoData.persistant_stats().emps_gamesdrawn().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Extraction", protoData.persistant_stats().emps_gamesdrawn().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/InstantAction", protoData.persistant_stats().emps_gamesdrawn().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/PowerStruggleLite", protoData.persistant_stats().emps_gamesdrawn().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesDrawn/TeamInstantAction", protoData.persistant_stats().emps_gamesdrawn().teaminstantaction());
		}
		//Gamemodes	EMPS_GamesLost
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/AllOrNothing", protoData.persistant_stats().emps_gameslost().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/Assault", protoData.persistant_stats().emps_gameslost().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/BombTheBase", protoData.persistant_stats().emps_gameslost().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/CaptureTheFlag", protoData.persistant_stats().emps_gameslost().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/Countdown", protoData.persistant_stats().emps_gameslost().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/CrashSite", protoData.persistant_stats().emps_gameslost().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/Extraction", protoData.persistant_stats().emps_gameslost().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/InstantAction", protoData.persistant_stats().emps_gameslost().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/PowerStruggleLite", protoData.persistant_stats().emps_gameslost().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesLost/TeamInstantAction", protoData.persistant_stats().emps_gameslost().teaminstantaction());
		}
		//Gamemodes	EMPS_GamesWon
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/AllOrNothing", protoData.persistant_stats().emps_gameswon().allornothing());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/Assault", protoData.persistant_stats().emps_gameswon().assault());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/BombTheBase", protoData.persistant_stats().emps_gameswon().bombthebase());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/CaptureTheFlag", protoData.persistant_stats().emps_gameswon().capturetheflag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/Countdown", protoData.persistant_stats().emps_gameswon().countdown());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/CrashSite", protoData.persistant_stats().emps_gameswon().crashsite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/Extraction", protoData.persistant_stats().emps_gameswon().extraction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/InstantAction", protoData.persistant_stats().emps_gameswon().instantaction());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/PowerStruggleLite", protoData.persistant_stats().emps_gameswon().powerstrugglelite());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_GamesWon/TeamInstantAction", protoData.persistant_stats().emps_gameswon().teaminstantaction());
		}

		pPlayerProfile->SetAttribute("MP/PersistantStats/DateFirstPlayed/Year", protoData.persistant_stats().emps_datefirstplayed().year());
		pPlayerProfile->SetAttribute("MP/PersistantStats/DateFirstPlayed/Month", protoData.persistant_stats().emps_datefirstplayed().month());
		pPlayerProfile->SetAttribute("MP/PersistantStats/DateFirstPlayed/Day", protoData.persistant_stats().emps_datefirstplayed().day());

		//Weapons EMPS_WeaponHeadshotKills
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/AY69", protoData.persistant_stats().emps_weaponheadshotkills().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/C4", protoData.persistant_stats().emps_weaponheadshotkills().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/DSG1", protoData.persistant_stats().emps_weaponheadshotkills().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Feline", protoData.persistant_stats().emps_weaponheadshotkills().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/FlashBangGrenades", protoData.persistant_stats().emps_weaponheadshotkills().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/FragGrenades", protoData.persistant_stats().emps_weaponheadshotkills().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Gauss", protoData.persistant_stats().emps_weaponheadshotkills().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Grendel", protoData.persistant_stats().emps_weaponheadshotkills().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Hammer", protoData.persistant_stats().emps_weaponheadshotkills().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/JAW", protoData.persistant_stats().emps_weaponheadshotkills().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Jackal", protoData.persistant_stats().emps_weaponheadshotkills().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/K_Volt", protoData.persistant_stats().emps_weaponheadshotkills().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/LTag", protoData.persistant_stats().emps_weaponheadshotkills().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Marshall", protoData.persistant_stats().emps_weaponheadshotkills().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Mk60", protoData.persistant_stats().emps_weaponheadshotkills().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Nova", protoData.persistant_stats().emps_weaponheadshotkills().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Revolver", protoData.persistant_stats().emps_weaponheadshotkills().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/SCAR", protoData.persistant_stats().emps_weaponheadshotkills().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/SCARAB", protoData.persistant_stats().emps_weaponheadshotkills().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/mike", protoData.persistant_stats().emps_weaponheadshotkills().mike());
		}
		//Weapons EMPS_WeaponHeadshots
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/AY69", protoData.persistant_stats().emps_weaponheadshots().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/C4", protoData.persistant_stats().emps_weaponheadshots().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/DSG1", protoData.persistant_stats().emps_weaponheadshots().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Feline", protoData.persistant_stats().emps_weaponheadshots().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/FlashBangGrenades", protoData.persistant_stats().emps_weaponheadshots().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/FragGrenades", protoData.persistant_stats().emps_weaponheadshots().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Gauss", protoData.persistant_stats().emps_weaponheadshots().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Grendel", protoData.persistant_stats().emps_weaponheadshots().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Hammer", protoData.persistant_stats().emps_weaponheadshots().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/JAW", protoData.persistant_stats().emps_weaponheadshots().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Jackal", protoData.persistant_stats().emps_weaponheadshots().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/K_Volt", protoData.persistant_stats().emps_weaponheadshots().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/LTag", protoData.persistant_stats().emps_weaponheadshots().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Marshall", protoData.persistant_stats().emps_weaponheadshots().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Mk60", protoData.persistant_stats().emps_weaponheadshots().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Nova", protoData.persistant_stats().emps_weaponheadshots().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Revolver", protoData.persistant_stats().emps_weaponheadshots().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/SCAR", protoData.persistant_stats().emps_weaponheadshots().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/SCARAB", protoData.persistant_stats().emps_weaponheadshots().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/mike", protoData.persistant_stats().emps_weaponheadshots().mike());
		}
		//Weapons EMPS_WeaponHits
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/AY69", protoData.persistant_stats().emps_weaponhits().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/C4", protoData.persistant_stats().emps_weaponhits().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/DSG1", protoData.persistant_stats().emps_weaponhits().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Feline", protoData.persistant_stats().emps_weaponhits().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/FlashBangGrenades", protoData.persistant_stats().emps_weaponhits().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/FragGrenades", protoData.persistant_stats().emps_weaponhits().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Gauss", protoData.persistant_stats().emps_weaponhits().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Grendel", protoData.persistant_stats().emps_weaponhits().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Hammer", protoData.persistant_stats().emps_weaponhits().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/JAW", protoData.persistant_stats().emps_weaponhits().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Jackal", protoData.persistant_stats().emps_weaponhits().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/K_Volt", protoData.persistant_stats().emps_weaponhits().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/LTag", protoData.persistant_stats().emps_weaponhits().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Marshall", protoData.persistant_stats().emps_weaponhits().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Mk60", protoData.persistant_stats().emps_weaponhits().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Nova", protoData.persistant_stats().emps_weaponhits().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/Revolver", protoData.persistant_stats().emps_weaponhits().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/SCAR", protoData.persistant_stats().emps_weaponhits().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/SCARAB", protoData.persistant_stats().emps_weaponhits().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponHits/mike", protoData.persistant_stats().emps_weaponhits().mike());
		}
		//Weapons EMPS_WeaponKills
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/AY69", protoData.persistant_stats().emps_weaponkills().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/C4", protoData.persistant_stats().emps_weaponkills().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/DSG1", protoData.persistant_stats().emps_weaponkills().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Feline", protoData.persistant_stats().emps_weaponkills().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/FlashBangGrenades", protoData.persistant_stats().emps_weaponkills().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/FragGrenades", protoData.persistant_stats().emps_weaponkills().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Gauss", protoData.persistant_stats().emps_weaponkills().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Grendel", protoData.persistant_stats().emps_weaponkills().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Hammer", protoData.persistant_stats().emps_weaponkills().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/JAW", protoData.persistant_stats().emps_weaponkills().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Jackal", protoData.persistant_stats().emps_weaponkills().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/K_Volt", protoData.persistant_stats().emps_weaponkills().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/LTag", protoData.persistant_stats().emps_weaponkills().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Marshall", protoData.persistant_stats().emps_weaponkills().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Mk60", protoData.persistant_stats().emps_weaponkills().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Nova", protoData.persistant_stats().emps_weaponkills().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/Revolver", protoData.persistant_stats().emps_weaponkills().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/SCAR", protoData.persistant_stats().emps_weaponkills().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/SCARAB", protoData.persistant_stats().emps_weaponkills().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponKills/mike", protoData.persistant_stats().emps_weaponkills().mike());
		}
		//Weapons EMPS_WeaponShots
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/AY69", protoData.persistant_stats().emps_weaponshots().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/C4", protoData.persistant_stats().emps_weaponshots().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/DSG1", protoData.persistant_stats().emps_weaponshots().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Feline", protoData.persistant_stats().emps_weaponshots().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/FlashBangGrenades", protoData.persistant_stats().emps_weaponshots().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/FragGrenades", protoData.persistant_stats().emps_weaponshots().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Gauss", protoData.persistant_stats().emps_weaponshots().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Grendel", protoData.persistant_stats().emps_weaponshots().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Hammer", protoData.persistant_stats().emps_weaponshots().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/JAW", protoData.persistant_stats().emps_weaponshots().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Jackal", protoData.persistant_stats().emps_weaponshots().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/K_Volt", protoData.persistant_stats().emps_weaponshots().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/LTag", protoData.persistant_stats().emps_weaponshots().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Marshall", protoData.persistant_stats().emps_weaponshots().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Mk60", protoData.persistant_stats().emps_weaponshots().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Nova", protoData.persistant_stats().emps_weaponshots().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/Revolver", protoData.persistant_stats().emps_weaponshots().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/SCAR", protoData.persistant_stats().emps_weaponshots().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/SCARAB", protoData.persistant_stats().emps_weaponshots().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponShots/mike", protoData.persistant_stats().emps_weaponshots().mike());
		}
		//Weapons EMPS_WeaponTime
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/AY69", protoData.persistant_stats().emps_weapontime().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/C4", protoData.persistant_stats().emps_weapontime().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/DSG1", protoData.persistant_stats().emps_weapontime().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Feline", protoData.persistant_stats().emps_weapontime().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/FlashBangGrenades", protoData.persistant_stats().emps_weapontime().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/FragGrenades", protoData.persistant_stats().emps_weapontime().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Gauss", protoData.persistant_stats().emps_weapontime().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Grendel", protoData.persistant_stats().emps_weapontime().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Hammer", protoData.persistant_stats().emps_weapontime().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/JAW", protoData.persistant_stats().emps_weapontime().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Jackal", protoData.persistant_stats().emps_weapontime().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/K_Volt", protoData.persistant_stats().emps_weapontime().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/LTag", protoData.persistant_stats().emps_weapontime().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Marshall", protoData.persistant_stats().emps_weapontime().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Mk60", protoData.persistant_stats().emps_weapontime().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Nova", protoData.persistant_stats().emps_weapontime().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/Revolver", protoData.persistant_stats().emps_weapontime().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/SCAR", protoData.persistant_stats().emps_weapontime().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/SCARAB", protoData.persistant_stats().emps_weapontime().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponTime/mike", protoData.persistant_stats().emps_weapontime().mike());
		}
		//Weapons EMPS_WeaponUnlocked
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/AY69", protoData.persistant_stats().emps_weaponunlocked().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/C4", protoData.persistant_stats().emps_weaponunlocked().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/DSG1", protoData.persistant_stats().emps_weaponunlocked().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Feline",  protoData.persistant_stats().emps_weaponunlocked().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/FlashBangGrenades",  protoData.persistant_stats().emps_weaponunlocked().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/FragGrenades",  protoData.persistant_stats().emps_weaponunlocked().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Gauss",  protoData.persistant_stats().emps_weaponunlocked().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Grendel",  protoData.persistant_stats().emps_weaponunlocked().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Hammer",  protoData.persistant_stats().emps_weaponunlocked().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/JAW",  protoData.persistant_stats().emps_weaponunlocked().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Jackal",  protoData.persistant_stats().emps_weaponunlocked().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/K_Volt",  protoData.persistant_stats().emps_weaponunlocked().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/LTag",  protoData.persistant_stats().emps_weaponunlocked().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Marshall",  protoData.persistant_stats().emps_weaponunlocked().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Mk60",  protoData.persistant_stats().emps_weaponunlocked().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Nova",  protoData.persistant_stats().emps_weaponunlocked().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Revolver",  protoData.persistant_stats().emps_weaponunlocked().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/SCAR",  protoData.persistant_stats().emps_weaponunlocked().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/SCARAB",  protoData.persistant_stats().emps_weaponunlocked().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/mike",  protoData.persistant_stats().emps_weaponunlocked().mike());
		}
		//Weapons EMPS_WeaponUsage
		{
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/AY69", protoData.persistant_stats().emps_weaponusage().ay69());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/C4", protoData.persistant_stats().emps_weaponusage().c4());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/DSG1", protoData.persistant_stats().emps_weaponusage().dsg1());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Feline", protoData.persistant_stats().emps_weaponusage().feline());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/FlashBangGrenades", protoData.persistant_stats().emps_weaponusage().flashbanggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/FragGrenades", protoData.persistant_stats().emps_weaponusage().fraggrenades());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Gauss", protoData.persistant_stats().emps_weaponusage().gauss());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Grendel", protoData.persistant_stats().emps_weaponusage().grendel());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Hammer", protoData.persistant_stats().emps_weaponusage().hammer());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/JAW", protoData.persistant_stats().emps_weaponusage().jaw());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Jackal", protoData.persistant_stats().emps_weaponusage().jackal());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/K_Volt", protoData.persistant_stats().emps_weaponusage().k_volt());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/LTag", protoData.persistant_stats().emps_weaponusage().ltag());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Marshall", protoData.persistant_stats().emps_weaponusage().marshall());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Mk60", protoData.persistant_stats().emps_weaponusage().mk60());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Nova", protoData.persistant_stats().emps_weaponusage().nova());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Revolver", protoData.persistant_stats().emps_weaponusage().revolver());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/SCAR", protoData.persistant_stats().emps_weaponusage().scar());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/SCARAB", protoData.persistant_stats().emps_weaponusage().scarab());
			pPlayerProfile->SetAttribute("MP/PersistantStats/EMPS_WeaponUsage/mike", protoData.persistant_stats().emps_weaponusage().mike());
		}
	}

	m_pProfileManager->ReloadProfile(pPlayerProfile, EProfileReasons::ePR_All);
}

CrysisProto::CryOnlineAttributes CCryProfiles::GetOnlineAttributes(IPlayerProfile* pPlayerProfile)
{
	int32 iValue = 0;
	float fValue = 0.0f;

	CrysisProto::CryOnlineAttributes onlineData;

	CrysisProto::PlayerProgression* pPlayerProgression = onlineData.mutable_player_progression();
	pPlayerProgression->Clear();

	// <!-- Player Progression -->
	{
		pPlayerProfile->GetAttribute("MP/PlayerProgression/XP", iValue);
		pPlayerProgression->set_xp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/LifetimeXP", iValue);
		pPlayerProgression->set_livetimexp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/Reincarnate", iValue);
		pPlayerProgression->set_reincarnate(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/DefaultXP", iValue);
		pPlayerProgression->set_defaultxp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/ArmorXP", iValue);
		pPlayerProgression->set_armorxp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/StealthXP", iValue);
		pPlayerProgression->set_stealthxp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/DefaultLifetimeXP", iValue);
		pPlayerProgression->set_defaultlifetimexp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/ArmorLifetimeXP", iValue);
		pPlayerProgression->set_armorlifetimexp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/StealthLifetimeXP", iValue);
		pPlayerProgression->set_stealthlifetimexp(iValue);

		pPlayerProfile->GetAttribute("MP/PlayerProgression/SkillRank", iValue);
		pPlayerProgression->set_skillrank(iValue);
	}

	CrysisProto::PersistantStats* pPersistantStats = onlineData.mutable_persistant_stats();
	pPersistantStats->Clear();
	//<!-- Persistant Stats -->
	{
		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_3DogtagsFrom1Player", iValue);
		pPersistantStats->set_eips_3dogtagsfrom1player(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_3FastGrenadeKills", iValue);
		pPersistantStats->set_eips_3fastgrenadekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_5HealthRestoresInOneLife", iValue);
		pPersistantStats->set_eips_5healthrestoresinonelife(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AcceleratorKills", iValue);
		pPersistantStats->set_eips_acceleratorkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AirDeathKills", iValue);
		pPersistantStats->set_eips_airdeathkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AirHeadshots", iValue);
		pPersistantStats->set_eips_airheadshots(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AirKillKills", iValue);
		pPersistantStats->set_eips_airkillkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AlienEnergyCollected", iValue);
		pPersistantStats->set_eips_alienenergycollected(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AlienGunship", iValue);
		pPersistantStats->set_eips_aliengunship(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AlienGunshipKills", iValue);
		pPersistantStats->set_eips_aliengunshipkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AlienTicksExtracted", iValue);
		pPersistantStats->set_eips_alienticksextracted(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AllEnemyDogTagsGot", iValue);
		pPersistantStats->set_eips_allenemydogtagsgot(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ArmorActivations", iValue);
		pPersistantStats->set_eips_armoractivations(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ArmorEfficiencyKills", iValue);
		pPersistantStats->set_eips_armorefficiencykills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ArmourHits", iValue);
		pPersistantStats->set_eips_armourhits(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AssaultDefendingKills", iValue);
		pPersistantStats->set_eips_assaultdefendingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AssaultKillLastAttack5pc", iValue);
		pPersistantStats->set_eips_assaultkilllastattack5pc(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_AssaultScopeKills", iValue);
		pPersistantStats->set_eips_assaultscopekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_BlindKills", iValue);
		pPersistantStats->set_eips_blindkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_BlindSelf", iValue);
		pPersistantStats->set_eips_blindself(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_BlindingKills", iValue);
		pPersistantStats->set_eips_blindingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_BombTheBase", iValue);
		pPersistantStats->set_eips_bombthebase(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_BulletPenetrationKills", iValue);
		pPersistantStats->set_eips_bulletpenetrationkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CaptureObjectives", iValue);
		pPersistantStats->set_eips_captureobjectives(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CarryObjectives", iValue);
		pPersistantStats->set_eips_carryobjectives(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CloakAwarenessKills", iValue);
		pPersistantStats->set_eips_cloakawarenesskills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CloakedReloads", iValue);
		pPersistantStats->set_eips_cloakedreloads(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CloakedWatchNearbyKill", iValue);
		pPersistantStats->set_eips_cloakedwatchnearbykill(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CompleteOnlineMatches", iValue);
		pPersistantStats->set_eips_completeonlinematches(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ConcentratedFireKills", iValue);
		pPersistantStats->set_eips_concentratedfirekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CrashSiteAttackingKills", iValue);
		pPersistantStats->set_eips_crashsiteattackingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CrashSiteDefendingKills", iValue);
		pPersistantStats->set_eips_crashsitedefendingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CrouchedKills", iValue);
		pPersistantStats->set_eips_crouchedkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CrouchedMeleeKills", iValue);
		pPersistantStats->set_eips_crouchedmeleekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_CrouchingOverCorpses", iValue);
		pPersistantStats->set_eips_crouchingovercorpses(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DeathsNoSuit", iValue);
		pPersistantStats->set_eips_deathsnosuit(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DeathsSuitArmor", iValue);
		pPersistantStats->set_eips_deathssuitarmor(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DeathsSuitDefault", iValue);
		pPersistantStats->set_eips_deathssuitdefault(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DeathsSuitStealth", iValue);
		pPersistantStats->set_eips_deathssuitstealth(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DetonationDelays", iValue);
		pPersistantStats->set_eips_detonationdelays(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DogtagsCollected", iValue);
		pPersistantStats->set_eips_dogtagscollected(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DogtagsUnlocked", iValue);
		pPersistantStats->set_eips_dogtagsunlocked(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DoubleKills", iValue);
		pPersistantStats->set_eips_doublekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_DualWeaponKills", iValue);
		pPersistantStats->set_eips_dualweaponkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ECMKills", iValue);
		pPersistantStats->set_eips_ecmkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_EnergyLeechKills", iValue);
		pPersistantStats->set_eips_energyleechkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_EnhancedVisorKills", iValue);
		pPersistantStats->set_eips_enhancedvisorkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ExtractionDefendingKills", iValue);
		pPersistantStats->set_eips_extractiondefendingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FinalIntel5SecRemaining", iValue);
		pPersistantStats->set_eips_finalintel5secremaining(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FirstBlood", iValue);
		pPersistantStats->set_eips_firstblood(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FlagCaptures", iValue);
		pPersistantStats->set_eips_flagcaptures(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FlagCarrierKills", iValue);
		pPersistantStats->set_eips_flagcarrierkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FlushedKills", iValue);
		pPersistantStats->set_eips_flushedkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_FriendlyFires", iValue);
		pPersistantStats->set_eips_friendlyfires(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GameComplete", iValue);
		pPersistantStats->set_eips_gamecomplete(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GameOverAfter2Before4", iValue);
		pPersistantStats->set_eips_gameoverafter2before4(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GotYourBackKills", iValue);
		pPersistantStats->set_eips_gotyourbackkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GrabAndThrow", iValue);
		pPersistantStats->set_eips_grabandthrow(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GrenadeLauncherKills", iValue);
		pPersistantStats->set_eips_grenadelauncherkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GrenadeSurvivals", iValue);
		pPersistantStats->set_eips_grenadesurvivals(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GuardianKills", iValue);
		pPersistantStats->set_eips_guardiankills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_GunslingerKills", iValue);
		pPersistantStats->set_eips_gunslingerkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_HeadShotKills", iValue);
		pPersistantStats->set_eips_headshotkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_HealthRestore", iValue);
		pPersistantStats->set_eips_healthrestore(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_HeavyArmsKills", iValue);
		pPersistantStats->set_eips_heavyarmskills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_HologramKills", iValue);
		pPersistantStats->set_eips_hologramkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_InAirDeaths", iValue);
		pPersistantStats->set_eips_inairdeaths(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_InAirGrenadeKills", iValue);
		pPersistantStats->set_eips_inairgrenadekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_InterventionKills", iValue);
		pPersistantStats->set_eips_interventionkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KickedCarKills", iValue);
		pPersistantStats->set_eips_kickedcarkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillAllAssaultAttackers", iValue);
		pPersistantStats->set_eips_killallassaultattackers(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillAssists", iValue);
		pPersistantStats->set_eips_killassists(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillJoyKills", iValue);
		pPersistantStats->set_eips_killjoykills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KilledAllEnemies", iValue);
		pPersistantStats->set_eips_killedallenemies(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KilledAllEnemiesNotDied", iValue);
		pPersistantStats->set_eips_killedallenemiesnotdied(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillsNoSuit", iValue);
		pPersistantStats->set_eips_killsnosuit(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillsSuitArmor", iValue);
		pPersistantStats->set_eips_killssuitarmor(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillsSuitDefault", iValue);
		pPersistantStats->set_eips_killssuitdefault(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillsSuitStealth", iValue);
		pPersistantStats->set_eips_killssuitstealth(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_KillsWithoutAssist", iValue);
		pPersistantStats->set_eips_killswithoutassist(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_LaserSightKills", iValue);
		pPersistantStats->set_eips_lasersightkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_LedgeGrabs", iValue);
		pPersistantStats->set_eips_ledgegrabs(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_LeetHeadshots", iValue);
		pPersistantStats->set_eips_leetheadshots(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_LightShotgunKills", iValue);
		pPersistantStats->set_eips_lightshotgunkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_LoneWolfKills", iValue);
		pPersistantStats->set_eips_lonewolfkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MaxFocusKills", iValue);
		pPersistantStats->set_eips_maxfocuskills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MeleeDeaths", iValue);
		pPersistantStats->set_eips_meleedeaths(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MeleeTakeDownKills", iValue);
		pPersistantStats->set_eips_meleetakedownkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MeleeTakeDownKillsNoAmmo", iValue);
		pPersistantStats->set_eips_meleetakedownkillsnoammo(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MicrowaveBeam", iValue);
		pPersistantStats->set_eips_microwavebeam(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MicrowaveBeamKills", iValue);
		pPersistantStats->set_eips_microwavebeamkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MobilityKills", iValue);
		pPersistantStats->set_eips_mobilitykills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_MountedKills", iValue);
		pPersistantStats->set_eips_mountedkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_NearDeathExperienceKills", iValue);
		pPersistantStats->set_eips_neardeathexperiencekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_NumCloakedVictimKills", iValue);
		pPersistantStats->set_eips_numcloakedvictimkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ObjectiveScore", iValue);
		pPersistantStats->set_eips_objectivescore(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_OneHitArmorMeleeKills", iValue);
		pPersistantStats->set_eips_onehitarmormeleekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_PhantomKills", iValue);
		pPersistantStats->set_eips_phantomkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_PiercingKills", iValue);
		pPersistantStats->set_eips_piercingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ProximityKills", iValue);
		pPersistantStats->set_eips_proximitykills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_QuadKills", iValue);
		pPersistantStats->set_eips_quadkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_QuinKills", iValue);
		pPersistantStats->set_eips_quinkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RadarJammer", iValue);
		pPersistantStats->set_eips_radarjammer(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RecoveryKills", iValue);
		pPersistantStats->set_eips_recoverykills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ReflexSightKills", iValue);
		pPersistantStats->set_eips_reflexsightkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RetaliationKills", iValue);
		pPersistantStats->set_eips_retaliationkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RipOffMountedWeapon", iValue);
		pPersistantStats->set_eips_ripoffmountedweapon(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RumbledKills", iValue);
		pPersistantStats->set_eips_rumbledkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_RunOver", iValue);
		pPersistantStats->set_eips_runover(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SafetyInNumbersKills", iValue);
		pPersistantStats->set_eips_safetyinnumberskills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ScanObject", iValue);
		pPersistantStats->set_eips_scanobject(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SlidingKills", iValue);
		pPersistantStats->set_eips_slidingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ShotInBack", iValue);
		pPersistantStats->set_eips_shotinback(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ShotsInMyBack", iValue);
		pPersistantStats->set_eips_shotsinmyback(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SidePackKills", iValue);
		pPersistantStats->set_eips_sidepackkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SilentFeetKills", iValue);
		pPersistantStats->set_eips_silentfeetkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SingleExplosionKills", iValue);
		pPersistantStats->set_eips_singleexplosionkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SkillKills", iValue);
		pPersistantStats->set_eips_skillkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SnipedFoot", iValue);
		pPersistantStats->set_eips_snipedfoot(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SniperScopeKills", iValue);
		pPersistantStats->set_eips_sniperscopekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_StealthActivations", iValue);
		pPersistantStats->set_eips_stealthactivations(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_StealthKills", iValue);
		pPersistantStats->set_eips_stealthkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_Suicides", iValue);
		pPersistantStats->set_eips_suicides(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuicidesByFalling", iValue);
		pPersistantStats->set_eips_suicidesbyfalling(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuicidesByFallingCollided", iValue);
		pPersistantStats->set_eips_suicidesbyfallingcollided(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuicidesByFrag", iValue);
		pPersistantStats->set_eips_suicidesbyfrag(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuitBoost", iValue);
		pPersistantStats->set_eips_suitboost(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuitBoostKills", iValue);
		pPersistantStats->set_eips_suitboostkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuitDisruptorAssists", iValue);
		pPersistantStats->set_eips_suitdisruptorassists(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuitDisruptor", iValue);
		pPersistantStats->set_eips_suitdisruptor(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_SuppressorKills", iValue);
		pPersistantStats->set_eips_suppressorkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TagAssist", iValue);
		pPersistantStats->set_eips_tagassist(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TaggedAndBagged", iValue);
		pPersistantStats->set_eips_taggedandbagged(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TaggedEntities", iValue);
		pPersistantStats->set_eips_taggedentities(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TeamRadar", iValue);
		pPersistantStats->set_eips_teamradar(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ThreatDetectorKills", iValue);
		pPersistantStats->set_eips_threatdetectorkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_ThrownObjectKill", iValue);
		pPersistantStats->set_eips_thrownobjectkill(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TrackerKills", iValue);
		pPersistantStats->set_eips_trackerkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_TripleKills", iValue);
		pPersistantStats->set_eips_triplekills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_UnderTheRadarKills", iValue);
		pPersistantStats->set_eips_undertheradarkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_UnmountedKills", iValue);
		pPersistantStats->set_eips_unmountedkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_UnsilencedShots", iValue);
		pPersistantStats->set_eips_unsilencedshots(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_VictimOnFinalKillcam", iValue);
		pPersistantStats->set_eips_victimonfinalkillcam(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WarBirdKills", iValue);
		pPersistantStats->set_eips_warbirdkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WeaponsTrainingKills", iValue);
		pPersistantStats->set_eips_weaponstrainingkills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WinningKill", iValue);
		pPersistantStats->set_eips_winningkill(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WinningKillAndFirstBlood", iValue);
		pPersistantStats->set_eips_winningkillandfirstblood(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WonCTFWithoutGivingUpAScore", iValue);
		pPersistantStats->set_eips_wonctfwithoutgivingupascore(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EIPS_WonExtractDefendingNoGiveUp", iValue);
		pPersistantStats->set_eips_wonextractdefendingnogiveup(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_CloakedNearEnemy", fValue);
		pPersistantStats->set_efps_cloakednearenemy(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_CrashSiteHeldTime", fValue);
		pPersistantStats->set_efps_crashsiteheldtime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DamageDelt", fValue);
		pPersistantStats->set_efps_damagedelt(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DamageTaken", fValue);
		pPersistantStats->set_efps_damagetaken(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DistanceAir", fValue);
		pPersistantStats->set_efps_distanceair(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DistanceRan", fValue);
		pPersistantStats->set_efps_distanceran(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DistanceSlid", fValue);
		pPersistantStats->set_efps_distanceslid(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DistanceSprint", fValue);
		pPersistantStats->set_efps_distancesprint(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_DistanceSwumPier", fValue);
		pPersistantStats->set_efps_distanceswumpier(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_EnergyUsed", fValue);
		pPersistantStats->set_efps_energyused(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_FallDistance", fValue);
		pPersistantStats->set_efps_falldistance(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_FlagCarriedTime", fValue);
		pPersistantStats->set_efps_flagcarriedtime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_IntelCollectedTime", fValue);
		pPersistantStats->set_efps_intelcollectedtime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_KillCamTime", fValue);
		pPersistantStats->set_efps_killcamtime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_LobbyTime", fValue);
		pPersistantStats->set_efps_lobbytime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_NoSuitTime", fValue);
		pPersistantStats->set_efps_nosuittime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_Overall", fValue);
		pPersistantStats->set_efps_overall(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_SuitArmorTime", fValue);
		pPersistantStats->set_efps_suitarmortime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_SuitDefaultTime", fValue);
		pPersistantStats->set_efps_suitdefaulttime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_SuitStealthTime", fValue);
		pPersistantStats->set_efps_suitstealthtime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_TimeCrouched", fValue);
		pPersistantStats->set_efps_timecrouched(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_TimePlayed", fValue);
		pPersistantStats->set_efps_timeplayed(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/EFPS_VisorActiveTime", fValue);
		pPersistantStats->set_efps_visoractivetime(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESFPS_DistanceAir", fValue);
		pPersistantStats->set_esfps_distanceair(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESFPS_HeightOnRooftops", fValue);
		pPersistantStats->set_esfps_heightonrooftops(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESFPS_TimeAlive", fValue);
		pPersistantStats->set_esfps_timealive(fValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_AssaultAttackersKilled", iValue);
		pPersistantStats->set_esips_assaultattackerskilled(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_Deaths", iValue);
		pPersistantStats->set_esips_deaths(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_GameOverLateAndLowScore", iValue);
		pPersistantStats->set_esips_gameoverlateandlowscore(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_Headshots", iValue);
		pPersistantStats->set_esips_headshots(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_HeadshotsPerLife", iValue);
		pPersistantStats->set_esips_headshotsperlife(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_HealthRestoresPerLife", iValue);
		pPersistantStats->set_esips_healthrestoresperlife(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_Kills", iValue);
		pPersistantStats->set_esips_kills(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_KillsNoReloadWeapChange", iValue);
		pPersistantStats->set_esips_killsnoreloadweapchange(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_Lose", iValue);
		pPersistantStats->set_esips_lose(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_MultiKillStreak", iValue);
		pPersistantStats->set_esips_multikillstreak(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_OnlineRankedWin", iValue);
		pPersistantStats->set_esips_onlinerankedwin(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_Win", iValue);
		pPersistantStats->set_esips_win(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_MeleeKillsThisSession", iValue);
		pPersistantStats->set_esips_meleekillsthissession(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_HeadshotKillsPerLife", iValue);
		pPersistantStats->set_esips_headshotkillsperlife(iValue);

		pPlayerProfile->GetAttribute("MP/PersistantStats/ESIPS_HeadshotKillsPerMatch", iValue);
		pPersistantStats->set_esips_headshotkillspermatch(iValue);

		//Gamemodes	EMPS_Gamemodes
		{
			CrysisProto::Gamemodes* pGameModes = pPersistantStats->mutable_emps_gamemodes();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/AllOrNothing", iValue);
			pGameModes->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/Assault", iValue);
			pGameModes->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/BombTheBase", iValue);
			pGameModes->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/CaptureTheFlag", iValue);
			pGameModes->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/Countdown", iValue);
			pGameModes->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/CrashSite", iValue);
			pGameModes->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/Extraction", iValue);
			pGameModes->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/InstantAction", iValue);
			pGameModes->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/PowerStruggleLite", iValue);
			pGameModes->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Gamemodes/TeamInstantAction", iValue);
			pGameModes->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamemodesTime
		{
			CrysisProto::Gamemodes* pGameModesTime = pPersistantStats->mutable_emps_gamemodestime();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/AllOrNothing", iValue);
			pGameModesTime->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Assault", iValue);
			pGameModesTime->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/BombTheBase", iValue);
			pGameModesTime->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/CaptureTheFlag", iValue);
			pGameModesTime->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Countdown", iValue);
			pGameModesTime->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/CrashSite", iValue);
			pGameModesTime->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/Extraction", iValue);
			pGameModesTime->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/InstantAction", iValue);
			pGameModesTime->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/PowerStruggleLite", iValue);
			pGameModesTime->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesTime/TeamInstantAction", iValue);
			pGameModesTime->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamemodesMVP
		{
			CrysisProto::Gamemodes* pGameModesMVP = pPersistantStats->mutable_emps_gamemodesmvp();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/AllOrNothing", iValue);
			pGameModesMVP->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Assault", iValue);
			pGameModesMVP->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/BombTheBase", iValue);
			pGameModesMVP->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/CaptureTheFlag", iValue);
			pGameModesMVP->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Countdown", iValue);
			pGameModesMVP->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/CrashSite", iValue);
			pGameModesMVP->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/Extraction", iValue);
			pGameModesMVP->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/InstantAction", iValue);
			pGameModesMVP->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/PowerStruggleLite", iValue);
			pGameModesMVP->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/TeamInstantAction", iValue);
			pGameModesMVP->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamemodesHighscore
		{
			CrysisProto::Gamemodes* pGameModesHighscore = pPersistantStats->mutable_emps_gamemodeshighscore();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/AllOrNothing", iValue);
			pGameModesHighscore->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Assault", iValue);
			pGameModesHighscore->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/BombTheBase", iValue);
			pGameModesHighscore->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/CaptureTheFlag", iValue);
			pGameModesHighscore->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Countdown", iValue);
			pGameModesHighscore->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/CrashSite", iValue);
			pGameModesHighscore->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/Extraction", iValue);
			pGameModesHighscore->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/InstantAction", iValue);
			pGameModesHighscore->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesHighscore/PowerStruggleLite", iValue);
			pGameModesHighscore->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamemodesMVP/TeamInstantAction", iValue);
			pGameModesHighscore->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamesDrawn
		{
			CrysisProto::Gamemodes* pGamesDrawn = pPersistantStats->mutable_emps_gamesdrawn();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/AllOrNothing", iValue);
			pGamesDrawn->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Assault", iValue);
			pGamesDrawn->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/BombTheBase", iValue);
			pGamesDrawn->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/CaptureTheFlag", iValue);
			pGamesDrawn->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Countdown", iValue);
			pGamesDrawn->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/CrashSite", iValue);
			pGamesDrawn->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/Extraction", iValue);
			pGamesDrawn->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/InstantAction", iValue);
			pGamesDrawn->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/PowerStruggleLite", iValue);
			pGamesDrawn->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesDrawn/TeamInstantAction", iValue);
			pGamesDrawn->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamesLost
		{
			CrysisProto::Gamemodes* pGamesLost = pPersistantStats->mutable_emps_gameslost();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/AllOrNothing", iValue);
			pGamesLost->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/Assault", iValue);
			pGamesLost->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/BombTheBase", iValue);
			pGamesLost->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/CaptureTheFlag", iValue);
			pGamesLost->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/Countdown", iValue);
			pGamesLost->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/CrashSite", iValue);
			pGamesLost->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/Extraction", iValue);
			pGamesLost->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/InstantAction", iValue);
			pGamesLost->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/PowerStruggleLite", iValue);
			pGamesLost->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesLost/TeamInstantAction", iValue);
			pGamesLost->set_teaminstantaction(iValue);
		}
		//Gamemodes	EMPS_GamesWon
		{
			CrysisProto::Gamemodes* pGamesWon = pPersistantStats->mutable_emps_gameswon();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/AllOrNothing", iValue);
			pGamesWon->set_allornothing(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/Assault", iValue);
			pGamesWon->set_assault(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/BombTheBase", iValue);
			pGamesWon->set_bombthebase(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/CaptureTheFlag", iValue);
			pGamesWon->set_capturetheflag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/Countdown", iValue);
			pGamesWon->set_countdown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/CrashSite", iValue);
			pGamesWon->set_crashsite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/Extraction", iValue);
			pGamesWon->set_extraction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/InstantAction", iValue);
			pGamesWon->set_instantaction(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/PowerStruggleLite", iValue);
			pGamesWon->set_powerstrugglelite(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_GamesWon/TeamInstantAction", iValue);
			pGamesWon->set_teaminstantaction(iValue);
		}

		//EMPS_DateFirstPlayed
		{
			CrysisProto::DateFirstPlayed* pDate = pPersistantStats->mutable_emps_datefirstplayed();

			pPlayerProfile->GetAttribute("MP/PersistantStats/DateFirstPlayed/Year", iValue);
			pDate->set_year(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/DateFirstPlayed/Month", iValue);
			pDate->set_month(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/DateFirstPlayed/Day", iValue);
			pDate->set_day(iValue);
		}

		//KillsByDamageType
		{
			CrysisProto::KillsByDamageType* pDamageType = pPersistantStats->mutable_emps_killsbydamagetype();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/HMG", iValue);
			pDamageType->set_hmg(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/aacannon", iValue);
			pDamageType->set_aacannon(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/alienDropPodBounce", iValue);
			pDamageType->set_aliendroppodbounce(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/bullet", iValue);
			pDamageType->set_bullet(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/collision", iValue);
			pDamageType->set_collision(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/event", iValue);
			pDamageType->set_event(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/explosion", iValue);
			pDamageType->set_explosion(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/fall", iValue);
			pDamageType->set_fall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/fire", iValue);
			pDamageType->set_fire(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/frag", iValue);
			pDamageType->set_frag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/gaussbullet", iValue);
			pDamageType->set_gaussbullet(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/heavyBullet", iValue);
			pDamageType->set_heavybullet(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/impulse_hit", iValue);
			pDamageType->set_impulse_hit(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/kvolt", iValue);
			pDamageType->set_kvolt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/melee", iValue);
			pDamageType->set_melee(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/mike_burn", iValue);
			pDamageType->set_mike_burn(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/normal", iValue);
			pDamageType->set_normal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/pingerPing", iValue);
			pDamageType->set_pingerping(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/punish", iValue);
			pDamageType->set_punish(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/punishFall", iValue);
			pDamageType->set_punishfall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/repair", iValue);
			pDamageType->set_repair(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/silentMelee", iValue);
			pDamageType->set_silentmelee(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/stamp", iValue);
			pDamageType->set_stamp(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_KillsByDamageType/stealthKill", iValue);
			pDamageType->set_stealthkill(iValue);
		}

		//Weapons EMPS_WeaponHeadshotKills
		{
			CrysisProto::Weapons* pWeaponsHeadshotsKills = pPersistantStats->mutable_emps_weaponheadshotkills();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/AY69", iValue);
			pWeaponsHeadshotsKills->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/C4", iValue);
			pWeaponsHeadshotsKills->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/DSG1", iValue);
			pWeaponsHeadshotsKills->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Feline", iValue);
			pWeaponsHeadshotsKills->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/FlashBangGrenades", iValue);
			pWeaponsHeadshotsKills->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/FragGrenades", iValue);
			pWeaponsHeadshotsKills->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Gauss", iValue);
			pWeaponsHeadshotsKills->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Grendel", iValue);
			pWeaponsHeadshotsKills->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Hammer", iValue);
			pWeaponsHeadshotsKills->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/JAW", iValue);
			pWeaponsHeadshotsKills->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Jackal", iValue);
			pWeaponsHeadshotsKills->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/K_Volt", iValue);
			pWeaponsHeadshotsKills->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/LTag", iValue);
			pWeaponsHeadshotsKills->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Marshall", iValue);
			pWeaponsHeadshotsKills->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Mk60", iValue);
			pWeaponsHeadshotsKills->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Nova", iValue);
			pWeaponsHeadshotsKills->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/Revolver", iValue);
			pWeaponsHeadshotsKills->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/SCAR", iValue);
			pWeaponsHeadshotsKills->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/SCARAB", iValue);
			pWeaponsHeadshotsKills->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshotKills/mike", iValue);
			pWeaponsHeadshotsKills->set_mike(iValue);
		}
		//Weapons EMPS_WeaponHeadshots
		{
			CrysisProto::Weapons* pWeaponsHeadshots = pPersistantStats->mutable_emps_weaponheadshots();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/AY69", iValue);
			pWeaponsHeadshots->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/C4", iValue);
			pWeaponsHeadshots->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/DSG1", iValue);
			pWeaponsHeadshots->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Feline", iValue);
			pWeaponsHeadshots->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/FlashBangGrenades", iValue);
			pWeaponsHeadshots->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/FragGrenades", iValue);
			pWeaponsHeadshots->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Gauss", iValue);
			pWeaponsHeadshots->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Grendel", iValue);
			pWeaponsHeadshots->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Hammer", iValue);
			pWeaponsHeadshots->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/JAW", iValue);
			pWeaponsHeadshots->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Jackal", iValue);
			pWeaponsHeadshots->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/K_Volt", iValue);
			pWeaponsHeadshots->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/LTag", iValue);
			pWeaponsHeadshots->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Marshall", iValue);
			pWeaponsHeadshots->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Mk60", iValue);
			pWeaponsHeadshots->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Nova", iValue);
			pWeaponsHeadshots->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/Revolver", iValue);
			pWeaponsHeadshots->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/SCAR", iValue);
			pWeaponsHeadshots->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/SCARAB", iValue);
			pWeaponsHeadshots->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponHeadshots/mike", iValue);
			pWeaponsHeadshots->set_mike(iValue);
		}
		//Weapons EMPS_WeaponKills
		{
			CrysisProto::Weapons* pWeaponsKills = pPersistantStats->mutable_emps_weaponkills();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/AY69", iValue);
			pWeaponsKills->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/C4", iValue);
			pWeaponsKills->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/DSG1", iValue);
			pWeaponsKills->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Feline", iValue);
			pWeaponsKills->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/FlashBangGrenades", iValue);
			pWeaponsKills->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/FragGrenades", iValue);
			pWeaponsKills->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Gauss", iValue);
			pWeaponsKills->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Grendel", iValue);
			pWeaponsKills->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Hammer", iValue);
			pWeaponsKills->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/JAW", iValue);
			pWeaponsKills->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Jackal", iValue);
			pWeaponsKills->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/K_Volt", iValue);
			pWeaponsKills->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/LTag", iValue);
			pWeaponsKills->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Marshall", iValue);
			pWeaponsKills->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Mk60", iValue);
			pWeaponsKills->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Nova", iValue);
			pWeaponsKills->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/Revolver", iValue);
			pWeaponsKills->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/SCAR", iValue);
			pWeaponsKills->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/SCARAB", iValue);
			pWeaponsKills->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponKills/mike", iValue);
			pWeaponsKills->set_mike(iValue);
		}
		//Weapons EMPS_WeaponShots
		{
			CrysisProto::Weapons* pWeaponsShots = pPersistantStats->mutable_emps_weaponshots();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/AY69", iValue);
			pWeaponsShots->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/C4", iValue);
			pWeaponsShots->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/DSG1", iValue);
			pWeaponsShots->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Feline", iValue);
			pWeaponsShots->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/FlashBangGrenades", iValue);
			pWeaponsShots->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/FragGrenades", iValue);
			pWeaponsShots->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Gauss", iValue);
			pWeaponsShots->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Grendel", iValue);
			pWeaponsShots->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Hammer", iValue);
			pWeaponsShots->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/JAW", iValue);
			pWeaponsShots->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Jackal", iValue);
			pWeaponsShots->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/K_Volt", iValue);
			pWeaponsShots->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/LTag", iValue);
			pWeaponsShots->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Marshall", iValue);
			pWeaponsShots->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Mk60", iValue);
			pWeaponsShots->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Nova", iValue);
			pWeaponsShots->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/Revolver", iValue);
			pWeaponsShots->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/SCAR", iValue);
			pWeaponsShots->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/SCARAB", iValue);
			pWeaponsShots->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponShots/mike", iValue);
			pWeaponsShots->set_mike(iValue);
		}
		//Weapons EMPS_WeaponTime
		{
			CrysisProto::Weapons* pWeaponsTime = pPersistantStats->mutable_emps_weapontime();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/AY69", iValue);
			pWeaponsTime->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/C4", iValue);
			pWeaponsTime->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/DSG1", iValue);
			pWeaponsTime->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Feline", iValue);
			pWeaponsTime->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/FlashBangGrenades", iValue);
			pWeaponsTime->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/FragGrenades", iValue);
			pWeaponsTime->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Gauss", iValue);
			pWeaponsTime->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Grendel", iValue);
			pWeaponsTime->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Hammer", iValue);
			pWeaponsTime->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/JAW", iValue);
			pWeaponsTime->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Jackal", iValue);
			pWeaponsTime->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/K_Volt", iValue);
			pWeaponsTime->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/LTag", iValue);
			pWeaponsTime->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Marshall", iValue);
			pWeaponsTime->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Mk60", iValue);
			pWeaponsTime->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Nova", iValue);
			pWeaponsTime->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/Revolver", iValue);
			pWeaponsTime->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/SCAR", iValue);
			pWeaponsTime->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/SCARAB", iValue);
			pWeaponsTime->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponTime/mike", iValue);
			pWeaponsTime->set_mike(iValue);
		}
		//Weapons EMPS_WeaponUnlocked
		{
			CrysisProto::Weapons* pWeaponsUnlocked = pPersistantStats->mutable_emps_weaponunlocked();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/AY69", iValue);
			pWeaponsUnlocked->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/C4", iValue);
			pWeaponsUnlocked->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/DSG1", iValue);
			pWeaponsUnlocked->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Feline", iValue);
			pWeaponsUnlocked->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/FlashBangGrenades", iValue);
			pWeaponsUnlocked->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/FragGrenades", iValue);
			pWeaponsUnlocked->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Gauss", iValue);
			pWeaponsUnlocked->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Grendel", iValue);
			pWeaponsUnlocked->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Hammer", iValue);
			pWeaponsUnlocked->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/JAW", iValue);
			pWeaponsUnlocked->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Jackal", iValue);
			pWeaponsUnlocked->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/K_Volt", iValue);
			pWeaponsUnlocked->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/LTag", iValue);
			pWeaponsUnlocked->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Marshall", iValue);
			pWeaponsUnlocked->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Mk60", iValue);
			pWeaponsUnlocked->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Nova", iValue);
			pWeaponsUnlocked->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/Revolver", iValue);
			pWeaponsUnlocked->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/SCAR", iValue);
			pWeaponsUnlocked->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/SCARAB", iValue);
			pWeaponsUnlocked->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUnlocked/mike", iValue);
			pWeaponsUnlocked->set_mike(iValue);
		}
		//Weapons EMPS_WeaponUsage
		{
			CrysisProto::Weapons* pWeaponsUsage = pPersistantStats->mutable_emps_weaponusage();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/AY69", iValue);
			pWeaponsUsage->set_ay69(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/C4", iValue);
			pWeaponsUsage->set_c4(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/DSG1", iValue);
			pWeaponsUsage->set_dsg1(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Feline", iValue);
			pWeaponsUsage->set_feline(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/FlashBangGrenades", iValue);
			pWeaponsUsage->set_flashbanggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/FragGrenades", iValue);
			pWeaponsUsage->set_fraggrenades(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Gauss", iValue);
			pWeaponsUsage->set_gauss(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Grendel", iValue);
			pWeaponsUsage->set_grendel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Hammer", iValue);
			pWeaponsUsage->set_hammer(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/JAW", iValue);
			pWeaponsUsage->set_jaw(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Jackal", iValue);
			pWeaponsUsage->set_jackal(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/K_Volt", iValue);
			pWeaponsUsage->set_k_volt(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/LTag", iValue);
			pWeaponsUsage->set_ltag(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Marshall", iValue);
			pWeaponsUsage->set_marshall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Mk60", iValue);
			pWeaponsUsage->set_mk60(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Nova", iValue);
			pWeaponsUsage->set_nova(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/Revolver", iValue);
			pWeaponsUsage->set_revolver(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/SCAR", iValue);
			pWeaponsUsage->set_scar(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/SCARAB", iValue);
			pWeaponsUsage->set_scarab(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_WeaponUsage/mike", iValue);
			pWeaponsUsage->set_mike(iValue);
		}

		//EMPS_Levels
		{
			CrysisProto::C2Levels* pCryLevels = pPersistantStats->mutable_emps_levels();

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_alien_vessel", iValue);
			pCryLevels->set_cw2_alien_vessel(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_alien_vessel_small", iValue);
			pCryLevels->set_cw2_alien_vessel_small(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_church", iValue);
			pCryLevels->set_cw2_church(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_city_hall", iValue);
			pCryLevels->set_cw2_city_hall(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_collided_buildings", iValue);
			pCryLevels->set_cw2_collided_buildings(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_cxp_liberty", iValue);
			pCryLevels->set_cw2_cxp_liberty(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_cxp_liberty_mil", iValue);
			pCryLevels->set_cw2_cxp_liberty_mil(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_cxp_liberty_statue", iValue);
			pCryLevels->set_cw2_cxp_liberty_statue(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_downtown", iValue);
			pCryLevels->set_cw2_downtown(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_lighthouse", iValue);
			pCryLevels->set_cw2_lighthouse(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_pier", iValue);
			pCryLevels->set_cw2_pier(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_rooftop_gardens", iValue);
			pCryLevels->set_cw2_rooftop_gardens(iValue);

			pPlayerProfile->GetAttribute("MP/PersistantStats/EMPS_Levels/wars/cw2_terminal", iValue);
			pCryLevels->set_cw2_terminal(iValue);
		}

		CrysisProto::ProgressionTokenSystem* pProgressionTokenSystem = onlineData.mutable_progression_token_system();
		pProgressionTokenSystem->Clear();

		//Attachments
		CrysisProto::Weapons* pProgressionAttachments = pProgressionTokenSystem->mutable_attachment();
		pProgressionAttachments->Clear();

		CrysisProto::ProgressionTokenSystemUnlocks* pProgressionTokenSystemUnlocks = onlineData.mutable_progression_token_system_unlocks();
		pProgressionTokenSystemUnlocks->Clear();

		//Loadout
		CrysisProto::Loadout* pLoadout = pProgressionTokenSystemUnlocks->mutable_ptsu_loadout();
		pLoadout->Clear();

		//Playlist
		CrysisProto::Playlist* pPlaylist = pProgressionTokenSystemUnlocks->mutable_ptsu_playlist();
		pPlaylist->Clear();

		//ArmorPerk
		CrysisProto::ArmorPerk* pArmorPerk = pProgressionTokenSystemUnlocks->mutable_ptsu_armorperk();
		pArmorPerk->Clear();

		//SP
		CrysisProto::StealthPerk* pStealthPerk = pProgressionTokenSystemUnlocks->mutable_ptsu_stealthperk();
		pStealthPerk->Clear();

		//Perk
		CrysisProto::Perk* pPerk = pProgressionTokenSystemUnlocks->mutable_ptsu_perk();
		pPerk->Clear();

		//Weapons
		CrysisProto::Weapons* pPTSU_Weapons = pProgressionTokenSystemUnlocks->mutable_ptsu_weapons();

		//AY69
		CrysisProto::Attachment* pAY69 = pProgressionTokenSystemUnlocks->mutable_ay69();
		pAY69->Clear();
		//DSG1
		CrysisProto::Attachment* pDSG1 = pProgressionTokenSystemUnlocks->mutable_dsg1();
		pDSG1->Clear();
		//Feline
		CrysisProto::Attachment* pFeline = pProgressionTokenSystemUnlocks->mutable_feline();
		pFeline->Clear();
		//Gauss
		CrysisProto::Attachment* pGauss = pProgressionTokenSystemUnlocks->mutable_gauss();
		pGauss->Clear();
		//Grendel
		CrysisProto::Attachment* pGrendel = pProgressionTokenSystemUnlocks->mutable_grendel();
		pGrendel->Clear();
		//Hammer
		CrysisProto::Attachment* pHammer = pProgressionTokenSystemUnlocks->mutable_hammer();
		pHammer->Clear();
		//Jackal
		CrysisProto::Attachment* pJackal = pProgressionTokenSystemUnlocks->mutable_jackal();
		pJackal->Clear();
		//K_Volt
		CrysisProto::Attachment* pK_Volt = pProgressionTokenSystemUnlocks->mutable_k_volt();
		pK_Volt->Clear();
		//Marshall
		CrysisProto::Attachment* pMarshall = pProgressionTokenSystemUnlocks->mutable_marshall();
		pMarshall->Clear();
		//Mk60
		CrysisProto::Attachment* pMk60 = pProgressionTokenSystemUnlocks->mutable_mk60();
		pMk60->Clear();
		//Nova
		CrysisProto::Attachment* pNova = pProgressionTokenSystemUnlocks->mutable_nova();
		pNova->Clear();
		//Revolver
		CrysisProto::Attachment* pRevolver = pProgressionTokenSystemUnlocks->mutable_revolver();
		pRevolver->Clear();
		//SCAR
		CrysisProto::Attachment* pSCAR = pProgressionTokenSystemUnlocks->mutable_scar();
		pSCAR->Clear();
		//SCARAB
		CrysisProto::Attachment* pSCARAB = pProgressionTokenSystemUnlocks->mutable_scarab();
		pSCARAB->Clear();


		return onlineData;
	}
}

void CCryProfiles::ReloadProfile()
{
	m_pProfileManager->ReloadProfile(GetCurrentProfile(), EProfileReasons::ePR_Game);
}

