/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 22/03/2019  18:15 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "CryDedicated.h"
#include "CryCustomLobby.h"
#include <ISimpleHttpServer.h>

#include <fstream>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"

CCryDedicated* pCryDedicatedInstance = nullptr;

void OnSessionUserJoin(UCryLobbyEventData eventData, void *userParam)
{
}

void UpdateLobbyRequest(ICryLobby* pLobby, CrySessionHandle h)
{
	CCryLobbyPacket lobbyPacket;
	if (lobbyPacket.CreateWriteBuffer(CryLobbyPacketReliableHeaderSize))
	{
		lobbyPacket.StartWrite(GameUserPacketDefinitions::eGUPD_SyncExtendedServerDetails, true);

		ECryLobbyError error = pLobby->GetMatchMaking()->SendToAllClients(&lobbyPacket, h);
		if (error != eCLE_Success)
			gEnv->pLog->LogError("[CryProto::UpdateLobbyData] Couldn't send packet to users with error<%u>", error);
		else
			gEnv->pLog->Log("[CryProto::UpdateLobbyData] Success.");
	}
}

void OnUserPacket(UCryLobbyEventData eventData, void *userParam)
{
	CCryLobbyPacket* pPacket = eventData.pUserPacketData->pPacket;

	uint32 packetId = pPacket->StartRead();
	if (packetId == ELobbyPacketType::eLPT_UserTokenAuthorization)
	{

	}
	else if (packetId == ELobbyPacketType::eLPT_UserPlayerProgression)
	{
		
	}
	else if (packetId == ELobbyPacketType::eLPT_UserPersistantStats)
	{
		
	}
	else if (packetId == ELobbyPacketType::eLPT_UserGameInfo)
	{
		
	}

}


CCryDedicated::CCryDedicated()
{
	if (!gEnv)
	{
		return;
	}

	//Check if GUIDSetup Exist!

	std::string jsonPath = gEnv->pSystem->GetRootFolder();

	std::ifstream setupJson(jsonPath + "GUIDSetup.json");
	
	if (setupJson.is_open())
	{
		gEnv->pLog->Log("[GUID] Trying to connect with MasterServer...");

		rapidjson::Document d;
	
		// get length of file:
		setupJson.seekg(0, setupJson.end);
		int length = setupJson.tellg();
		setupJson.seekg(0, setupJson.beg);

		char* buffer = new char[length];
		setupJson.read(buffer, length);

		d.Parse(buffer);

		rapidjson::Value& serverTitle = d["ServerTitle"];
		rapidjson::Value& serverGuid = d["DedicatedGUID"];

		m_dedicatedGuid.m_sServerTitle = serverTitle.GetString();
		m_dedicatedGuid.m_sServerGUID = serverGuid.GetString();


	}
	else
	{
		gEnv->pSystem->FatalError("GUIDSetup.json wasn't open!");
	}


	
	m_pGameFramework = gEnv->pGame->GetIGameFramework();
	m_pCryLobby = gEnv->pNetwork->GetLobby();

	m_pServerNub = m_pGameFramework->GetServerNetNub();

	if (m_pGameFramework)
	{
		m_pGameFramework->GetIGameplayRecorder()->RegisterListener(this);
	}

	m_pCryLobby->RegisterEventInterest(eCLSE_SessionUserJoin, OnSessionUserJoin, NULL);
	m_pCryLobby->RegisterEventInterest(eCLSE_UserPacket, OnUserPacket, NULL);

	m_pCryProfiles = new CCryProfiles();

	m_pCryLobbyPacketSendler = new CCryLobbyPacketSendler(m_pCryLobby);

	pCryDedicatedInstance = this;
}

CCryDedicated::~CCryDedicated()
{
	m_pCryLobby->UnregisterEventInterest(eCLSE_SessionUserJoin, OnSessionUserJoin, NULL);
	m_pCryLobby->UnregisterEventInterest(eCLSE_UserPacket, OnUserPacket, NULL);

	m_pGameFramework->GetIGameplayRecorder()->UnregisterListener(this);
}

void CCryDedicated::OnGameplayEvent(IEntity * pEntity, const GameplayEvent & event)
{
	if (event.event == eGE_Kill && m_bIsFirstBlood == false)
	{
		m_bIsFirstBlood = true;
		string cmdSay = "sv_say FIRST BLOOD BY " + string(pEntity->GetName());

		gEnv->pConsole->ExecuteString(cmdSay.c_str());
	}

	if (event.event == eGE_Connected)
	{
		
	}

	if (event.event == eGE_Disconnected)
	{
		
	}

	if (event.event == eGE_Kill)
	{
		
	}

	if (event.event == eGE_Death)
	{
		
	}

	if (event.event == eGE_Rank)
	{
		
	}

	if (event.event == eGE_GameEnd)
	{
		
	}
}
