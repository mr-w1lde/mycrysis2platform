/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 23/03/2019  15:34 : Created by Stanislav Migunov
*************************************************************************/
#pragma once


enum ELobbyPacketType
{
	eLPT_UserTokenAuthorization = 160,
	eLPT_UserPlayerProgression,
	eLPT_UserGameInfo,
	eLPT_UserPersistantStats
};

enum GameUserPacketDefinitions
{
	eGUPD_LobbyStartCountdownTimer = CRYLOBBY_USER_PACKET_START,
	eGUPD_LobbyGameHasStarted,
	eGUPD_LobbyEndGame,
	eGUPD_LobbyEndGameResponse,
	eGUPD_LobbyUpdatedSessionInfo,
	eGUPD_LobbyMoveSession,
	eGUPD_SquadJoin,
	eGUPD_SquadJoinGame,
	eGUPD_SquadNotInGame,
	eGUPD_SetTeam,
	eGUPD_SendChatMessage,								// Clients request to send a message to other players
	eGUPD_ChatMessage,										// Server sent message to all appropriate other players.
	eGUPD_ReservationRequest,							// Sent by squad leader client after joined game to identify self as leader and to tell the game server to reserve slots for its members
	eGUPD_ReservationClientIdentify,			// Sent by clients after joined game to identify self to game server so it can check if client passes reserve checks (if any)
	eGUPD_ReservationsMade,								// Sent to a squad leader by a server when requested reservations have been successfully made upon receipt of a eGUPD_ReservationRequest packet 
	eGUPD_ReservationFailedSessionFull,		// Can be sent to clients when there's no room for them in a game session. Generally causes them to "kick" themselves by deleting their own session
	eGUPD_SyncPlaylistContents,						// Sync entire playlist
	eGUPD_SetGameVariant,
	eGUPD_SyncPlaylistRotation,
	eGUPD_SquadLeaveGame,									// Squad: Tell all members in the squad to leave (game host will leave last)
	eGUPD_SquadNotSupported,							// Squads not suported in current gamemode
	eGUPD_UpdateCountdownTimer,
	eGUPD_RequestAdvancePlaylist,
	eGUPD_SyncExtendedServerDetails,
#if INCLUDE_DEDICATED_LEADERBOARDS
	eGUPD_StartOfGameUserStats,						// User has joined a ranked server, send them there stats
	eGUPD_EndOfGameUserStats,							// User has played to end of round, save their stats
	eGUPD_UpdateUserStats,
	eGUPD_UpdateUserStatsReceived,				// response from server to client to say it has received all the stats
	eGUPD_FailedToReadOnlineData,
#endif
	eGUPD_DetailedServerInfoRequest,
	eGUPD_DetailedServerInfoResponse,
	eGUPD_SquadDifferentVersion,					// Response to SquadJoin packet sent when the client is on a different patch
	eGUPD_SquadKick,
	eGUPD_UnloadPreviousLevel,						// Tell clients that we're about to start and they should clean up the previous game

	eGUPD_End
};


class CCryLobbyPacketSendler
{
public:
	CCryLobbyPacketSendler(ICryLobby* pLobby);
	~CCryLobbyPacketSendler();
public:
	template<typename T>
	ECryLobbyError SendProtoDataToClient(const T& protoData, CrySessionHandle h, SCryMatchMakingConnectionUID uid, uint32 type);

	template<typename T>
	ECryLobbyError SendProtoDataToServer(const T& protoData, CrySessionHandle h, uint32 type);
public:
	template<typename T>
	bool ParseCryProtoData(CCryLobbyPacket* pPacket, T& protoData);
private:
	ICryLobby* m_pCryLobby;
};


template<typename T>
ECryLobbyError CCryLobbyPacketSendler::SendProtoDataToClient(const T& protoData, CrySessionHandle h, SCryMatchMakingConnectionUID uid, uint32 type)
{
	if (gEnv->IsDedicated())
		gEnv->pLog->Log("[CCryLobbyPacketSendler] Sending packet<id:%u> to CryUser<id:%u>", type, uid.m_uid);
	else
		printf("[CCryLobbyPacketSendler] Sending packet<id:%u> to CryUser<id:%u>\n", type, uid.m_uid);

	CCryLobbyPacket lobbyPacket;
	uint32 protoSize = protoData.ByteSizeLong();
	uint32 packetSize = CryLobbyPacketReliableHeaderSize + protoSize;

	void* pBuffer = malloc(protoSize);
	protoData.SerializeToArray(pBuffer, protoSize);

	if (lobbyPacket.CreateWriteBuffer(packetSize))
	{
		lobbyPacket.StartWrite(type, true);
		lobbyPacket.WriteData(pBuffer, protoSize);

		SAFE_DELETE(pBuffer);
	}

	return m_pCryLobby->GetMatchMaking()->SendToClient(&lobbyPacket, h, uid);
}

template<typename T>
ECryLobbyError CCryLobbyPacketSendler::SendProtoDataToServer(const T& protoData, CrySessionHandle h, uint32 type)
{
	if (gEnv->IsDedicated())
		gEnv->pLog->Log("[CCryLobbyPacketSendler] Sending packet<id:%u> to Dedicated Server", type);
	else
		printf("[CCryLobbyPacketSendler] Sending packet<id:%u> to Dedicated Server\n", type);

	CCryLobbyPacket lobbyPacket;
	uint32 protoSize = protoData.ByteSizeLong();
	uint32 packetSize = CryLobbyPacketReliableHeaderSize + protoSize;

	void* pBuffer = malloc(protoSize);
	protoData.SerializeToArray(pBuffer, protoSize);

	if (lobbyPacket.CreateWriteBuffer(packetSize))
	{
		lobbyPacket.StartWrite(type, true);
		lobbyPacket.WriteData(pBuffer, protoSize);

		SAFE_DELETE(pBuffer);
	}

	return m_pCryLobby->GetMatchMaking()->SendToServer(&lobbyPacket, h);
}

template<typename T>
bool CCryLobbyPacketSendler::ParseCryProtoData(CCryLobbyPacket * pPacket, T& protoData)
{
	uint32 protoLen = pPacket->GetReadBufferSize() - CryLobbyPacketReliableHeaderSize;
	void* pBuffer = malloc(protoLen);

	pPacket->ReadData(pBuffer, protoLen);

	if (protoData.ParseFromArray(pBuffer, protoLen))
	{
		delete pBuffer;
		return true;
	}

	return false;
}
