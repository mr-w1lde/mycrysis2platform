/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 02/04/2019  16:45 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "CryBattleRoyale.h"

#include <IGameFramework.h>

CCryBattleRoyal::CCryBattleRoyal()
{
	gEnv->pLog->Log("Here");
}

CCryBattleRoyal::~CCryBattleRoyal()
{
	gEnv->pGame->GetIGameFramework()->GetIGameRulesSystem()->SetCurrentGameRules(nullptr);
}

bool CCryBattleRoyal::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);

	if (!pGameObject->BindToNetwork())
		return false;

	if (gEnv->pGame->GetIGameFramework())
		//m_pActorSystem = gEnv->pGameFramework->GetIActorSystem();

		gEnv->pGame->GetIGameFramework()->GetIGameRulesSystem()->SetCurrentGameRules(this);

	return true;
}

void CCryBattleRoyal::Release()
{
}

void CCryBattleRoyal::Update(SEntityUpdateContext & ctx, int updateSlot)
{
}

void CCryBattleRoyal::OnConnect(INetChannel * pNetChannel)
{
}

void CCryBattleRoyal::OnDisconnect(EDisconnectionCause cause, const char * desc)
{
}

bool CCryBattleRoyal::OnClientConnect(int channelId, bool isReset)
{
	return false;
}

void CCryBattleRoyal::OnClientDisconnect(int channelId, EDisconnectionCause cause, const char * desc, bool keepClient)
{
}

bool CCryBattleRoyal::OnClientEnteredGame(int channelId, bool isReset)
{
	return false;
}

void CCryBattleRoyal::OnEntitySpawn(IEntity * pEntity)
{
}

void CCryBattleRoyal::OnEntityRemoved(IEntity * pEntity)
{
}

void CCryBattleRoyal::OnEntityReused(IEntity * pEntity, SEntitySpawnParams & params, EntityId prevId)
{
}
