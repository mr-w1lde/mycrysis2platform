#pragma once

#include <Windows.h>
#include <string>
#include <thread>
#include <vector>

#define GAME_IS_CRYSIS2
#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS 1
#define MASTER_SERVER_IP "127.0.0.1"
#define MASTER_SERVER_PORT 23066

#define NOT_USE_CRY_MEMORY_MANAGER

#include <NetSerialization/OnlineAttributes.pb.h>
#include <NetSerialization/Packet.pb.h>
#include <NetSerialization/Authorization.pb.h>

#include <platform.h>
#include <ISystem.h>
#include <IGame.h>
#include <IConsole.h>

#include <IGameSessionHandler.h>
#include <CryLobbyPacket.h>


#include "C2PlatformSDK/PlatformIntegration.h"
#include <C2PlatfromSDK/IPlatformSDK.h>

#include <jdbc/mysql_connection.h>
#include <jdbc/cppconn/statement.h>
#include <jdbc/cppconn/prepared_statement.h>
#include <jdbc/mysql_driver.h>
#include <jdbc/mysql_error.h>

const char sqlServer[] = { '\0' };
const char sqlDatabase[] = {  '\0' };
const char sqlId[] = {  '\0' };
const char sqlPassword[] = { '\0' };

struct C2Startup
{
public:
	struct SCryUserAutho
	{
		const char* m_nickname = nullptr;
		const char* m_userToken = nullptr;
		const char* m_ipServer = nullptr;
	};

public:
	bool m_bClient = false;
	bool m_bServer = false;
public:
	SCryUserAutho m_userAutho;

};

#define C2_API extern "C" __declspec(dllexport)
#define USERN_PTR DWORD(0x396F2178)
#define USERN_PTR_OFFSET DWORD(0x6F2178)

DWORD ReadPtr(DWORD baseAddr, DWORD offset);
void SetClientUsername(const char* nick);
void ConnectToSQL();

extern C2::IPlatformSDK* pPlatformSDK;
extern sql::Connection* pSQLconnection;

#define REGISTER_GAME_OBJECT_EXTENSION(framework, name)\
	{\
		struct C##name##Creator : public IGameObjectExtensionCreatorBase\
		{\
		C##name *Create()\
			{\
			return new C##name();\
			}\
			void GetGameObjectExtensionRMIData( void ** ppRMI, size_t * nCount )\
			{\
			C##name::GetGameObjectExtensionRMIData( ppRMI, nCount );\
			}\
		};\
		static C##name##Creator _creator;\
		framework->GetIGameObjectSystem()->RegisterExtension(#name, &_creator, NULL);\
	}

