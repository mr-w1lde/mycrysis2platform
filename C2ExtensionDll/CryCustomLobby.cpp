/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 23/03/2019  15:34 : Created by Stanislav Migunov
*************************************************************************/
#include "StdAfx.h"
#include "CryCustomLobby.h"

CCryLobbyPacketSendler::CCryLobbyPacketSendler(ICryLobby * pLobby)
{
	gEnv->pLog->Log("[CCryLobbyPacketSendler] Initializing...");

	if (pLobby)
		m_pCryLobby = pLobby;
}


CCryLobbyPacketSendler::~CCryLobbyPacketSendler()
{
	gEnv->pLog->Log("[CCryLobbyPacketSendler] :~CCryLobbyPacketSendler()");
}
