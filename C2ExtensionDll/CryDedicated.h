/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 22/03/2019  18:15 : Created by Stanislav Migunov
*************************************************************************/
#pragma once

#include <IGameFramework.h>
#include <ICryLobby.h>
#include <IGameplayRecorder.h>

#include "CryCustomLobby.h"
#include "CryProfiles.h"

struct SDedicatedGUID
{
	std::string m_sServerTitle;
	std::string m_sServerGUID;

	SDedicatedGUID()
	{
		m_sServerTitle = "";
		m_sServerGUID = "";
	}
};

class CCryDedicated : public IGameplayListener
{
public:
	CCryDedicated();
	~CCryDedicated();
public:
	// IGameplayListener
	virtual void OnGameplayEvent(IEntity *pEntity, const GameplayEvent &event) override;
	//~IGameplayListener
public:
	ICryLobby* GetCryLobby() { return m_pCryLobby; }
	CCryProfiles* GetCryProfiles() { return m_pCryProfiles; }
	CCryLobbyPacketSendler* GetPacketSendler() { return m_pCryLobbyPacketSendler; }
private:
	IGameFramework* m_pGameFramework = nullptr;
	ICryLobby* m_pCryLobby = nullptr;
	INetNub* m_pServerNub = nullptr;
	CCryProfiles* m_pCryProfiles = nullptr;
	CCryLobbyPacketSendler* m_pCryLobbyPacketSendler = nullptr;
	SDedicatedGUID m_dedicatedGuid = SDedicatedGUID();

private:
	bool m_bIsFirstBlood = false;
};