#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS 1

#include <iostream>

#include <CryModuleDefs.h>
#define eCryModule eCryM_Launcher

#include <platform.h>
#include <platform_impl.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <ShellAPI.h>


#include <fileapi.h>
#include <IConsole.h>

#include "ISystem.h"
#include "GameStartup.h"
#include <vector>
#include <map>

#define SECUROM_INCLUDE_EXE_FUNCTIONS
#include <CopyProtection.h>

#include <CryMemoryManager_impl.h>

HINSTANCE hCryGameDll = NULL;
HINSTANCE hC2Extension = NULL;

struct C2Startup
{
public:
	struct SCryUserAutho
	{
		const char* m_nickname = nullptr;
		const char* m_userToken = nullptr;
		const char* m_ipServer = nullptr;
	};

public:
	bool m_bClient = false;
	bool m_bServer = false;
public:
	SCryUserAutho m_userAutho;
};

typedef void(*FLoadC2Extension)(SSystemGlobalEnvironment*, C2Startup&);
typedef std::map<string, string> CmdArgs;

CmdArgs GetCmdArgs(const char* lpCmdLine)
{
	string sCmd = lpCmdLine;
	
	string parsElement = "-";

	size_t next = 0;
	size_t prev = 0;

	CmdArgs argsArr;

	while ((next = sCmd.find(parsElement, prev)) != string::npos)
	{
		if (next > 1)
		{
			string strArg = sCmd.substr(prev - 1, next - (prev - 1));

			if (size_t spacePos = strArg.find(" "))
			{
				size_t strArgLen = strArg.length();

				string cmd = strArg.substr(1, spacePos - 1);
				string val = strArg.substr(spacePos + 1, (strArgLen - 1) - (spacePos + 1));

				argsArr.insert(std::pair<string, string>(cmd, val));
			}
		}

		prev = next + (size_t)parsElement.length();
	}

	return argsArr;
}

void InitRootDir(char szExeFileName[] = 0, uint nSize = 0)
{
	WCHAR szExePathName[_MAX_PATH];
	size_t nLen = GetModuleFileNameW(GetModuleHandle(NULL), szExePathName, _MAX_PATH);

	// Find path above exe name and deepest folder.
	int nCount = 0;
	for (size_t n = nLen - 1; n > 0; n--)
	{
		if (szExePathName[n] == '\\')
		{
			nLen = n;
			if (++nCount == 2)
				break;
		}
	}
	if (nCount > 0)
	{
		szExePathName[nLen++] = 0;

		// Switch to upper folder.
		SetCurrentDirectoryW(szExePathName);

		// Return exe name and relative folder, assuming it's ASCII.
		if (szExeFileName)
			wcstombs(szExeFileName, szExePathName + nLen, nSize);
	}
}

int RunGame(const char* commandLine, C2Startup::SCryUserAutho cryUser)
{
	HANDLE mutex = CreateMutex(NULL, TRUE, "CrytekApplication");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		bool ignoreTooManyApps = false;
		if (!ignoreTooManyApps)
		{
			MessageBox(GetDesktopWindow(), "There is already a Crytek application running\nClose the last running app.", "Too many apps", MB_OK);
			return 1;
		}
	}

	_CryMemoryManagerPoolHelper::Init();


	hCryGameDll = CryLoadLibrary("CryGameCrysis2.dll");
	if (!hCryGameDll)
	{
		MessageBox(0, "Failed to load the Game DLL!", "Error", MB_OK);
		CloseHandle(mutex);
		return 0;
	}
	IGameStartup::TEntryFunction efCreateGameStartup = (IGameStartup::TEntryFunction)GetProcAddress(hCryGameDll, "CreateGameStartup2");
	if (!efCreateGameStartup)
	{
		CryFreeLibrary(hCryGameDll);
		MessageBox(0, "Game startup creation failed!", "Error", MB_OK);
		CloseHandle(mutex);
		return 0;
	}

	CGameStartup* pGameStartup = (CGameStartup*)efCreateGameStartup();
	if (!pGameStartup)
	{
		CryFreeLibrary(hCryGameDll);
		MessageBox(0, "Creation of the Startup Interface failed", "Error", MB_OK);
		CloseHandle(mutex);
		return 0;
	}

	SSystemInitParams startupParams;
	startupParams.hInstance = GetModuleHandle(0);
	startupParams.sLogFileName = "Crysis2.log";

	strcpy(startupParams.szSystemCmdLine, commandLine);

	hC2Extension = CryLoadLibrary("C2ExtensionDll.dll");
	if (!hC2Extension)
	{
		MessageBox(0, "Failed to load the C2Extension DLL!", "Error", MB_OK);
		CloseHandle(mutex);
		return 0;
	}

	FLoadC2Extension fLoadEx = (FLoadC2Extension)GetProcAddress(hC2Extension, "LoadC2Extencion");
	if (!fLoadEx)
	{
		CryFreeLibrary(hC2Extension);
		MessageBox(0, "C2Extension startup creation failed!", "Error", MB_OK);
		CloseHandle(mutex);
		return 0;
	}

	InitRootDir();

	C2Startup startup;
	startup.m_bClient = true;
	startup.m_userAutho = cryUser;

	if (pGameStartup->Init(startupParams))
	{
		SSystemGlobalEnvironment* mEnv = startupParams.pSystem->GetGlobalEnvironment();

		fLoadEx(mEnv, startup);

		//Call Run to start the game loop. Optional level to start with.
		pGameStartup->Run(NULL);

		//Shutdown after game is finished.
		pGameStartup->Shutdown();

		pGameStartup = nullptr;
		CryFreeLibrary(hCryGameDll);
		CryFreeLibrary(hC2Extension);
	}
	else
	{
		MessageBox(0, "Game Startup failed!", "Error", MB_OK);
		pGameStartup->Shutdown();
		pGameStartup = nullptr;
		CryFreeLibrary(hCryGameDll);
		CryFreeLibrary(hC2Extension);
		CloseHandle(mutex);
		return 0;
	}

	return 0;
}

string GetValueFromCmdArray(const CmdArgs& contaner,const string& tag, const char* msg)
{
	if (contaner.find(tag) != contaner.end())
	{
		return contaner.at(tag);
	}
	else
	{
		MessageBoxA(0, msg, "Error", MB_OK);
		throw std::runtime_error(msg);
		return NULL;
	}
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int mCmdShow)
{	
	//CmdArgs arrArgs = GetCmdArgs(lpCmdLine);
	//C2Startup::SCryUserAutho cryUser;

	//if (arrArgs.size() == 0) // port is not required
	//{
	//	MessageBoxA(0, "Invalid Startup Params!", "Error", MB_OK);
	//	return 0;
	//}

	//string forceLaunch = GetValueFromCmdArray(arrArgs, "launch_force_mycrysis", "Game Launcher Param Is Invalid");
	//if (forceLaunch == string("1"))
	//{
	//	cryUser.m_userToken = GetValueFromCmdArray(arrArgs, "uid_token", "User Token is Invalid.");
	//	cryUser.m_nickname = GetValueFromCmdArray(arrArgs, "uid_nickname", "User Nickname is Invalid.");
	//	cryUser.m_ipServer = GetValueFromCmdArray(arrArgs, "game_conn_ip", "Invalid Server Address.");

	//	return RunGame(lpCmdLine, cryUser);
	//}
	//else
	//{
	//	MessageBoxA(0, "Game must be launch from mycrysis launcher!", "Error", MB_OK);
	//	return 0;
	//}

	C2Startup::SCryUserAutho cryUser;
	cryUser.m_userToken = "AAABBBCCCDDD";
	cryUser.m_nickname = "mr.w1lde";

	RunGame(lpCmdLine, cryUser);
	return 0;

}