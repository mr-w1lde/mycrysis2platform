/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  17:35 : Created by Stanislav Migunov.
*************************************************************************/

#include <Windows.h>

#include "QLog/ILogSystem.h"
#include "UProfiles/UProfiles.h"
#include "QIniReader/QIniReader.h"

#include "NetSerialization/Authorization.pb.h"
#include "NetSerialization/OnlineAttributes.pb.h"
#include "NetSerialization/Packet.pb.h"

#ifndef __stdafx_c2ms_h__
#define __stdafx_c2ms_h__

#define SAFE_DELETE(x) { if(x) { x = nullptr; delete x; } }

#define PROGRAM_NAME "MASTER SERVER FOR CRYSIS 2 1.1.1.5620"

#define PROGRAM_VER_MAJOR 0
#define PROGRAM_VER_MINOR 5
#define PROGRAM_BUILD 1101
#define PROGRAM_VER_STR PROGRAM_VER_MAJOR "." PROGRAM_VER_MINOR

template<class Container, class Value>
inline void find_and_erase(Container& container, const Value& value)
{
	typename Container::iterator it = std::find(container.begin(), container.end(), value);
	if (it != container.end())
	{
		container.erase(it);
	}
}

extern QIniManager*		pQIniManager;
extern ILogSystem*		pLogSystem;
extern CUProfiles*		pProfileSystem;

#endif // !__stdafx_c2ms_h__
