/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 13/05/2019  4:46 : Created by Stanislav Migunov.
*************************************************************************/

#include "inih/ini.h"
#include "inih/INIReader.h"

#include <qstring.h>

#ifndef __qini_reader_h__
#define __qini_reader_h__

class QIniManager;
class QIniReader;

class QIniManager
{
public:
									QIniManager();
									~QIniManager() {}
public:
	void							ReadFile(const char* file, QIniReader& readed);
private:
	QString							m_RootPath;
};

class QIniReader
{
	friend class QIniManager;
public:
									QIniReader() {}
									~QIniReader();
public:
	int								GetParseError();
	std::string						GetSVal(std::string section, std::string name, std::string default_value);
	long							GetIVal(std::string section, std::string name, long default_value);
	double							GetRealVal(std::string section, std::string name, double default_value);
	bool							GetBoolVal(std::string section, std::string name, bool default_value);
	const std::set<std::string>		GetSections() const;
	const std::set<std::string>		GetFields(std::string section) const;
protected:
	void							Open(QString filePath);
public:
	INIReader*						m_pFileReader = nullptr;
};

#endif !__qini_reader_h__
