/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 13/05/2019  4:46 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "QIniReader.h"

QIniManager::QIniManager()
{
	pLogSystem->Log("[QIniManager] Initializing...");
	TCHAR szExePathName[_MAX_PATH];
	GetCurrentDirectory(sizeof(szExePathName), szExePathName);

	m_RootPath = szExePathName;
}

void QIniManager::ReadFile(const char* file, QIniReader& readed)
{
	QString fullPath = m_RootPath + "\\" + file;
	readed.Open(fullPath);

	if (readed.GetParseError() == -1)
	{
		pLogSystem->LogError("[QIniManager] Couldn't open %s file.", file);
		return;
	}
}

QIniReader::~QIniReader()
{
	SAFE_DELETE(m_pFileReader);
}

int QIniReader::GetParseError()
{
	return m_pFileReader->ParseError();
}

std::string QIniReader::GetSVal(std::string section, std::string name, std::string default_value)
{
	return m_pFileReader->Get(section, name, default_value);
}

long QIniReader::GetIVal(std::string section, std::string name, long default_value)
{
	return m_pFileReader->GetInteger(section, name, default_value);
}

double QIniReader::GetRealVal(std::string section, std::string name, double default_value)
{
	return m_pFileReader->GetReal(section, name, default_value);
}

bool QIniReader::GetBoolVal(std::string section, std::string name, bool default_value)
{
	return m_pFileReader->GetBoolean(section, name, default_value);
}

const std::set<std::string> QIniReader::GetSections() const
{
	return m_pFileReader->GetSections();
}

const std::set<std::string> QIniReader::GetFields(std::string section) const
{
	return m_pFileReader->GetFields(section);
}

void QIniReader::Open(QString filePath)
{
	m_pFileReader = new INIReader(filePath.toStdString());
}
