/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 11/05/2019  01:19 : Created by Stanislav Migunov.
*************************************************************************/

#include "NetSerialization/OnlineAttributes.pb.h"
#include <string>

#ifndef __c2_uprofiles_h__
#define __c2_uprofiles_h__

class CUProfiles
{
public:
	CUProfiles();
	~CUProfiles();
public:
	void CreateProfile(const char* uprofile);
	bool IsProfileExist(const char* uprofile, bool createProfile = false);

public:
	CrysisProto::CryOnlineAttributes GetProfileData(const char* uprofile);
	bool SetProfileData(const char* uprofile, const CrysisProto::CryOnlineAttributes& data);
private:
	std::string m_folderPath;
};

#endif // !__c2_uprofiles_h__

