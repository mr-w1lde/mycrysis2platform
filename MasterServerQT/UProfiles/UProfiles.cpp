/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 11/05/2019  01:19 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "UProfiles.h"

#include <qfile.h>
#include <QTextStream.h>

#include <fstream>

CUProfiles::CUProfiles()
{
	pLogSystem->Log("[UProfile-System] Initializing...");

	TCHAR szExePathName[_MAX_PATH];
	GetCurrentDirectory(sizeof(szExePathName), szExePathName);

	QIniReader iniConfig;

	pQIniManager->ReadFile("conf.ini", iniConfig);

	std::string localPath = iniConfig.GetSVal("Properties", "saves_path", szExePathName);
	std::string folder_name = iniConfig.GetSVal("Properties", "saves_folder_name", "Profiles");

	m_folderPath = localPath + folder_name;

	CreateDirectory(m_folderPath.c_str(), NULL);
}

CUProfiles::~CUProfiles()
{
}

void CUProfiles::CreateProfile(const char* uprofile)
{
	pLogSystem->Log("[UProfile] Creating save file for user<%s>...", uprofile);

	std::string str = m_folderPath + "\\" + std::string(uprofile) + ".cryproto";
	std::ofstream save_file(str, std::ios::binary | std::ios::out);

	if (!save_file.is_open())
	{
		pLogSystem->LogError("[UProfile] Couldn't create save file for user<%s>...", uprofile);
		return;
	}


	CrysisProto::CryOnlineAttributes onlineAttr;
	CrysisProto::CryUser* pUser = onlineAttr.mutable_user();

	pUser->set_login(uprofile);

	CrysisProto::PlayerProgression* pPlayerProgression = onlineAttr.mutable_player_progression();

	pPlayerProgression->set_initialload(1);

	onlineAttr.SerializeToOstream(&save_file);

	save_file.close();
	
}

bool CUProfiles::IsProfileExist(const char* uprofile, bool createProfile)
{
	pLogSystem->Log("[UProfile] Checking save file for user<%s>...", uprofile);

	std::string str = m_folderPath + "\\" + std::string(uprofile) + ".cryproto";
	std::ifstream save_file(str, std::ios::binary | std::ios::in);
	if (save_file.is_open())
	{
		pLogSystem->LogSuccess("[UProfile] Save file found for user<%s>.", uprofile);
		save_file.close();
		return true;
	}
	else
	{
		if (!save_file.is_open() && createProfile)
		{
			pLogSystem->Log("[UProfile] Save file wasn't found. Creating this one for user<%s>...", uprofile);
			CreateProfile(uprofile);
			return true;
		}

		pLogSystem->LogError("[UProfile] Save file wasn't found for user<%s>.", uprofile);
		return false;
	}

	return false;
}

CrysisProto::CryOnlineAttributes CUProfiles::GetProfileData(const char* uprofile)
{
	pLogSystem->Log("[UProfile] Getting save data for user<%s>...", uprofile);

	std::string str = m_folderPath + "\\" + std::string(uprofile) + ".cryproto";
	std::ifstream save_file(str, std::ios::binary | std::ios::in);


	if (save_file.is_open())
	{
		CrysisProto::CryOnlineAttributes aoDataSendler;

		aoDataSendler.ParseFromIstream(&save_file);

		save_file.close();

		return aoDataSendler;
	}
	else
	{
		pLogSystem->LogError("Poshel naxuy");
	}

	return CrysisProto::CryOnlineAttributes();
}

bool CUProfiles::SetProfileData(const char* uprofile, const CrysisProto::CryOnlineAttributes& data)
{
	std::string str = m_folderPath + "\\" + std::string(uprofile) + ".cryproto";
	std::ofstream save_file(str, std::ios::binary | std::ios::out);

	if (save_file.is_open())
	{
		pLogSystem->Log("[UProfile] Rewriting data of user<%s>...", uprofile);
		save_file.clear();

		data.SerializePartialToOstream(&save_file);

		save_file.close();
	}
	else
	{
		pLogSystem->LogError("[UProfile] Rewriting data error for user<%s>...", uprofile);
		return false;
	}

	return false;
}


