/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  17:35 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include <QtCore/QCoreApplication>

#include "QServer/QServer.h"
#include "QLog/LogSystem.h"

#include <Windows.h>

#include "QServer/QUdpServer.h"

void InitRootPath()
{
	WCHAR szExePathName[_MAX_PATH];
	size_t nLen = GetModuleFileNameW(GetModuleHandle(NULL), szExePathName, _MAX_PATH);

	// Find path above exe name and deepest folder.
	int nCount = 0;
	for (size_t n = nLen - 1; n > 0; n--)
	{
		if (szExePathName[n] == '\\')
		{
			nLen = n;
			if (++nCount == 1)
				break;
		}
	}
	if (nCount > 0)
	{
		szExePathName[nLen++] = 0;

		// Switch to upper folder.
		SetCurrentDirectoryW(szExePathName);

		// Return exe name and relative folder, assuming it's ASCII.
		//if (szExeFileName)
			//wcstombs(szExeFileName, szExePathName + nLen, nSize);
	}
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	InitRootPath();

	CLogSystem* pLog = new CLogSystem();
	pLog->Initialize();

	pLog->Log("[MS-System] Initializing...");

	pQIniManager = new QIniManager();
	pProfileSystem = new CUProfiles();

	QIniReader iniConfig;

	pQIniManager->ReadFile("conf.ini", iniConfig);

	CQtUdpServer* pQtUdpServer = new CQtUdpServer();
	
	if (iniConfig.GetParseError() != -1)
	{
		CQtMServer* pServer = new CQtMServer();

		int port_lobby = iniConfig.GetIVal("Network", "lobby_port", 31000);
		int port_launcher = iniConfig.GetIVal("Network", "cw2_port", 31000);

		QString bind = iniConfig.GetSVal("Network", "bind", "127.0.0.1").c_str();

		pServer->RunServer(bind, port_lobby);
		pQtUdpServer->InitializeUdpServer(bind, port_launcher);
	}
	else
	{
		pLog->LogError("[QServer] conf.ini file not found. Please, create and setup it.");
		exit(1);
	}
	

	return a.exec();
}
