/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 01/06/2019  19:40 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "QUdpServer.h"

#include <qnetworkdatagram.h>

#include "NetSerialization/NetworkDatagram.pb.h"

#include <Windows.h>
#include <qhostinfo.h>

void CQtUdpServer::InitializeUdpServer(QString host, int port)
{
	pLogSystem->Log("[QtUDP] Starting QUDP Server at %s:%u", host.toStdString().c_str(), port);
	QHostAddress b(host);

	m_pUdpSocket = new QUdpSocket();

	if (!m_pUdpSocket->bind(b, port))
	{
		pLogSystem->LogError("[QtUDP] Faild to bind udp socket... port has been bussy.");
	}

	connect(m_pUdpSocket, SIGNAL(readyRead()),
		this, SLOT(readPendingDatagrams()));

	QIniReader iniConfig;
	pQIniManager->ReadFile("conf.ini", iniConfig);

	m_serverRegion = iniConfig.GetSVal("Server", "region", "NULL").c_str();
	m_serverCity = iniConfig.GetSVal("Server", "city", "NULL").c_str();
}

void CQtUdpServer::readPendingDatagrams()
{
	while (m_pUdpSocket->hasPendingDatagrams())
	{
		QNetworkDatagram datagram = m_pUdpSocket->receiveDatagram();
		
		//qDebug() << "\n";
		//qDebug() << "Incomming datagarm: " << datagram.data();

		NetworkDatagram::Datagram data;
		if (data.ParseFromString(datagram.data().toStdString()))
		{
			switch (data.msg_method())
			{
			case NetworkDatagram::Datagram_Method_AUTH:
			{
				NetworkDatagram::Datagram sendlerData;

				sendlerData.set_msg_method(NetworkDatagram::Datagram_Method_GET);
				sendlerData.set_data(QString(m_serverRegion + " : " + m_serverCity).toStdString().c_str());

				QByteArray Data;
				Data.append(sendlerData.SerializeAsString().c_str());

				m_pUdpSocket->writeDatagram(Data, datagram.senderAddress(), datagram.senderPort());

				//qDebug() << "Datagram was send to : " << datagram.senderAddress();

				break;
			}
			}
		}
		else
		{
			pLogSystem->LogError("[QtMessage] Couldn't parse a datagram...");
		}
	}
}
