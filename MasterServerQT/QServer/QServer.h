/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  18:52 : Created by Stanislav Migunov.
*************************************************************************/

#include <qtcpserver.h>
#include <qtcpsocket.h>

#include "QClient/QClient.h"

#ifndef __c2_qt_mserver_h__
#define __c2_qt_mserver_h__

class CQtMServer : public QTcpServer
{
	Q_OBJECT

public:
	CQtMServer();
	~CQtMServer();
public:
	void			RunServer(QString bind, int port);
public:
	// QTcpServer
	virtual void	incomingConnection(qintptr handle);
	//~QTcpServer	
public slots:
	void RemoveClient(QClient* pClient);
private:
	QTcpSocket*		m_pMSocket;
	QByteArray		m_pBufferData;
private:
	int				m_msPort;
private:
	QList<QClient*>	m_pClients;
};

#endif // !__c2_qt_mserver_h__
