/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  18:52 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "QServer.h"

CQtMServer::CQtMServer()
	: QTcpServer(new QTcpServer())
{
}

CQtMServer::~CQtMServer()
{
}

void CQtMServer::RunServer(QString bind, int port)
{
	m_msPort = port;

	pLogSystem->Log("[QServer] Running MS for Crysis 2...");

	QHostAddress b(bind);

	if (this->listen(b, m_msPort))
	{
		pLogSystem->LogSuccess("[QServer] Binding server<%s:%u>",bind.toStdString().c_str() , m_msPort);
		this->waitForNewConnection();
	}
	else
	{
		pLogSystem->LogError("[QServer] %u is bussy now...", port);
	}
}

void CQtMServer::RemoveClient(QClient* pClient)
{
	find_and_erase(m_pClients, pClient);

	qDebug() << "Removing client from array... Now array size = " << m_pClients.size();
}

void CQtMServer::incomingConnection(qintptr handle)
{
	QAbstractSocket* pIncomeSocket = this->nextPendingConnection();

	QClient* pConnectedClient = new QClient(handle);

	connect(pConnectedClient, SIGNAL(RemoveEvent(QClient*)), this, SLOT(RemoveClient(QClient*)), Qt::DirectConnection);
	pConnectedClient->run();

	m_pClients.push_back(pConnectedClient);


	qDebug() << "QClient array len = " << m_pClients.size();
}
