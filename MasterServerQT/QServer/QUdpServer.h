/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 01/06/2019  19:40 : Created by Stanislav Migunov.
*************************************************************************/

#include <qudpsocket.h>

#include "QClient/QClient.h"

#ifndef __c2_qt_udp_server_h__
#define __c2_qt_udp_server_h__

class CQtUdpServer : public QObject
{
	Q_OBJECT

public:
	void InitializeUdpServer(QString host, int port);

public slots:
	void readPendingDatagrams();

private:
	void SendGetRequest(QUdpSocket* sendTo);
private:
	QUdpSocket* m_pUdpSocket;

	QString m_serverRegion;
	QString m_serverCity;
};

#endif // !__c2_qt_udp_server_h__
