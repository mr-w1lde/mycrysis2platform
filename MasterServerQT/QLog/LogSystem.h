/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  21:07 : Created by Stanislav Migunov.
*************************************************************************/

#include "ILogSystem.h"

#include <string>
#include <vector>

#ifndef __log_system_h__
#define __log_system_h__

class CLogToFile;

class CLogSystem : public ILogSystem
{
public:
	CLogSystem();
	~CLogSystem();
public:
	void Initialize();
	void SendLog(std::string msg, va_list args, ELogType type);
public:
	// ILogSystem
	virtual void Log(const char* msg, ...) override;
	virtual void LogError(const char* msg, ...) override;
	virtual void LogWarning(const char* msg, ...) override;
	virtual void LogSuccess(const char* msg, ...) override;
	virtual void FatalError(const char* msg, ...) override;

	virtual void AddListener(ILogEventCallback* listener) override;
	virtual void RemoveListener(ILogEventCallback* listener) override;
	//~ILogSystem
private:
	std::vector<ILogEventCallback*> m_logCallbackListener;
	CLogToFile*						m_pLogToFile = nullptr;
};

struct SSystemInfo
{
	std::string m_ProcessorName;
	std::string m_ProcessorCores;
	std::string m_ProcessorArchitecture;
	std::string m_OS_NamePC;
	std::string m_OS_NameUser;
	std::string m_OS_Version;
	std::string m_OS_BuildVersion;

	std::vector <std::wstring> m_VideoDrivesName;
	std::vector <std::string> m_VideoDrivesMemory;

	std::string m_Ram;
};

class CLogToFile : public ILogEventCallback
{
public:
	CLogToFile(const char* fileName);
	~CLogToFile();
public:
	// ILogEventCallback
	virtual void OnLogEvent(const char* msg, ELogType type) override;
	//~ILogEventCallback

private:
	void GetTime();
	void GetSystemInfo();
	void WriteSystemInfoToFile();

private:
	tm* m_pTm;
	SSystemInfo* m_pSystemInfo;
	std::string m_currentTime;
	std::string m_currentDate;
	std::string m_path;
};

#endif // !__log_system_h__
