/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  21:03 : Created by Stanislav Migunov.
*************************************************************************/


#ifndef __interface_log_listener_h__
#define __interface_log_listener_h__

enum ELogType
{
	eLT_Log = 0,
	eLT_LogError,
	eLT_LogWarning,
	eLT_LogSuccess,
	eLT_FatalError
};

struct ILogEventCallback
{
	virtual void OnLogEvent(const char* msg, ELogType type) = 0;
};

struct ILogSystem
{
	virtual void Log(const char* msg, ...) = 0;
	virtual void LogError(const char* msg, ...) = 0;
	virtual void LogWarning(const char* msg, ...) = 0;
	virtual void LogSuccess(const char* msg, ...) = 0;
	virtual void FatalError(const char* msg, ...) = 0;

	virtual void AddListener(ILogEventCallback* listener) = 0;
	virtual void RemoveListener(ILogEventCallback* listener) = 0;

	//destructor
	virtual ~ILogSystem() {};
};

#endif // !__interface_log_listener_h__
