/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 10/05/2019  17:35 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"

QIniManager*	pQIniManager = nullptr;
ILogSystem*		pLogSystem = nullptr;
CUProfiles*		pProfileSystem = nullptr;