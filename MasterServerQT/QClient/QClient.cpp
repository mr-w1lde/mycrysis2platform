/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 12/05/2019  02:17 : Created by Stanislav Migunov.
*************************************************************************/
#include "StdAfx.h"
#include "QClient.h"

QClient::QClient(qintptr handler)
{
	m_pSockId = handler;
}

QClient::~QClient()
{
}

bool QClient::SendData(const char* data, quint64 ulen)
{
	m_pClientSocket->waitForConnected();

	if (m_pClientSocket->state() == QAbstractSocket::ConnectedState)
	{
		pLogSystem->Log("[QServer] Sending data to QClient<%s:%u>", m_clientHostAddress.toString().toStdString().c_str(), (int)m_clientPort);
		m_pClientSocket->flush();
		m_pClientSocket->write(data, ulen);

		bool sending = true;

		while (m_pClientSocket->bytesToWrite() > 0)
		{
			if (!m_pClientSocket->waitForBytesWritten())
			{
				pLogSystem->LogError("[QServer] Too large data in socket...");
				sending = false;
			}

			sending = true;
		}

		return sending;
	}
	else
	{
		pLogSystem->LogError("[QServer] Couldn't send data to QClient<%s:%u>. Connection is lost.", m_clientHostAddress.toString().toStdString().c_str(), (int)m_clientPort);
		return false;
	}

	return false;
}

void QClient::SetClientType(EClientType type)
{
	pLogSystem->Log("[QClient] Client with id<%u> changed type<%u>", (int)m_pSockId, type);
	m_clientType = type;
}

void QClient::run()
{
	m_pClientSocket = new QTcpSocket();

	this->setParent(m_pClientSocket);

	if (!m_pClientSocket->setSocketDescriptor(m_pSockId))
	{
		pLogSystem->LogError("[QClient] Couldn't setup socket. Error id<%u>.", m_pClientSocket->error());
		return;
	}

	connect(m_pClientSocket, SIGNAL(readyRead()), this, SLOT(OnReadReady()));
	connect(m_pClientSocket, SIGNAL(disconnected()), this, SLOT(OnDisconnect()));

	m_clientHostAddress = m_pClientSocket->peerAddress();
	m_clientPort = m_pClientSocket->peerPort();

	pLogSystem->Log("[QClient] Client connected <%s:%u> [id<%u> : type<%u>]", m_clientHostAddress.toString().toStdString().c_str(), (int)m_clientPort, (int)m_pSockId, m_clientType);

}

void QClient::OnReadReady()
{
	while (m_pClientSocket->bytesAvailable() > 0)
	{
		pLogSystem->Log("[NET] Receiving data from QClient<%u>", (int)m_pSockId);
		m_pBuffer = m_pClientSocket->readAll();

		qDebug() << "Data from client<" << m_pSockId << ">:" << m_pBuffer;
		qDebug() << "Data len <" << m_pSockId << ">:" << m_pBuffer.size();

		MasterServer::Packet packet;
		if (packet.ParseFromArray(m_pBuffer.data(), m_pBuffer.size()))
		{
			//packet.PrintDebugString();
			switch (packet.type())
			{
			case MasterServer::Packet_Type::Packet_Type_AUTH_USER:
			{
				SetClientType(IS_CLIENT_CRYSIS);

				//Later using SQL;
				MasterServer::AuthorizationPacket aoPacket;

				if (!aoPacket.ParseFromArray(packet.data().c_str(), packet.len()))
				{
					pLogSystem->LogError("[Protobuf] Invalid AuthorizationPacket data.");
					return;
				}

				const char* nickname = aoPacket.token().c_str();

				//Create or send save data.
				if (pProfileSystem->IsProfileExist(nickname, true))
				{
					CrysisProto::CryOnlineAttributes data = pProfileSystem->GetProfileData(nickname);

					size_t dataSize = data.ByteSizeLong();

					MasterServer::Packet packet;
					packet.set_type(MasterServer::Packet_Type::Packet_Type_USER_SAVE_DATA);

					std::string dataStr = data.SerializeAsString();

					packet.set_data(dataStr);
					packet.set_len(dataSize);


					SendData(packet.SerializeAsString().c_str(), packet.ByteSizeLong());
				}

				break;
			}
			case MasterServer::Packet_Type::Packet_Type_DISCONNECT:
			{
				//Client Disconnect
				break;
			}
			case MasterServer::Packet_Type::Packet_Type_USER_SAVE_DATA:
			{//Reciving the user save data.

				//Set Profile Data.
				CrysisProto::CryOnlineAttributes data;

				if (data.ParseFromArray(packet.data().data(), packet.len()))
				{
					const char* nickname = data.user().token().c_str();

					if (pProfileSystem->IsProfileExist(nickname, false))
						pProfileSystem->SetProfileData(nickname, data);
					else
						pLogSystem->LogError("[Protobuf] Couldn't get online attributes...");
				}
				else
				{
					pLogSystem->LogError("[Protobuf] Invalid CryOnlineAttributes data.");
				}
				break;
			}
			}
		}
		else
		{
			pLogSystem->LogError("[Protobuf] Parsing Error...");
			//m_pClientSocket->write("Parse error. Try again!\n");
			//m_pClientSocket->waitForBytesWritten(300);
		}
	}
}

void QClient::OnDisconnect()
{
	emit RemoveEvent(this);

	pLogSystem->Log("[QClient] Client has been disconnected <%s:%u> [id<%u> : type<%u>]", m_clientHostAddress.toString().toStdString().c_str(), (int)m_clientPort, (int)m_pSockId, m_clientType);
	m_pClientSocket->deleteLater();
}


