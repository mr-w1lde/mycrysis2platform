/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- 12/05/2019  02:17 : Created by Stanislav Migunov.
*************************************************************************/

#include <qtcpsocket.h>
#include <qtcpserver.h>

#include <qthread.h>

#include "UProfiles/UProfiles.h"

#ifndef __c2_qclient_h__
#define __c2_qclient_h__

enum class EClientType : int
{
	eCT_UNKNOWN = -1,
	eCT_CRYSIS = 1,
	eCT_DEDICATED
};

#define IS_CLIENT_UNKNOWN EClientType::eCT_UNKNOWN
#define IS_CLIENT_CRYSIS EClientType::eCT_CRYSIS
#define IS_CLIENT_DEDICATED EClientType::eCT_DEDICATED

class QClient : public QObject
{
	Q_OBJECT

public:
	QClient(qintptr handler);
	~QClient();
public:
	bool				SendData(const char* data, quint64 ulen);
	void				SetClientType(EClientType type);
public:
	const EClientType	GetClientType() const { return m_clientType; }
public:
	void				run();
public slots:
	void				OnReadReady();
	void				OnDisconnect();
signals:
	void				RemoveEvent(QClient*);
private:
	QTcpSocket*			m_pClientSocket;
	QByteArray			m_pBuffer;
	qintptr				m_pSockId;
	QHostAddress		m_clientHostAddress;
	quint16				m_clientPort;
private:
	EClientType			m_clientType = IS_CLIENT_UNKNOWN;
};

#endif // !__c2_qclient_h__
