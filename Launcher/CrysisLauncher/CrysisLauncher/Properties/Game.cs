﻿using System.Configuration;

namespace CrysisLauncher.Properties {

    [System.Serializable]
    internal class C2QualitySettings
    {
        enum EQuality
        {
            Low = 0, Medium, High, Ultra
        }

        enum EMSAA
        {
            Off = 0, x4, x6, x8, x10
        };

#pragma warning disable 414

        EQuality Quality = EQuality.Low;
        EQuality ObjectDetail = EQuality.Low;
        EQuality Effects = EQuality.Low;
        EQuality Shading = EQuality.Low;
        EQuality Shadows = EQuality.Low;
        EQuality Physics = EQuality.Low;
        EQuality VolEfects = EQuality.Low;
        EQuality Textures = EQuality.Low;
        EQuality PostProc = EQuality.Low;
        EQuality Sound = EQuality.Low;
        EQuality Wather = EQuality.Low;
        EQuality Practicles = EQuality.Low;
        EQuality SSAO = EQuality.Low;

        EMSAA MSAA = EMSAA.Off;

#pragma warning restore 414

        [System.ComponentModel.DefaultValue(90)]
        int Fow
        {
            get
            {
                return Fow;
            }
            set
            {
                if ((value >= 50) && value <= 130)
                    Fow = value;
                else
                    throw new System.ArgumentOutOfRangeException("Fow must be beetwen 50 and 130!");
            }
        }

#pragma warning disable CS0414 // Полю "C2QualitySettings.vsync" присвоено значение, но оно ни разу не использовано.
        bool    vsync = false;
#pragma warning restore CS0414 // Полю "C2QualitySettings.vsync" присвоено значение, но оно ни разу не использовано.
#pragma warning disable CS0414 // Полю "C2QualitySettings.shadows" присвоено значение, но оно ни разу не использовано.
        bool    shadows = false;
#pragma warning restore CS0414 // Полю "C2QualitySettings.shadows" присвоено значение, но оно ни разу не использовано.
#pragma warning disable CS0414 // Полю "C2QualitySettings.skipIntro" присвоено значение, но оно ни разу не использовано.
        bool    skipIntro = false;
#pragma warning restore CS0414 // Полю "C2QualitySettings.skipIntro" присвоено значение, но оно ни разу не использовано.
    }

    internal sealed class CrysisSettings : ApplicationSettingsBase
    {
    // If you used [ApplicationScopedSetting()] instead of [UserScopedSetting()],
    // you would NOT be able to persist any data changes!
        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.Binary)]
        [DefaultSettingValue("")]
        public C2QualitySettings CustomEmployees
        {
            get
            {
                return ((C2QualitySettings)this["CustomEmployees"]);
            }
            set
            {
                this["CustomEmployees"] = (C2QualitySettings)value;
            }
        }
    }

    internal sealed partial class Game {
        


        public Game() {
            // // Для добавления обработчиков событий для сохранения и изменения параметров раскомментируйте приведенные ниже строки:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Добавьте здесь код для обработки события SettingChangingEvent.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Добавьте здесь код для обработки события SettingsSaving.
        }
    }
}
