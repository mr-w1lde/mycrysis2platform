﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/

using System;
using System.Collections.Generic;

namespace CrysisLauncher
{
    public class Updater
    {
        HttpClient httpClient;
        string checksummFileRaw = null;
        Md5 Md5 = new Md5();

        string currfile;

        public updateStatus currentStatus { get; private set;}

        public delegate void UpdateEvents(updateStatus? status, System.Net.WebExceptionStatus exceptionStatus,string msg);
        public delegate void FileProgression(int status,string file,int filesTotal, int CurrentFile, string speed);

        public event UpdateEvents Succses;
        public event UpdateEvents NeedToDownload;
        public event UpdateEvents Fail;
        public event FileProgression StatusUpdate;

        bool FTPerror;

        public enum updateStatus
        {
            Success = 0,
            HTTP_return_null_checksumm,
            Busy,
            Fail
        }

        struct ChecksummFile
        {
            public string relPath;
            public string hash;

            public ChecksummFile(string relPath, string hash)
            {
                this.relPath = relPath;
                this.hash = hash;
            }
        }

        List<ChecksummFile> checksummFiles = new List<ChecksummFile>();
        List<String> filesToDwnld = new List<string>();

        public void CheckUpdates()
        {

            httpClient = new HttpClient();

            httpClient.Update += HTTPclient_Update;

            httpClient.Fail += HTTPclient_Fail;

            GetChecksummFile();

            if(string.IsNullOrEmpty(checksummFileRaw))
            {
                Fail?.Invoke(updateStatus.HTTP_return_null_checksumm,System.Net.WebExceptionStatus.Success,"");
                return;
            }

            PharseChecksummFile();

            if(checksummFiles.Count == 0)
            {
                Fail?.Invoke(updateStatus.HTTP_return_null_checksumm,System.Net.WebExceptionStatus.Success, "");
                return;
            }

            foreach(ChecksummFile File in checksummFiles)
            {
                if(CompareFiles(File) == false)
                {
                    filesToDwnld.Add(File.relPath);
                }
            }

            if (filesToDwnld.Count == 0)
            {

            }
            else
            {
                NeedToDownload?.Invoke(updateStatus.Busy, System.Net.WebExceptionStatus.Success,"");
                httpClient.InitSpeedCounter();


                foreach (String file in filesToDwnld)
                {
                    currfile = file;
                    httpClient.DownloadFile("http://" + CrysisLauncher.Properties.Settings.Default.HTTPserver + file, AppDomain.CurrentDomain.BaseDirectory + file);
                    if (FTPerror)
                        break;
                }
                httpClient.StopSpeedCounter();
            }
            if (!FTPerror)
                Succses?.Invoke(updateStatus.Success, System.Net.WebExceptionStatus.Success,"");
        }

        private void HTTPclient_Fail(System.Net.WebExceptionStatus exceptionStatus,string msg)
        {
            FTPerror = true;
            Fail?.Invoke(updateStatus.Fail, exceptionStatus,msg);
        }

        private void HTTPclient_Update(int progress, string speed)
        {
            StatusUpdate?.Invoke(progress, currfile, filesToDwnld.Count, filesToDwnld.LastIndexOf(currfile),speed);
        }

        void GetChecksummFile()
        {
            checksummFileRaw = httpClient.GetFile("http://"+CrysisLauncher.Properties.Settings.Default.HTTPserver + "/MD5-CHECKSUM.txt ");
        }

        void PharseChecksummFile()
        {
            checksummFileRaw = checksummFileRaw.Trim('\0');
            char dfs = Convert.ToChar(32);
            checksummFileRaw = checksummFileRaw.Trim(' ');
            String[] rows = checksummFileRaw.Split('\n');
            foreach(String row in rows)
            {
                if (!string.IsNullOrWhiteSpace(row) && row.IndexOf(':') > 0)
                {
                    String[] tmp = row.Split(':');

                    checksummFiles.Add(new ChecksummFile(tmp[0], tmp[1]));
                }
            }
        }

        bool CompareFiles(ChecksummFile file)
        {
            string localMd5 = Md5.GetMd5Summ(AppDomain.CurrentDomain.BaseDirectory + file.relPath.Substring(1));

            return string.Equals(localMd5, file.hash);
        }

        
    }
}
