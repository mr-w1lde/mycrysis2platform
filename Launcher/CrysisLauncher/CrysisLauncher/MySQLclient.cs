﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/

using System;
using MySql.Data.MySqlClient;

namespace CrysisLauncher
{
    static class MySQLclient
    {
        private static readonly string ConnectionString = @"Data Source=sql190.main-hosting.eu;Database=u647652329_users;" +
                                                 @"User id=u647652329_admin; Password=q6Rs85d+cv$lVSoRc[;";

        private static MySqlConnection myConnection;

        private static EMysqlState mysqlState;

        public static EMysqlState MysqlState
        {
            get
            {
                return mysqlState;
            }
            private set
            {
                
                mysqlState = value;
                StateChanged(value);
            }
        }

        public enum EMysqlState
        {
            eClosed = 0,
            eOpen,
            eConnecting,
            eTimeOut,
            eSqlException,
            eBroken,
            eExecuting,
            eFetching,
            eServerUnavaible,
            eUnknown
        }

        public delegate void MySqlconnectionEvents(EMysqlState mysqlState);

        public static event MySqlconnectionEvents StateChanged;

        public static void CloseConnection()
        {
            myConnection.Close();
            mysqlState = EMysqlState.eClosed;
        }

        public static void ConnectToServer()
        {
            MysqlState = EMysqlState.eConnecting;

            myConnection = new MySqlConnection(ConnectionString);

            try
            {
                myConnection.Open();
            }
            catch (TimeoutException)
            {
                MysqlState = EMysqlState.eTimeOut;
                myConnection.Close();
                return;
            }
            catch(MySqlException)
            {
                MysqlState = EMysqlState.eSqlException;
            }

            if (!myConnection.Ping())
            {
                MysqlState = EMysqlState.eServerUnavaible;
                return;
            }

            MysqlState = EMysqlState.eOpen;
        }

        public static Authentication.AuthData? Login(string login, string password)
        {
            MySqlCommand myCommand = new MySqlCommand("select GetIdByLogin(\"" + login + "\");", myConnection);
            string ret = myCommand.ExecuteScalar().ToString();
            int id = int.Parse(ret);
            myCommand = new MySqlCommand("select AuthenticateUser(" + id + ",'" + password + "');", myConnection);
            object token = myCommand.ExecuteScalar();
            myCommand = new MySqlCommand("select Nickname from Users where Users.id = " + id + ";", myConnection);
            object Nick = myCommand.ExecuteScalar();

            if ((string)token != "")
            {
                Authentication.AuthData authData = new Authentication.AuthData
                {
                    Token = (string)token,
                    ID = id,
                    Login = login,
                    Password = password,
                    Nick = (string)Nick
                };

                return authData;
            }
            else
            {
                return null;
            }
        }

        public static void UnLogin()
        {
            myConnection.Close();
            ConnectToServer();
            MySqlCommand myCommand = new MySqlCommand("select LogOut(" + Authentication.currentAuthData.Value.ID+");", myConnection);
            object debug = myCommand.ExecuteScalar();
        }
    }
}
