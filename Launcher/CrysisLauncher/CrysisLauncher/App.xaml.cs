﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace CrysisLauncher
{


    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static List<CultureInfo> m_Languages = new List<CultureInfo>();

        public static System.Timers.Timer GlobalTimer1sec = new System.Timers.Timer(1000);

        public static Window GetMainWindow;

        public static string serverip;
        public static string port;


        public static List<CultureInfo> Languages
        {
            get
            {
                return m_Languages;
            }
        }

        public App()
        {
            App.GlobalTimer1sec.AutoReset = true;
            App.GlobalTimer1sec.Start();

            CMainWindow.WindowLoaded += ShowLoginWindow;

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            string[] args = Environment.GetCommandLineArgs();

            //RegisterMyProtocol(args[0]);    
            
        }

        private void ShowLoginWindow()
        {
            LoginWindow loginWindow = new LoginWindow();
            bool succeslogin = false;
            if (CrysisLauncher.Properties.Settings.Default.LoggedIn)
            {
                succeslogin =loginWindow.TryToAutologin();
            }

            if (succeslogin)
            {
                MainWindow.Show();
            }
            else
            {
                MainWindow = loginWindow;

                loginWindow?.Show();
                loginWindow?.Activate();

                loginWindow.Closed += ShowMainWindow;
            }
        }

        private void ShowMainWindow(object sender, EventArgs e)
        {
            MainWindow = GetMainWindow;
            MainWindow.Show();
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            if (Authentication.currentAuthData != null)
                if (string.IsNullOrEmpty(Authentication.currentAuthData.Value.Token))
                    MySQLclient.UnLogin();
        }

        protected override void OnLoadCompleted(NavigationEventArgs e)
        {
            base.OnLoadCompleted(e);
        }

        static void RegisterMyProtocol(string AppPath)
        {
            RegistryKey key = Registry.ClassesRoot.OpenSubKey("MyCrysisLauncher");

            if (key == null)  //if the protocol is not registered yet...we register it
            {
                key = Registry.ClassesRoot.CreateSubKey("MyCrysisLauncher");
                key.SetValue(string.Empty, "URL: MyCrysisLauncher Protocol");
                key.SetValue("URL Protocol", string.Empty);

                key = key.CreateSubKey(@"shell\open\command");
                key.SetValue(string.Empty, AppPath + " " + "%1");
            }

            key.Close();
        }
    }


}
