﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.Protobuf;
using Google;

using NetworkDatagram;

using System.Net.Sockets;
using System.Net;

using System.IO;


namespace CrysisLauncher
{
    class MasterUdpClient
    {
        private UdpClient _masterSocket;

        public delegate void UpdClientDelegate(string server);
        public event UpdClientDelegate ClientConnected;
        public event UpdClientDelegate ClientFailded;

        public void InitializeConnection(string host, int port)
        {
            
            _masterSocket = new UdpClient();
            _masterSocket.Client.ReceiveTimeout = 120;
            _masterSocket.Connect(host, port);

            Datagram datagram = new Datagram();

            datagram.MsgMethod = Datagram.Types.Method.Auth;
            datagram.Data = "cw2_launcher";

            byte[] sendlerData = ((IMessage)datagram).ToByteArray();

            _masterSocket.Send(sendlerData, sendlerData.Length);

            IPEndPoint remoteIp = null;

            //Сделать проверку на получение данных... Если данные не были получены в теченн
            //определенного времени, то сделать переотправку датаграмы!
            byte[] receiveData = null;

            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    receiveData = _masterSocket.Receive(ref remoteIp); // получаем данные
                }
                catch (SocketException)
                {
                    continue;
                }

                Stream stream = new MemoryStream(receiveData);
                Datagram getableData = GetProtoMessage<Datagram>(stream);
                ClientConnected?.Invoke(getableData.Data);
                _masterSocket.Close();
                return;
            }
            _masterSocket.Close();

            ClientFailded?.Invoke(null);

        }

        private T GetProtoMessage<T>(Stream stream) where T : IMessage, new()
        {
            T message = new T();
            message.MergeFrom(stream);
            return message;
        }
    }
}
