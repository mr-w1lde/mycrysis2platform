﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CrysisLauncher
{
    class HttpClient
    {


        public delegate void HTTPclientEvents(WebExceptionStatus exceptionStatus, string mesg);
        public event HTTPclientEvents Fail;
        public event HTTPclientEvents Succes;

        public delegate void HTTPclientProgress(int progress, string speed);
        public event HTTPclientProgress Update;

        Timer timer = new Timer(1000);

        int totalSize = 0;
        int completion = 0;

        public void InitSpeedCounter()
        {
            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = true;
            timer.Start();
        }

        public void StopSpeedCounter()
        {
            timer.Close();
        }

        public String GetFile(string serverUri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serverUri);

            request.Method = WebRequestMethods.Http.Get;


            HttpWebResponse httpResponse;

            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException exc)
            {
                Fail?.Invoke(exc.Status, exc.Message);
                return null;
            }

            String response = null;

            int size = 64;
            char[] buffer = new char[64];


            Stream responseStream = httpResponse.GetResponseStream();
            StreamReader responseFileStream = new StreamReader(responseStream);

            while (size == 64)
            {
                size = responseFileStream.Read(buffer, 0, buffer.Length);

                response += new String(buffer);

                Array.Clear(buffer, 0, buffer.Length);

            }

            Succes?.Invoke(WebExceptionStatus.Success, "");
            return response;

        }
        public void DownloadFile(string serverUri, string filePath)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serverUri);

            request.Method = WebRequestMethods.Http.Get;

            HttpWebResponse httpResponse;

            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException exc)
            {
                Fail?.Invoke(exc.Status, exc.Message);
                return;
            }

            const int buffSize = 1024;

            int size = 64;
            byte[] buffer = new byte[buffSize];

            try
            {
                System.IO.Directory.CreateDirectory(filePath.Remove(filePath.LastIndexOf('\\'), filePath.Length - filePath.LastIndexOf('\\')));
                using (FileStream file = new FileStream(filePath.Replace('/', '\\'), FileMode.Create))
                using (Stream responseFileStream = httpResponse.GetResponseStream())
                {
                    while (size > 0)
                    {
                        try
                        {
                            size = responseFileStream.Read(buffer, 0, buffer.Length);
                        }
                        catch (IOException)
                        {

                            file.Close();
                            file.Dispose();
                            break;
                        }

                        file.Write(buffer, 0, size);

                        Array.Clear(buffer, 0, buffer.Length);

                        completion = (int)(file.Length / (float)httpResponse.ContentLength * 100f);

                        totalSize += size;
                    }
                }
            }
            catch (WebException exp)
            {
                Fail?.Invoke(exp.Status, exp.Message);
            }
            Succes?.Invoke(WebExceptionStatus.Success, "");
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            float len = totalSize;

            string[] sizes = { "b/s", "Kb/s", "Mb/s", "Gb/s", "Tb/s" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len /= 1024;
            }

            Update?.Invoke(completion, String.Format("{0:0.##} {1}", len, sizes[order]));
            totalSize = 0;
        }



    }
}
