﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/

using System;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CrysisLauncher
{
    public class Md5
    {
        MD5 md5 = MD5.Create();

        public string GetMd5Summ(string path)
        {

            string hash = null;
            using (var md5 = MD5.Create())
            {
                try
                {
                    using (var stream = File.OpenRead(path))
                    {
                        var hashtmp = md5.ComputeHash(stream);
                        hash = BitConverter.ToString(hashtmp).Replace("-", "").ToLowerInvariant();
                    }
                }
                catch (IOException e)
                {
                    hash = null;
                }
            }

            return  hash;
        }
    }
}
