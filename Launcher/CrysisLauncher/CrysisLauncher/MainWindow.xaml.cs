﻿using System;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace CrysisLauncher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class CMainWindow : Window
    {
        public delegate void WindowEvent();
        public static event WindowEvent WindowLoaded;

        private void Window_Initialized(object sender, EventArgs e)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            LauncherVersion.Content = CrysisLauncher.Properties.Settings.Default.LauncherVer;
            Game_Build.Content = CrysisLauncher.Properties.Settings.Default.GameBuild;
            WindowLoaded += CMainWindow_WindowLoaded;
            App.GetMainWindow = this;
            Download_status_label.Visibility = Visibility.Hidden;
            Hide();

            WindowLoaded?.Invoke();
        }

        private void CMainWindow_WindowLoaded()
        {

            MasterUdpClient masterServer = new MasterUdpClient();
            masterServer.ClientConnected += MasterServer_ClientConnected;
            masterServer.ClientFailded += MasterServer_ClientFailded;
            masterServer.InitializeConnection("mycrysis.online", 31000);

            //NickName.Content = CrysisLauncher.Properties.Settings.Default.Username;
        }

        private void MasterServer_ClientFailded(string server)
        {
            Server_name.Content = (string)Application.Current.FindResource("m_main_error_connectMS");
            OnlineIndicator.Foreground = (Brush)Resources["OnlineRed"];

        }

        private void MasterServer_ClientConnected(string server)
        {
            Server_name.Content = server;
            OnlineIndicator.Foreground = (Brush)Resources["OnlineGreen"];
            
        }

        private void Label3_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Rectangle1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Play()
        {
            string path = @AppDomain.CurrentDomain.BaseDirectory + CrysisLauncher.Properties.Game.Default.GamePath;
            string args = "-launch_force_mycrysis 1 -uid_token "
                   + CrysisLauncher.Authentication.currentAuthData.Value.Token
                   + " -uid_nickname "
                   + Authentication.currentAuthData.Value.Nick
                   + " -game_conn_ip 127.0.0.1 -cmd_end";

            try
            {
                System.Diagnostics.Process.Start(path, args);
            }
            catch(System.ComponentModel.Win32Exception exp)
            {
                MessageBox.Show(exp.Message + ": " + path, (string)Application.Current.FindResource("m_main_error_FileError"));
            }
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Updater updater = new Updater();

            Download_status_label.Content = "";
            ProgressBar_file.IsIndeterminate = true;
            PlayButton.IsEnabled = false;

            updater.NeedToDownload += Updater_NeedToDownload;
            updater.StatusUpdate += Updater_StatusUpdate;
            updater.Succses += Updater_Succses;
            updater.Fail += Updater_Fail;

            TaskbarItemInfo = new System.Windows.Shell.TaskbarItemInfo();
            Play_button_label.Content = (string)Application.Current.FindResource("m_main_menu_play_init");
            PlayButton.Background = (Brush)Resources["ProcessingButtonColor"];
            ProgressBar_file.Foreground = (Brush)Resources["ProgressBarDefault"];

            RotationStart_BeginStoryboard.Storyboard.Completed += (snder, ev) =>
            {
                Rotation_BeginStoryboard.Storyboard.RepeatBehavior = RepeatBehavior.Forever;
                Rotation_BeginStoryboard.Storyboard.Begin();
            };

            RotationStart_BeginStoryboard.Storyboard.Begin();

            App.GlobalTimer1sec.Elapsed += GlobalTimer1sec_Elapsed;



            updateCheck(updater);

            Download_status_label.Visibility = Visibility.Visible;
            Download_status_label.Content = (string)Application.Current.FindResource("m_Update_DwnldChechsummsFile");
        }

        private void GlobalTimer1sec_Elapsed(object sender, ElapsedEventArgs e)
        {
            ProgressBar_file_LayoutUpdated();
        }

        private void Updater_StatusUpdate(int status, string file, int filesTotal, int CurrentFile,string speed)
        {
                Dispatcher.Invoke(() =>
                {
                    ProgressBar_file.Maximum = filesTotal;
                    ProgressBar_file.Value = CurrentFile;
                    Download_status_label.Content = string.Format("[{0}({1} %) {2}]", file, status,speed);

                });

        }

        private void ProgressBar_file_LayoutUpdated()
        {
            Dispatcher.Invoke(() =>
            {
                if (ProgressBar_file.IsIndeterminate)
                {
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Indeterminate;
                }
                else
                {
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
                }
                if (ProgressBar_file.Foreground == (Brush)Resources["ProgressBarError"])
                {
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Error;
                }
                TaskbarItemInfo.ProgressValue = ProgressBar_file.Value / ProgressBar_file.Maximum;
            });
        }

        private async void updateCheck(Updater updater)
        {
            await Task.Run(()=>updater.CheckUpdates());
            ProgressBar_file.IsIndeterminate = false;
        }

        private void Updater_Fail(Updater.updateStatus? status, System.Net.WebExceptionStatus exceptionStatus, string msg)
        {
            Dispatcher.Invoke(() =>
            {
                ProgressBar_file.IsIndeterminate = false;
                ProgressBar_file.Value = 100;
                ProgressBar_file.Foreground = (Brush)Resources["ProgressBarError"];

                string ErrorMessage = "Unknown error";
                switch (status)
                {
                    case Updater.updateStatus.HTTP_return_null_checksumm:
                        ErrorMessage = (string)Application.Current.FindResource("m_Update_error_ftpChechsummsFileError");
                        break;
                    default:
                        ErrorMessage = msg;
                        break;
                }

                Download_status_label.Content = (string)Application.Current.FindResource("m_Update_error") + " "+ErrorMessage;
                //Download_status_label.Foreground = (Brush)FindResource("ProgressBarRed");
                Download_status_label.Visibility = Visibility.Visible;

                Play_button_label.Content = (string)Application.Current.FindResource("m_Update_TryAgain");

                PlayButton.IsEnabled = true;
                PlayButton.Background = (Brush)FindResource("TryAgainButtonColor");

                App.GlobalTimer1sec.Stop();
            });

        }

        private void Updater_Succses(Updater.updateStatus? status, System.Net.WebExceptionStatus exceptionStatus, string ms)
        {
            Dispatcher.Invoke(() =>
            {
                ProgressBar_file.IsIndeterminate = false;
                ProgressBar_file.Foreground = (Brush)Resources["ProgressBarReady"];
                ProgressBar_file.Value = 100;

                TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.None; 

                Download_status_label.Content = (string)Application.Current.FindResource("m_Update_success");
                //Download_status_label.Foreground = (Brush)FindResource("ProgressBarGreen");
                Download_status_label.Visibility = Visibility.Visible;

                PlayButton.Background = (Brush)Resources["DefaultButtonColor"];
                PlayButton.IsEnabled = true;

                Play_button_label.Content = (string)Application.Current.FindResource("m_main_menu_play");

                Rotation_BeginStoryboard.Storyboard.Stop();
                RotationEnd_BeginStoryboard.Storyboard.Begin();

                App.GlobalTimer1sec.Elapsed -= GlobalTimer1sec_Elapsed;
            });
            Play();
        }

        private void Updater_NeedToDownload(Updater.updateStatus? status, System.Net.WebExceptionStatus exceptionStatus, string ms)
        {
            Dispatcher.Invoke(() =>
            {
                Play_button_label.Content = (string)Application.Current.FindResource("m_main_menu_play_downloading");
                ProgressBar_file.IsIndeterminate = false;
            });
        }

        private void Grid_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            UiGrid.Effect = new BlurEffect();
            settings_Frame.Visibility = Visibility.Visible;
            settings_Frame.Closed += SettingsFrame_Closed;
            UiGrid.IsEnabled = false;
        }

        private void SettingsFrame_Closed()
        {
            UiGrid.Effect = null;
            UiGrid.IsEnabled = true;
            settings_Frame.Visibility = Visibility.Hidden;
        }

        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Authentication.LogOut();
        }

        private void Tray_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}
