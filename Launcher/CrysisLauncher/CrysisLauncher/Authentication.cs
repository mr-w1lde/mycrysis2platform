﻿/*************************************************************************
Copyright (C), MYCRYSIS.ONLINE TEAM, 2019, support@mycrysis.online.
-------------------------------------------------------------------------
History:
- Created by Denis Beloedoff
*************************************************************************/
using System;
using System.Windows;

namespace CrysisLauncher
{
    static class Authentication
    {
        public static bool IsLoggedIn { get => CrysisLauncher.Properties.Settings.Default.LoggedIn; }

        public struct AuthData
        {
            public string Token;
            public string Nick;
            public string Login;
            public string Password;
            public int? ID;
            public bool Remember;
        }

        public static AuthData? currentAuthData = null;

        public enum EAuthenticationSuccessCode
        {
            eOk = 0, ePasswordInvalid, eInternalError
        }

        public delegate void AuthenticationEvents(EAuthenticationSuccessCode type);

        public static event AuthenticationEvents Succses;
        public static event AuthenticationEvents Fail;

        public static bool TryToLoggIn(string login, string password, bool remember)
        {
            MySQLclient.StateChanged += MySQLconnection_StateChanged;
            MySQLclient.ConnectToServer();

            if (MySQLclient.MysqlState == MySQLclient.EMysqlState.eOpen)
            {
                currentAuthData = MySQLclient.Login(login, password);

                if (currentAuthData == null)
                {
                    Fail?.Invoke(EAuthenticationSuccessCode.ePasswordInvalid);
                    MySQLclient.CloseConnection();
                    return false;
                }
                else
                {
                    Succses?.Invoke(EAuthenticationSuccessCode.eOk);

                    CrysisLauncher.Properties.Settings.Default.Username = currentAuthData.Value.Login;
                    CrysisLauncher.Properties.Settings.Default.LoggedIn = remember;
                    if (remember)
                    {
                        CrysisLauncher.Properties.Settings.Default.Password = currentAuthData.Value.Password;
                    }
                    CrysisLauncher.Properties.Settings.Default.Save();



                    MySQLclient.CloseConnection();
                    return true;
                }
            }
            else
            {
                MySQLclient.CloseConnection();
                return false;
            }
        }

        public static void LogOut()
        {
            CrysisLauncher.Properties.Settings.Default.Username = "";
            CrysisLauncher.Properties.Settings.Default.Password = "";
            CrysisLauncher.Properties.Settings.Default.LoggedIn = false;
            CrysisLauncher.Properties.Settings.Default.Save();
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        private static void MySQLconnection_StateChanged(MySQLclient.EMysqlState token)
        {
            switch (token)
            {
                case MySQLclient.EMysqlState.eTimeOut:
                case MySQLclient.EMysqlState.eSqlException:
                case MySQLclient.EMysqlState.eBroken:
                case MySQLclient.EMysqlState.eExecuting:
                case MySQLclient.EMysqlState.eServerUnavaible:
                    Fail?.Invoke(EAuthenticationSuccessCode.eInternalError);
                break;
            }
        }
    }
}
