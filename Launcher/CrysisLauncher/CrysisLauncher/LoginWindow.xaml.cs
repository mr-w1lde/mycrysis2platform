﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace CrysisLauncher
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        bool success = false;

        System.Windows.Media.Brush RedBrush = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.DarkRed);
        System.Windows.Media.Brush DefalutBrush;

        public LoginWindow()
        {
            InitializeComponent();
            LoginBox.Text = CrysisLauncher.Properties.Settings.Default.Username;
            PassBox.Password = CrysisLauncher.Properties.Settings.Default.Password;
            RememberMe.IsChecked = CrysisLauncher.Properties.Settings.Default.LoggedIn;
            DefalutBrush = LoginBox.BorderBrush;
            LoginBox.Focus();
        }

        public bool TryToAutologin()
        {

            Authentication.Succses += onSuccess;
            Authentication.Fail += onFail;
            grid_with_login_controls.IsEnabled = false;
            bool temp = Authentication.TryToLoggIn(CrysisLauncher.Properties.Settings.Default.Username, CrysisLauncher.Properties.Settings.Default.Password, true);
            grid_with_login_controls.IsEnabled = true;
            return temp;
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://mycrysis.online");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (!success)
                Environment.Exit(0);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProcessLogin();
        }

        private void ProcessLogin()
        {
            ErrorLabel.Visibility = Visibility.Hidden;
            LoginBox.BorderBrush = DefalutBrush;
            PassBox.BorderBrush = DefalutBrush;
            this.UpdateLayout();
            if (string.IsNullOrEmpty(LoginBox.Text))
            {
                ErrorLabel.Visibility = Visibility.Visible;
                ErrorLabel.Content = (string)Application.Current.FindResource("loginbox_Error_EmptyLogin");
                LoginBox.BorderBrush = RedBrush;
                LoginBox.Focus();
            }
            else
           if (string.IsNullOrEmpty(PassBox.Password))
            {
                ErrorLabel.Visibility = Visibility.Visible;
                ErrorLabel.Content = (string)Application.Current.FindResource("loginbox_Error_EmptyPass");
                PassBox.BorderBrush = RedBrush;
                PassBox.Focus();
            }
            else
            {
                Authentication.Succses += onSuccess;
                Authentication.Fail += onFail;
                grid_with_login_controls.IsEnabled = false;
                StartAnim();
                AsyncTryToLogIn();
            }
        }

        private void StartAnim()
        {
            Storyboard sb = this.FindResource("Blink") as Storyboard;
            sb.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard.SetTarget(sb, this.label_LogIn);
            sb.Begin();
        }

        private async void AsyncTryToLogIn()
        {
            string pass = PassBox.Password;
            string login = LoginBox.Text;
            bool remember = RememberMe.IsChecked.Value;
            await Task.Run(() => Authentication.TryToLoggIn(pass, login, remember));
        }

        public static void LogIn(object e)
        {
            Authentication.AuthData data = (Authentication.AuthData)e;
            Authentication.TryToLoggIn(data.Login, data.Password, data.Remember);
        }

        private void onSuccess(Authentication.EAuthenticationSuccessCode e)
        {
            Dispatcher.Invoke(() =>
            {
                if (e == Authentication.EAuthenticationSuccessCode.eOk)
                {
                    success = true;
                    this.Close();
                    CrysisLauncher.Properties.Settings.Default.LoggedIn = true;
                }
                grid_with_login_controls.IsEnabled = true;
                StopAnim();
            });
        }

        private void StopAnim()
        {
            Storyboard sb = this.FindResource("Blink") as Storyboard;
            Storyboard.SetTarget(sb, this.label_LogIn);
            sb.Stop();
        }

        private void onFail(Authentication.EAuthenticationSuccessCode e)
        {
            Dispatcher.Invoke(() =>
            {
                switch (e)
            {
                case Authentication.EAuthenticationSuccessCode.ePasswordInvalid:
                    ErrorLabel.Content = Application.Current.FindResource("loginbox_Error_WrongPassOrLogin");
                    break;
                case Authentication.EAuthenticationSuccessCode.eInternalError:
                    break;
                default:
                    break;
            }
            switch (MySQLclient.MysqlState)
            {
                case MySQLclient.EMysqlState.eTimeOut:
                    ErrorLabel.Content = Application.Current.FindResource("loginbox_Error_ServerTimeout");
                    break;
                case MySQLclient.EMysqlState.eSqlException:
                    ErrorLabel.Content = Application.Current.FindResource("UnknownError") + " 1";
                    break;
                case MySQLclient.EMysqlState.eBroken:
                    ErrorLabel.Content = Application.Current.FindResource("loginbox_Error_ServerBroken");
                    break;
                case MySQLclient.EMysqlState.eServerUnavaible:
                    ErrorLabel.Content = Application.Current.FindResource("loginbox_Error_ServerUnavailable");
                    break;
                default:
                    break;
            }
            ErrorLabel.Visibility = Visibility.Visible;
                grid_with_login_controls.IsEnabled = true;

                StopAnim();
            });
        }

        private void LoginBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PassBox.Focus();
            }
        }

        private void PassBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ProcessLogin();
            }
        }
    }
}
