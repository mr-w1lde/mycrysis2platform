﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CrysisProto;

using Google.Protobuf;
using Google;

using System.IO;

namespace CryprotoReader
{
    class Program
    {
        static void Main(string[] args)
        {
            CryOnlineAttributes onlineAttributes = new CryOnlineAttributes();

            var readData = File.OpenRead(@"X:\UProfiles\xui_sosy.cryproto");

            onlineAttributes = ReadMessage<CryOnlineAttributes>(readData);

            Console.Write(onlineAttributes.ToString());

            Console.ReadLine();
        }

        public static T ReadMessage<T>(Stream stream) where T : IMessage, new()
        {
            T message = new T();
            message.MergeFrom(stream);
            return message;
        }
    }
}
