﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace DedicatedGUID
{
    class GeneratorDedicatedJson
    {
        public String ServerTitle { get; set; }
        public String DedicatedGUID { get; set; }

        public GeneratorDedicatedJson(String serverName)
        {
            ServerTitle = serverName;
            GenerateGuid();
            SaveToJson();
        }

        private void GenerateGuid()
        {
            Guid id = Guid.NewGuid();
            DedicatedGUID = id.ToString().ToUpper();
        }

        private void SaveToJson()
        {
            string data = JsonConvert.SerializeObject(this);

            FileStream file = null;

            if (!File.Exists("GUIDSetup.json"))
                file = File.Create("GUIDSetup.json");
            else
                file = File.Open("GUIDSetup.json", FileMode.Open, FileAccess.ReadWrite);

            file.SetLength(0);


            file.Write(Encoding.ASCII.GetBytes(data), 0, data.Length);
            file.Close();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\t\t\t\t\t\tGUID Dedicated Server Generator\n");
            Console.WriteLine("Write your server name: ");


            String serverName = Console.ReadLine();

            Console.WriteLine("\nGenerating GUIDSetup.json ... ");
            new GeneratorDedicatedJson(serverName);

            Console.WriteLine("\nDone.");

            Console.ReadLine();
        }
    }
}
