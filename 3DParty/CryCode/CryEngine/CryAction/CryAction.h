/*************************************************************************
  Crytek Source File.
  Copyright (C), Crytek Studios, 2001-2004.
 -------------------------------------------------------------------------
	$Id$
	$DateTime$
	Description:	Implementation of the IGameFramework interface. CCryAction
								provides a generic game framework for action based games
								such as 1st and 3rd person shooters.
  
 -------------------------------------------------------------------------
  History:
  - 20:7:2004   10:51 : Created by Marco Koegler
	- 3:8:2004		11:11 : Taken-over by Marcio Martins

*************************************************************************/
#ifndef __CRYACTION_H__
#define __CRYACTION_H__

#if _MSC_VER > 1000
#	pragma once
#endif


#include <ISystem.h>
#include <ICmdLine.h>
#include <IAgent.h>

#include "IGameFramework.h"
#include "ICryPak.h"
#include "ISaveGame.h"
#include "ITestSystem.h"

struct IFlowSystem;
struct IGameTokenSystem;
struct IEffectSystem;
struct IForceFeedbackSystem;

class CAINetworkDebugRenderer;
class CAIVisualDebugRenderer;
class CGameRulesSystem;
class CScriptBind_Action;
class CScriptBind_ActorSystem;
class CScriptBind_ItemSystem;
class CScriptBind_ActionMapManager;
class CScriptBind_Network;
class CScriptBind_FlowSystem;
class CScriptBind_VehicleSystem;
class CScriptBind_Vehicle;
class CScriptBind_VehicleSeat;
class CScriptBind_Inventory;
class CScriptBind_DialogSystem;
class CScriptBind_MaterialEffects;
class CScriptBind_IKInteractions;
class CScriptBind_UIAction;

class CFlowSystem;
class CDevMode;
class CTimeDemoRecorder;
class CGameQueryListener;
class CScriptRMI;
class CAnimationGraphManager;
class CGameSerialize;
class CMaterialEffects; 
class CMaterialEffectsCVars;
class CGameObjectSystem;
class CActionMapManager;
class CActionGame;
class CActorSystem;
class CallbackTimer;
class CGameClientNub;
class CGameContext;
class CGameServerNub;
class CItemSystem;
class CLevelSystem;
class CUIDraw;
class CVehicleSystem;
class CViewSystem;
class CGameplayRecorder;
class CPersistantDebug;
class CPlayerProfileManager;
class CDialogSystem;
class CSubtitleManager;
class CDownloadTask;
class CGameplayAnalyst;
class CTimeOfDayScheduler;
class CNetworkCVars;
class CCryActionCVars;
class CGameStatsConfig;
class CCombatLog;
class CSignalTimer;
class CRangeSignaling;
class CVisualLog;
class CAIProxy;
class CommunicationVoiceLibrary;
class CIKTargetSystem;
class CIKInteractionManager;
class CAIProxyManager;
class CForceFeedBackSystem;
class CCryActionPhysicQueues;
class CNetworkStallTickerThread;
class CFlashUIActionEvents;

class CCharacterPartsManager;

struct IRenderNode;


class CTweakMenuController;
class CSharedParamsManager;
struct ICooperativeAnimationManager;
struct IGameSessionHandler;

struct IAnimationGraphState;
struct IRealtimeRemoteUpdate;
struct ISerializeHelper;

class CCryAction :
	public IGameFramework
{

public:
	CCryAction();
	virtual ~CCryAction();

	// IGameFramework
	
	virtual void RegisterFactory(const char *name, IActorCreator * pCreator, bool isAI);
	virtual void RegisterFactory(const char *name, IItemCreator * pCreator, bool isAI);
	virtual void RegisterFactory(const char *name, IVehicleCreator * pCreator, bool isAI);
	virtual void RegisterFactory(const char *name, IGameObjectExtensionCreator * pCreator, bool isAI );
	virtual void RegisterFactory(const char *name, IAnimationStateNodeFactory *(*func)(), bool isAI );
	virtual void RegisterFactory(const char *name, ISaveGame *(*func)(), bool);
	virtual void RegisterFactory(const char *name, ILoadGame *(*func)(), bool);

	VIRTUAL bool Init(SSystemInitParams &startupParams);
	VIRTUAL void InitGameType(bool multiplayer, bool fromInit);
	VIRTUAL bool CompleteInit();
	VIRTUAL void Shutdown();
	VIRTUAL bool PreUpdate(bool haveFocus, unsigned int updateFlags);
	VIRTUAL void PostUpdate(bool haveFocus, unsigned int updateFlags);
	VIRTUAL void Reset(bool clients);

	VIRTUAL void PauseGame(bool pause, bool force, unsigned int nFadeOutInMS=0); // 15
	VIRTUAL bool IsGamePaused();
	VIRTUAL bool IsGameStarted();

	VIRTUAL bool IsLevelPrecachingDone() const;
	VIRTUAL void SetLevelPrecachingDone(bool bValue);

	VIRTUAL void *GetAimatedCharacter();
	VIRTUAL ILanQueryListener *GetILanQueryListener();
	VIRTUAL IUIDraw *GetIUIDraw();	
	VIRTUAL IGameObjectSystem *GetIGameObjectSystem();
	VIRTUAL ILevelSystem *GetILevelSystem();
	VIRTUAL IActorSystem *GetIActorSystem();
	VIRTUAL IItemSystem *GetIItemSystem();
	VIRTUAL IBreakReplicator *GetIBreakReplicator();
	VIRTUAL IActionMapManager *GetIActionMapManager();
	VIRTUAL IViewSystem *GetIViewSystem();
	VIRTUAL IGameplayRecorder *GetIGameplayRecorder(); // 30
	VIRTUAL IVehicleSystem *GetIVehicleSystem();
	VIRTUAL IGameRulesSystem *GetIGameRulesSystem();
	VIRTUAL IFlowSystem *GetIFlowSystem();
	VIRTUAL IGameTokenSystem *GetIGameTokenSystem();
	VIRTUAL IEffectSystem *GetIEffectSystem();
	VIRTUAL IMaterialEffects *GetIMaterialEffects();
	VIRTUAL IDialogSystem *GetIDialogSystem();
	VIRTUAL IPlayerProfileManager *GetIPlayerProfileManager();
	VIRTUAL void GetDerbisMgr(void);
	VIRTUAL ISubtitleManager *GetISubtitleManager();
	VIRTUAL void GetCharacterParsMgr(void);
	VIRTUAL void CreateIFaceGen(void);
	VIRTUAL ITweakMenuController* CreateITweakMenuController();
	VIRTUAL IRealtimeRemoteUpdate * GetIRealTimeRemoteUpdate();
	VIRTUAL IGameStatistics* GetIGameStatistics();
	VIRTUAL IVisualLog *GetIVisualLog();
	VIRTUAL ICombatLog *GetICombatLog();
	VIRTUAL ICooperativeAnimationManager* GetICooperativeAnimationManager();
	VIRTUAL ICheckpointSystem* GetICheckpointSystem();
	VIRTUAL IForceFeedbackSystem* GetIForceFeedbackSystem() const;
	VIRTUAL ICommunicationVoiceLibrary* GetICommunicationVoiceLibrary() const;
	VIRTUAL IGameSessionHandler* GetIGameSessionHandler();
	VIRTUAL ISharedParamsManager *GetISharedParamsManager();
	VIRTUAL bool StartGameContext( const SGameStartParams * pGameStartParams );
	VIRTUAL bool ChangeGameContext( const SGameContextParams * pGameContextParams );
	VIRTUAL void EndGameContext();
	VIRTUAL bool StartedGameContext() const;
	VIRTUAL bool StartingGameContext() const;
	VIRTUAL void SetGameSessionHandler(IGameSessionHandler* pSessionHandler);
	VIRTUAL bool BlockingSpawnPlayer();
	VIRTUAL void FlushBreakableObjects();  // defined in ActionGame.cpp
	VIRTUAL void ResetBrokenGameObjects();
	VIRTUAL void CloneBrokenObjectsAndRevertToStateAtTime(int32 iFirstBreakEventIndex, uint16 * pBreakEventIndices, int32& iNumBreakEvents, IRenderNode** outClonedNodes, int32& iNumClonedNodes, SRenderNodeCloneLookup& renderNodeLookup);
	VIRTUAL void ApplySingleProceduralBreakFromEventIndex(uint16 uBreakEventIndex, const SRenderNodeCloneLookup& renderNodeLookup);
	VIRTUAL void UnhideBrokenObjectsByIndex( uint16 * ObjectIndicies, int32 iNumObjectIndices );
	VIRTUAL void InitEditor(IGameToEditorInterface* pGameToEditor);
	VIRTUAL void SetEditorLevel(const char *levelName, const char *levelFolder);
	VIRTUAL void GetEditorLevel(char **levelName, char **levelFolder);
	VIRTUAL void BeginLanQuery();
	VIRTUAL void EndCurrentQuery();
	VIRTUAL IActor * GetClientActor() const;
	VIRTUAL EntityId GetClientActorId() const;
	VIRTUAL INetChannel * GetClientChannel() const; // 70
	VIRTUAL void DelegateAuthority(EntityId entityId, uint16 channelId);
	VIRTUAL CTimeValue GetServerTime();
	VIRTUAL uint16 GetGameChannelId(INetChannel *pNetChannel);
	VIRTUAL bool IsChannelOnHold(uint16 channelId);
	VIRTUAL INetChannel *GetNetChannel(uint16 channelId);
	VIRTUAL IGameObject * GetGameObject(EntityId id);
	VIRTUAL bool GetNetworkSafeClassId(uint16 &id, const char *className);
	VIRTUAL bool GetNetworkSafeClassName(char *className, size_t maxn, uint16 id);
	VIRTUAL IGameObjectExtension * QueryGameObjectExtension( EntityId id, const char * name);
	VIRTUAL bool SaveGame( const char * path, bool bQuick = false, bool bForceImmediate=false, ESaveGameReason reason = eSGR_QuickSave, bool ignoreDelay = false, const char* checkpointName = NULL);
	VIRTUAL bool LoadGame( const char * path, bool quick = false, bool ignoreDelay = false);
	VIRTUAL void ScheduleEndLevelNow(const char* nextLevel);
	VIRTUAL void OnEditorSetGameMode( int iMode );
	VIRTUAL bool IsEditing();
	VIRTUAL bool IsInLevelLoad(void);
	VIRTUAL bool IsLoadingSaveGame(void);
	VIRTUAL bool IsInTimeDemo(void);
	VIRTUAL void AllowSave(bool bAllow = true);
	VIRTUAL void AllowLoad(bool bAllow = true);
	VIRTUAL bool CanSave();
	VIRTUAL bool CanLoad();
	VIRTUAL ISerializeHelper* GetSerializeHelper() const;
	VIRTUAL bool CanCheat();
	VIRTUAL const char* GetLevelName(void);
	VIRTUAL const char * GetAbsLevelPath(char*const pPath, const uint32 cPathMaxLen) = 0;
	VIRTUAL IPersistantDebug * GetIPersistantDebug();
	VIRTUAL IGameStatsConfig* GetIGameStatsConfig();
	VIRTUAL IAnimationGraphState * GetMusicGraphState();
	VIRTUAL IMusicLogic * GetMusicLogic();
	VIRTUAL void	AddBreakEventListener(IBreakEventListener * pListener);
	VIRTUAL void	RemoveBreakEventListener(IBreakEventListener * pListener);
	VIRTUAL void RegisterListener(IGameFrameworkListener *pGameFrameworkListener, const char *name, EFRAMEWORKLISTENERPRIORITY eFrameworkListenerPriority);
	VIRTUAL void UnregisterListener(IGameFrameworkListener *pGameFrameworkListener);
	VIRTUAL INetNub * GetServerNetNub();
	VIRTUAL INetNub * GetClientNetNub();
	VIRTUAL void SetGameGUID( const char * gameGUID);
	VIRTUAL const char* GetGameGUID();
	VIRTUAL INetContext* GetNetContext();
	VIRTUAL void GetMemoryUsage(ICrySizer *pSizer) const;
	VIRTUAL void EnableVoiceRecording(const bool enable);
	VIRTUAL void MutePlayerById(EntityId mutePlayer);
	VIRTUAL IDebugHistoryManager* CreateDebugHistoryManager();
	VIRTUAL void DumpMemInfo(const char* format, ...) PRINTF_PARAMS(2, 3);
	VIRTUAL bool IsVoiceRecordingEnabled();
	VIRTUAL bool IsImmersiveMPEnabled();
	VIRTUAL void ExecuteCommandNextFrame(const char* cmd);
	VIRTUAL const char* GetNextFrameCommand() const;
	VIRTUAL void ClearNextFrameCommand();
    VIRTUAL void ShowPageInBrowser(const char* URL);
	VIRTUAL bool StartProcess(const char* cmd_line);
    VIRTUAL bool SaveServerConfig(const char* path);
	VIRTUAL void PrefetchLevelAssets( const bool bEnforceAll );
	VIRTUAL void ReleaseGameStats();
	VIRTUAL void OnBreakageSpawnedEntity(IEntity* pEntity, IPhysicalEntity* pPhysEntity, IPhysicalEntity* pSrcPhysEntity);
	VIRTUAL bool IsGameSession(CrySessionHandle sessionHandle);
	//Add 3 Funcs... 
	//AddTimer()
	//AddTimer()
	//GetPreUpdateTickets()
	//
};

#endif //__CRYACTION_H__
