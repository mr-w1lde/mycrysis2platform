/*************************************************************************
  Crytek Source File.
  Copyright (C), Crytek Studios, 2001-2004.
 -------------------------------------------------------------------------
	$Id$
	$DateTime$
  Description:	This is the interface which the launcher.exe will interact
								with to start the game framework. For an implementation of
								this interface refer to CryAction.
  
 -------------------------------------------------------------------------
  History:
  - 20:7:2004   10:34 : Created by Marco Koegler
	- 3:8:2004		11:29 : Taken-over by M�rcio Martins

*************************************************************************/
#include DEVIRTUALIZE_HEADER_FIX(IGameFramework.h)

#ifndef __IGAMEFRAMEWORK_H__
#define __IGAMEFRAMEWORK_H__

#if _MSC_VER > 1000
#	pragma once
#endif

#include "IGameStartup.h"
#include "Cry_Color.h"
#include "TimeValue.h"

struct pe_explosion;
struct IPhysicalEntity;
struct EventPhysRemoveEntityParts;

struct IVisualLog;
struct ICombatLog;
struct IAIActorProxy;
struct ICooperativeAnimationManager;
struct IGameSessionHandler;
struct IRealtimeRemoteUpdate;
struct IForceFeedbackSystem;
struct ICommunicationVoiceLibrary;

struct ISerializeHelper;

//Define to control the logging of breakability code
#define BREAK_LOGGING 0

#if BREAK_LOGGING
#define BreakLogAlways CryLogAlways
#else
#define BreakLogAlways(...) ((void)0)
#endif

// Summary
//   Generic factory creation
// Description
//   This macro is used to register new game object extension classes. 
#define REGISTER_FACTORY(host, name, impl, isAI)												\
	(host)->RegisterFactory((name), (impl *)0, (isAI), (impl *)0)								\


#define DECLARE_GAMEOBJECT_FACTORY(impl)																	\
public:																																		\
	virtual void RegisterFactory(const char *name, impl *(*)(), bool isAI) = 0;				\
	template <class T> void RegisterFactory(const char *name, impl *, bool isAI, T *)	\
	{																																				\
		struct Factory																												\
		{																																			\
			static impl *Create()																								\
			{																																		\
				return new T();																										\
			}																																		\
		};																																		\
		RegisterFactory(name, Factory::Create, isAI);																\
	}

// game object extensions need more information than the generic interface can provide
struct IGameObjectExtension;

struct IGameObjectExtensionCreatorBase
{
	virtual ~IGameObjectExtensionCreatorBase(){}
	virtual IGameObjectExtension * Create() = 0;
	virtual void GetGameObjectExtensionRMIData( void ** ppRMI, size_t * nCount ) = 0;

	void GetMemoryUsage(ICrySizer *pSizer) const { /*LATER*/ }
};

#define DECLARE_GAMEOBJECTEXTENSION_FACTORY(name) \
	struct I##name##Creator : public IGameObjectExtensionCreatorBase \
	{ \
	}; \
	template <class T> \
	struct C##name##Creator : public I##name##Creator \
	{ \
		IGameObjectExtension * Create() \
		{ \
			return (IGameObjectExtension *)new T(); \
		} \
		void GetGameObjectExtensionRMIData( void ** ppRMI, size_t * nCount ) \
		{ \
			T::GetGameObjectExtensionRMIData( ppRMI, nCount ); \
		} \
	}; \
	virtual void RegisterFactory(const char *name, I##name##Creator*, bool isAI) = 0; \
	template <class T> void RegisterFactory(const char *name, I##name *, bool isAI, T *) \
	{ \
		static C##name##Creator<T> creator; \
		RegisterFactory(name, &creator, isAI); \
	}

struct ISystem;
struct IUIDraw;
struct ILanQueryListener;
struct IActor;
struct IActorSystem;
struct IItem;
struct IGameRules;
struct IWeapon;
struct IItemSystem;
struct ILevelSystem;
struct IActionMapManager;
struct IGameChannel;
struct IViewSystem;
struct IVehicle;
struct IVehicleSystem;
struct IGameRulesSystem;
struct IFlowSystem;
struct IGameTokenSystem;
struct IEffectSystem;
struct IGameObject;
struct IGameObjectExtension;
struct IGameObjectSystem;
struct IGameplayRecorder;
struct IAnimationStateNodeFactory;
struct ISaveGame;
struct ILoadGame;
struct IGameObject;
struct IMaterialEffects;
struct INetChannel;
struct IPlayerProfileManager;
struct IMusicLogic;
struct IAnimationGraphState;
struct INetNub;
struct ISaveGame;
struct ILoadGame;
struct IDebugHistoryManager;
struct IDebrisMgr;
struct ISubtitleManager;
struct IDialogSystem;
struct ICharacterPartsManager;
struct IGameStatistics;
struct IFaceGen;
struct ITweakMenuController;
struct ICheckpointSystem;
struct IGameToEditorInterface;

struct IStatObj;

class ISharedParamsManager;

struct INeuralNet;
typedef _smart_ptr<INeuralNet> INeuralNetPtr;

enum EGameStartFlags
{
	eGSF_NoLevelLoading            = 0x0001,
	eGSF_Server                    = 0x0002,
	eGSF_Client                    = 0x0004,
	eGSF_NoDelayedStart            = 0x0008,
	eGSF_BlockingClientConnect     = 0x0010,
	eGSF_NoGameRules               = 0x0020,
	eGSF_LocalOnly                 = 0x0040,
	eGSF_NoQueries                 = 0x0080,
	eGSF_NoSpawnPlayer             = 0x0100,
	eGSF_BlockingMapLoad           = 0x0200,

	eGSF_DemoRecorder              = 0x0400,
	eGSF_DemoPlayback              = 0x0800,

	eGSF_ImmersiveMultiplayer      = 0x1000,
	eGSF_RequireController         = 0x2000,
	eGSF_RequireKeyboardMouse      = 0x4000,

	eGSF_HostMigrated							 = 0x8000,
	eGSF_NonBlockingConnect					= 0x10000,
};

enum ESaveGameReason
{
	eSGR_LevelStart,
	eSGR_FlowGraph,
	eSGR_Command,
	eSGR_QuickSave
};

static const EntityId LOCAL_PLAYER_ENTITY_ID = 0x7777u; // 30583 between static and dynamic EntityIDs

struct SGameContextParams
{
	const char * levelName;
	const char * gameRules;
	const char * demoRecorderFilename;
	const char * demoPlaybackFilename;

	SGameContextParams()
	{
		levelName = 0;
		gameRules = 0;
		demoRecorderFilename = 0;
		demoPlaybackFilename = 0;
	}
};

struct SGameStartParams
{
	// ip address/hostname of server to connect to - needed if bClient==true
	const char * hostname;
	// optional connection string for client
	const char * connectionString;
	// context parameters - needed if bServer==true
	const SGameContextParams * pContextParams;
	// a combination of EGameStartFlags - needed if bServer==true
	uint32 flags;
	// maximum players to allow to connect
	int32 maxPlayers;
	// the session handle if connecting via CryLobby
	CrySessionHandle	session;
	// UDP port to connect to
	uint16 port;
	
	SGameStartParams()
	{
		flags = 0;
		port = 0;
		hostname = 0;
		connectionString = 0;
		pContextParams = NULL;
		maxPlayers = 32;
		session = CrySessionInvalidHandle;
	}
};

struct SEntityTagParams
{
	EntityId entity;
	string text;
	float size;						// font size
	float visibleTime;		// seconds before starting fade, >= 0
	float fadeTime;				// seconds to fade over, >= 0
	float viewDistance;		// maximum distance of entity from camera to show tag
	string staticId;			// when nonempty string, display first for entity, and only most recent one (for continuous info like health display)
	int column;						// For multicolumn tag display (0 or 1 defaults to standard 1 column display)
	ColorF color;
	string tagContext;

	SEntityTagParams()	{ Init(); }
	SEntityTagParams(EntityId entity, const char *text)
	{
		Init();
		this->entity = entity;
		this->text = text ? text : "";
	}
	SEntityTagParams(EntityId entity, const char *text, float size, const ColorF& color, float duration)
	{
		Init();
		this->entity = entity;
		this->text = text ? text : "";
		this->size = size;
		this->color = color;
		this->fadeTime = duration;
	}

private:
	void Init()
	{
		entity = 0;
		text = "";
		size = 1.5f;
		visibleTime = 2.f;
		fadeTime = 1.f;
		viewDistance = 1000.f;
		staticId = "";
		column = 1;
		color = ColorF(1.f,1.f,1.f,1.f);
		tagContext = "";
	}
};

typedef uint32 THUDWarningId;
struct IGameWarningsListener
{
	virtual ~IGameWarningsListener(){}
	virtual bool OnWarningReturn(THUDWarningId id, const char* returnValue) { return true; }
	virtual void OnWarningRemoved(THUDWarningId id) {}
};

/////////////////////////////////
// SRenderNodeCloneLookup is used to associate original IRenderNodes (used in the game) with
//	cloned IRenderNodes (used for the [redacted]), to allow breaks to be played back.
struct SRenderNodeCloneLookup
{
	SRenderNodeCloneLookup()
	{
		pOriginalNodes	= NULL;
		pClonedNodes		= NULL;
		iNumPairs				= 0;
	}

	void UpdateStoragePointers(std::vector<IRenderNode*>& originalNodes,	std::vector<IRenderNode*>& clonedNodes)
	{
		pOriginalNodes	= originalNodes.empty() ? NULL : &(originalNodes[0]);
		pClonedNodes		= clonedNodes.empty() ? NULL : &(clonedNodes[0]);		
	}

	void AddNodePair(IRenderNode * originalNode, IRenderNode * clonedNode)
	{
		pOriginalNodes[iNumPairs] = originalNode;
		pClonedNodes[iNumPairs]		= clonedNode;
		iNumPairs++;
	}

	void Reset()
	{
		iNumPairs = 0;
		pOriginalNodes	= NULL;
		pClonedNodes		= NULL;		
	}
	
	IRenderNode ** pOriginalNodes;
	IRenderNode ** pClonedNodes;
	int iNumPairs;
};

// Provides an interface to game so game will be able to display numeric stats in user-friendly way.
UNIQUE_IFACE struct IGameStatsConfig
{
	virtual ~IGameStatsConfig(){}
	virtual int GetStatsVersion() = 0;
	virtual int GetCategoryMod(const char* cat) = 0;
	virtual const char* GetValueNameByCode(const char* cat, int id) = 0;
};

UNIQUE_IFACE struct IBreakReplicator
{
	virtual ~IBreakReplicator(){}
	virtual const EventPhysRemoveEntityParts * GetRemovePartEvents(int & iNumEvents) = 0;
};

UNIQUE_IFACE struct IPersistantDebug
{
	virtual ~IPersistantDebug(){}
	virtual void Begin( const char * name, bool clear ) = 0;
	virtual void AddSphere( const Vec3& pos, float radius, ColorF clr, float timeout ) = 0;
	virtual void AddDirection( const Vec3& pos, float radius, const Vec3& dir, ColorF clr, float timeout ) = 0;
	virtual void AddLine( const Vec3& pos1, const Vec3& pos2, ColorF clr, float timeout ) = 0;
	virtual void AddPlanarDisc( const Vec3& pos, float innerRadius, float outerRadius, ColorF clr, float timeout ) = 0;
	virtual void AddCone( const Vec3& pos, const Vec3& dir, float baseRadius, float height, ColorF clr, float timeout ) = 0;
	virtual void AddCylinder( const Vec3& pos, const Vec3& dir, float radius, float height, ColorF clr, float timeout ) = 0;
	virtual void Add2DText ( const char * text, float size, ColorF clr, float timeout ) = 0;
	virtual void AddText ( float x, float y, float size, ColorF clr, float timeout, const char * fmt, ... ) = 0;
	virtual void Add2DLine( float x1, float y1, float x2, float y2, ColorF clr, float timeout ) = 0;
	virtual void AddQuat( const Vec3& pos, const Quat& q, float r, ColorF clr, float timeout ) = 0;
	virtual void AddEntityTag(const SEntityTagParams& params, const char *tagContext="") = 0;
	virtual void ClearEntityTags(EntityId entityId) = 0;
	virtual void ClearStaticTag(EntityId entityId, const char *staticId) = 0;
	virtual void ClearTagContext(const char *tagContext) = 0; 
	virtual void ClearTagContext(const char *tagContext, EntityId entityId) = 0; 
	virtual void Reset() = 0;
};

// When you add stuff here, also update in CCryAction::Init .
enum EGameFrameworkEvent
{
	eGFE_PauseGame,
	eGFE_ResumeGame,
	eGFE_OnCollision,
	eGFE_OnPostStep,
	eGFE_OnStateChange,
	eGFE_ResetAnimationGraphs,
#ifndef GAME_IS_CRYSIS2
	eGFE_OnBreakable2d,
#endif
	eGFE_OnBecomeVisible,
	eGFE_PreFreeze,
	eGFE_PreShatter,
	eGFE_BecomeLocalPlayer,
	eGFE_DisablePhysics,
	eGFE_EnablePhysics,
};

// All events game should be aware of need to be added here.
enum EActionEvent
{
  eAE_channelCreated,
  eAE_channelDestroyed,
  eAE_connectFailed,
  eAE_connected,
  eAE_disconnected,
  eAE_clientDisconnected,
	// Map resetting.
	eAE_resetBegin,
	eAE_resetEnd,
	eAE_resetProgress,
	eAE_preSaveGame,  // m_value -> ESaveGameReason
	eAE_postSaveGame, // m_value -> ESaveGameReason, m_description: 0 (failed), != 0 (successful)
	eAE_inGame,

	eAE_serverName, // Started server.
	eAE_serverIp,		// Obtained server ip.
	eAE_earlyPreUpdate,  // Called from CryAction's PreUpdate loop after System has been updated, but before subsystems.
	eAE_demoRecorderCreated,
	eAE_mapCmdIssued,
	eAE_unloadLevel,
	eAE_postUnloadLevel,
	eAE_loadLevel,
};

struct SActionEvent
{
  SActionEvent(EActionEvent e, int val=0,const char* des = 0):
    m_event(e),
    m_value(val),
    m_description(des)
  {}
  EActionEvent  m_event;
  int           m_value;
  const char*   m_description;
};

// We must take care of order in which listeners are called.
// Priority order is from low to high.
// As an example, menu must follow hud as it must be drawn on top of the rest.
enum EFRAMEWORKLISTENERPRIORITY
{
	// Default priority should not be used unless you don't care about order (it will be called first)
	FRAMEWORKLISTENERPRIORITY_DEFAULT,

	// Add your order somewhere here if you need to be called between one of them
	FRAMEWORKLISTENERPRIORITY_GAME,
	FRAMEWORKLISTENERPRIORITY_HUD,
	FRAMEWORKLISTENERPRIORITY_MENU
};

struct IGameFrameworkListener
{
	virtual ~IGameFrameworkListener(){}
	virtual void OnPostUpdate(float fDeltaTime) = 0;
	virtual void OnSaveGame(ISaveGame* pSaveGame) = 0;
	virtual void OnLoadGame(ILoadGame* pLoadGame) = 0;
	virtual void OnLevelEnd(const char* nextLevel) = 0;
  virtual void OnActionEvent(const SActionEvent& event) = 0;
	virtual void OnPreRender() {}
};

struct IBreakEventListener
{
	virtual ~IBreakEventListener(){}
	virtual void OnBreakEvent(uint16 uBreakEventIndex) = 0;
	virtual void OnPartRemoveEvent(int32 iPartRemoveEventIndex) = 0;
	virtual void OnEntityDrawSlot(IEntity * pEntity, int32 slot, int32 flags) = 0;
	virtual void OnEntityChangeStatObj(IEntity *pEntity, int32 iBrokenObjectIndex, int32 slot, IStatObj * pOldStatObj, IStatObj * pNewStatObj) = 0;
	virtual void OnSetSubObjHideMask(IEntity *pEntity, int nSlot, uint64 nSubObjHideMask) = 0;
};

// Summary
//   Interface which exposes the CryAction subsystems
UNIQUE_IFACE struct  IGameFramework
{
	virtual ~IGameFramework(){}

	DECLARE_GAMEOBJECT_FACTORY(IAnimationStateNodeFactory);
	DECLARE_GAMEOBJECT_FACTORY(ISaveGame);
	DECLARE_GAMEOBJECT_FACTORY(ILoadGame);
	DECLARE_GAMEOBJECTEXTENSION_FACTORY(Actor);
	DECLARE_GAMEOBJECTEXTENSION_FACTORY(Item);
	DECLARE_GAMEOBJECTEXTENSION_FACTORY(Vehicle);
	DECLARE_GAMEOBJECTEXTENSION_FACTORY(GameObjectExtension);
	
	// Summary
	//   Entry function to the game framework
	// Description
	//   Entry function used to create a new instance of the game framework from 
	//   outside its own DLL.
	// Returns
	//   a new instance of the game framework
	typedef IGameFramework *(*TEntryFunction)();

	// Summary:
	//		Initialize CryENGINE with every system needed for a general action game.
	//		Independently of the success of this method, Shutdown must be called.
	// Arguments:
	//		startupParams - Pointer to SSystemInitParams structure containing system initialization setup!
	// Return Value:
	//		0 if something went wrong with initialization, non-zero otherwise.
	VIRTUAL bool Init(SSystemInitParams &startupParams) = 0;
	VIRTUAL void InitGameType(bool multiplayer, bool fromInit) = 0;
	VIRTUAL bool CompleteInit() = 0;
	VIRTUAL void Shutdown() = 0;
	VIRTUAL bool PreUpdate(bool haveFocus, unsigned int updateFlags) = 0;
	VIRTUAL void PostUpdate(bool haveFocus, unsigned int updateFlags) = 0;
	VIRTUAL void Reset(bool clients) = 0;

	VIRTUAL void PauseGame(bool pause, bool force, unsigned int nFadeOutInMS = 0) = 0;
	VIRTUAL bool IsGamePaused() = 0;
	VIRTUAL bool IsGameStarted() = 0;

	VIRTUAL bool IsLevelPrecachingDone() const = 0;
	VIRTUAL void SetLevelPrecachingDone(bool bValue) = 0;

	VIRTUAL void *GetAimatedCharacter() = 0;
	VIRTUAL ILanQueryListener *GetILanQueryListener() = 0;
	VIRTUAL IUIDraw *GetIUIDraw() = 0;
	VIRTUAL IGameObjectSystem *GetIGameObjectSystem() = 0;
	VIRTUAL ILevelSystem *GetILevelSystem() = 0;
	VIRTUAL IActorSystem *GetIActorSystem() = 0;
	VIRTUAL IItemSystem *GetIItemSystem() = 0;
	VIRTUAL IBreakReplicator *GetIBreakReplicator() = 0;
	VIRTUAL IActionMapManager *GetIActionMapManager() = 0;
	VIRTUAL IViewSystem *GetIViewSystem() = 0;
	VIRTUAL IGameplayRecorder *GetIGameplayRecorder() = 0;
	VIRTUAL IVehicleSystem *GetIVehicleSystem() = 0;
	VIRTUAL IGameRulesSystem *GetIGameRulesSystem() = 0;
	VIRTUAL IFlowSystem *GetIFlowSystem() = 0;
	VIRTUAL IGameTokenSystem *GetIGameTokenSystem() = 0;
	VIRTUAL IEffectSystem *GetIEffectSystem() = 0;
	VIRTUAL IMaterialEffects *GetIMaterialEffects() = 0;
	VIRTUAL IDialogSystem *GetIDialogSystem() = 0;
	VIRTUAL IPlayerProfileManager *GetIPlayerProfileManager() = 0;
	VIRTUAL void GetDerbisMgr(void) = 0;
	VIRTUAL ISubtitleManager *GetISubtitleManager() = 0;
	VIRTUAL void GetCharacterParsMgr(void) = 0;
	VIRTUAL void CreateIFaceGen(void) = 0;
	VIRTUAL ITweakMenuController* CreateITweakMenuController() = 0;
	VIRTUAL IRealtimeRemoteUpdate * GetIRealTimeRemoteUpdate() = 0;
	VIRTUAL IGameStatistics* GetIGameStatistics() = 0;
	VIRTUAL IVisualLog *GetIVisualLog() = 0;
	VIRTUAL ICombatLog *GetICombatLog() = 0;
	VIRTUAL ICooperativeAnimationManager* GetICooperativeAnimationManager() = 0;
	VIRTUAL ICheckpointSystem* GetICheckpointSystem() = 0;
	VIRTUAL IForceFeedbackSystem* GetIForceFeedbackSystem() const = 0;
	VIRTUAL ICommunicationVoiceLibrary* GetICommunicationVoiceLibrary() const = 0;
	VIRTUAL IGameSessionHandler* GetIGameSessionHandler() = 0;
	VIRTUAL ISharedParamsManager *GetISharedParamsManager() = 0;
	VIRTUAL bool StartGameContext(const SGameStartParams * pGameStartParams) = 0;
	VIRTUAL bool ChangeGameContext(const SGameContextParams * pGameContextParams) = 0;
	VIRTUAL void EndGameContext() = 0;
	VIRTUAL bool StartedGameContext() const = 0;
	VIRTUAL bool StartingGameContext() const = 0;
	VIRTUAL void SetGameSessionHandler(IGameSessionHandler* pSessionHandler) = 0;
	VIRTUAL bool BlockingSpawnPlayer() = 0;
	VIRTUAL void FlushBreakableObjects() = 0;
	VIRTUAL void ResetBrokenGameObjects() = 0;
	VIRTUAL void CloneBrokenObjectsAndRevertToStateAtTime(int32 iFirstBreakEventIndex, uint16 * pBreakEventIndices, int32& iNumBreakEvents, IRenderNode** outClonedNodes, int32& iNumClonedNodes, SRenderNodeCloneLookup& renderNodeLookup) = 0;
	VIRTUAL void ApplySingleProceduralBreakFromEventIndex(uint16 uBreakEventIndex, const SRenderNodeCloneLookup& renderNodeLookup) = 0;
	VIRTUAL void UnhideBrokenObjectsByIndex(uint16 * ObjectIndicies, int32 iNumObjectIndices) = 0;
	VIRTUAL void InitEditor(IGameToEditorInterface* pGameToEditor) = 0;
	VIRTUAL void SetEditorLevel(const char *levelName, const char *levelFolder) = 0;
	VIRTUAL void GetEditorLevel(char **levelName, char **levelFolder) = 0;
	VIRTUAL void BeginLanQuery() = 0;
	VIRTUAL void EndCurrentQuery() = 0;
	VIRTUAL IActor * GetClientActor() const = 0;
	VIRTUAL EntityId GetClientActorId() const = 0;
	VIRTUAL INetChannel * GetClientChannel() const = 0;
	VIRTUAL void DelegateAuthority(EntityId entityId, uint16 channelId) = 0;
	VIRTUAL CTimeValue GetServerTime() = 0;
	VIRTUAL uint16 GetGameChannelId(INetChannel *pNetChannel) = 0;
	VIRTUAL bool IsChannelOnHold(uint16 channelId) = 0;
	VIRTUAL INetChannel *GetNetChannel(uint16 channelId) = 0;
	VIRTUAL IGameObject * GetGameObject(EntityId id) = 0;
	VIRTUAL bool GetNetworkSafeClassId(uint16 &id, const char *className) = 0;
	VIRTUAL bool GetNetworkSafeClassName(char *className, size_t maxn, uint16 id) = 0;
	VIRTUAL IGameObjectExtension * QueryGameObjectExtension(EntityId id, const char * name) = 0;
	VIRTUAL bool SaveGame(const char * path, bool bQuick = false, bool bForceImmediate = false, ESaveGameReason reason = eSGR_QuickSave, bool ignoreDelay = false, const char* checkpointName = NULL) = 0;
	VIRTUAL bool LoadGame(const char * path, bool quick = false, bool ignoreDelay = false) = 0;
	VIRTUAL void ScheduleEndLevelNow(const char* nextLevel) = 0;
	VIRTUAL void OnEditorSetGameMode(int iMode) = 0;
	VIRTUAL bool IsEditing() = 0;
	VIRTUAL bool IsInLevelLoad(void) = 0;
	VIRTUAL bool IsLoadingSaveGame(void) = 0;
	VIRTUAL bool IsInTimeDemo(void) = 0;
	VIRTUAL void AllowSave(bool bAllow = true) = 0;
	VIRTUAL void AllowLoad(bool bAllow = true) = 0;
	VIRTUAL bool CanSave() = 0;
	VIRTUAL bool CanLoad() = 0;
	VIRTUAL ISerializeHelper* GetSerializeHelper() const = 0;
	VIRTUAL bool CanCheat() = 0;
	VIRTUAL const char* GetLevelName(void) = 0;
	VIRTUAL const char * GetAbsLevelPath(char*const pPath, const uint32 cPathMaxLen) = 0;
	VIRTUAL IPersistantDebug * GetIPersistantDebug() = 0;
	VIRTUAL IGameStatsConfig* GetIGameStatsConfig() = 0;
	VIRTUAL IAnimationGraphState * GetMusicGraphState() = 0;
	VIRTUAL IMusicLogic * GetMusicLogic() = 0;
	VIRTUAL void	AddBreakEventListener(IBreakEventListener * pListener) = 0;
	VIRTUAL void	RemoveBreakEventListener(IBreakEventListener * pListener) = 0;
	VIRTUAL void RegisterListener(IGameFrameworkListener *pGameFrameworkListener, const char *name, EFRAMEWORKLISTENERPRIORITY eFrameworkListenerPriority) = 0;
	VIRTUAL void UnregisterListener(IGameFrameworkListener *pGameFrameworkListener) = 0;
	VIRTUAL INetNub * GetServerNetNub() = 0;
	VIRTUAL INetNub * GetClientNetNub() = 0;
	VIRTUAL void SetGameGUID(const char * gameGUID) = 0;
	VIRTUAL const char* GetGameGUID() = 0;
	VIRTUAL INetContext* GetNetContext() = 0;
	VIRTUAL void GetMemoryUsage(ICrySizer *pSizer) const = 0;
	VIRTUAL void EnableVoiceRecording(const bool enable) = 0;
	VIRTUAL void MutePlayerById(EntityId mutePlayer) = 0;
	VIRTUAL IDebugHistoryManager* CreateDebugHistoryManager() = 0;
	VIRTUAL void DumpMemInfo(const char* format, ...) PRINTF_PARAMS(2, 3) = 0;
	VIRTUAL bool IsVoiceRecordingEnabled() = 0;
	VIRTUAL bool IsImmersiveMPEnabled() = 0;
	VIRTUAL void ExecuteCommandNextFrame(const char* cmd) = 0;
	VIRTUAL const char* GetNextFrameCommand() const = 0;
	VIRTUAL void ClearNextFrameCommand() = 0;
	VIRTUAL void ShowPageInBrowser(const char* URL) = 0;
	VIRTUAL bool StartProcess(const char* cmd_line) = 0;
	VIRTUAL bool SaveServerConfig(const char* path) = 0;
	VIRTUAL void PrefetchLevelAssets(const bool bEnforceAll) = 0;
	VIRTUAL void ReleaseGameStats() = 0;
	VIRTUAL void OnBreakageSpawnedEntity(IEntity* pEntity, IPhysicalEntity* pPhysEntity, IPhysicalEntity* pSrcPhysEntity) = 0;
	VIRTUAL bool IsGameSession(CrySessionHandle sessionHandle) = 0;
};

ILINE bool IsDemoPlayback()
{
	ISystem* pSystem = GetISystem();
	IGame* pGame = gEnv->pGame;
	IGameFramework* pFramework = pGame->GetIGameFramework();
	INetContext* pNetContext = pFramework->GetNetContext();
	return pNetContext ? pNetContext->IsDemoPlayback() : false;
}

#endif //__IGAMEFRAMEWORK_H__

