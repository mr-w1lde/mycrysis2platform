﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Md5Checker
{
    public struct hash
    {
        public string path;
        public string md5;

        public hash(string pathin, string md5in)
        {
            path = pathin;
            md5 = md5in;
        }

        public override string ToString()
        {
            return String.Format("{0,-45}:{1,45}", path, md5);
        }
        public string ToString(int lengh)
        {
            return String.Format("{0,-" + lengh + "}:{1,1}", path, md5);
        }
    }

    class Program
    {

        private const string PathArg = "--path";
        static string path;
        static string savePath = "0";
        static string[] searchpatern = { "*" };
        static bool gui = false;

        public static List<hash> hashes;
        public static List<string> pathsToIgnore = new List<string>();
        static readonly string[] MyArgs = { PathArg, "-?", "--help", "--pattern", "--savepath", "--gui","--ignorepaths" };

        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                CheckArgs(args);

            }
            if (path == null)
            {
                if (gui)
                {
                    path = GUIopen();
                }
                else
                {
                    Help();
                }

                if (path == null)
                    Environment.Exit(0);
            }
            Console.WriteLine("Coosen path: " + path);
            hashes = Md5.CheckMd5(path, searchpatern, hashes);
            if (savePath != "0")
            {
                SaveHases();
            }
            else
            {
                if (gui)
                {
                    savePath = GuiSave();
                    SaveHases();
                }
                else
                {
                    Help();
                }
            }
        }

        private static void SaveHases()
        {
            using (StreamWriter fileStream = new StreamWriter(savePath, false))
            {
                foreach (hash hash in hashes)
                {
                    if (hash.path.IndexOf(savePath.Substring(savePath.LastIndexOf('\\') + 1)) == -1) // if filename != checksumm out file 
                        fileStream.WriteLine(hash.ToString(Md5.maxLengh) + "\n");
                }
            }
        }

        static string GuiSave()
        {
            using
            (
                var fbd = new SaveFileDialog
                {
                    Filter = "txt (*.txt)|*.txt|md5 (*.md5)|*.md5",
                    Title = "Save checksum file",
                    FileName = "MD5-CHECKSUM.txt"
                }
            )
            {
                DialogResult result = fbd.ShowDialog();


                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    return fbd.FileName;
                }
            }
            return null;
        }

        static string GUIopen()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    return fbd.SelectedPath;
                }
            }
            return null;
        }

        static void CheckArgs(string[] args)
        {
            int parsedargsnum = 0;
            foreach (string myarg in MyArgs)
            {
                for (int i = 0; i < args.Length; i++)
                {

                    if (args[i] == myarg && (myarg == PathArg || myarg == "--pattern" || myarg == "--savepath" || myarg == "--ignorepaths"))
                    {
                        try
                        {
                            ArgFound(args[i], args[i + 1]);
                        }
                        catch (IndexOutOfRangeException)
                        {
                            Error("Error string after --path empty!");
                        }
                        parsedargsnum++;
                    }
                    else if (args[i] == myarg)
                    {
                        ArgFound(args[i]);
                        parsedargsnum++;
                    }
                }
            }
            if (parsedargsnum == 0)
            {
                Error("Invalid Args!");
            }
        }

        static void Error(string val)
        {
            Console.WriteLine(val + " \n");
            Help();
        }
        static void Help()
        {
            Console.WriteLine("Md5 checksums lister Usage: Md5Checker.exe [--path <path>] [--help/-?] [--pattern <filespattern>] [--savepath <path>] [--gui] \n" +
                                " Example: Md5Checker.exe --path C:\\fakeroot --savepath C:\\saves \n Md5Checker.exe --help");
        }

        static void ArgFound(string arg)
        {
            Console.WriteLine(arg);
            if (arg == "--gui")
                gui = true;
        }
        static void ArgFound(string arg, string value)
        {
            if (arg == PathArg)
                path = value;
            if (arg == "--pattern")
                searchpatern = value.Split(',');
            if (arg == "--savepath")
                savePath = value;
            if (arg == "--ignorepaths")
                pathsToIgnore.AddRange(value.Split(','));

        }
    }

    public static class Md5
    {
        static MD5 md5 = MD5.Create();
        public static int maxLengh = 0;
        public static List<hash> CheckMd5(string path, string[] searchpattern, List<hash> hashes)
        {
            List<hash> tmp = new List<hash>();
            DirectoryInfo directoryInfo = null;
            try
            {
                directoryInfo = new DirectoryInfo(path);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("Dir not found: " + path);
                Environment.Exit(0);
            }
            List<FileInfo> fileInfos = new List<FileInfo>();
            foreach (string pattern in searchpattern)
            {
                try
                {
                    fileInfos.AddRange(directoryInfo.GetFiles(pattern, SearchOption.AllDirectories));
                }
                catch (DirectoryNotFoundException)
                {
                    Console.WriteLine("Dir not found: " + path);
                    Environment.Exit(0);
                }
            }
            foreach (FileInfo file in fileInfos)
            {
                bool ignore = false;
                foreach(string s in Program.pathsToIgnore)
                {
                    if (file.Directory.ToString().Contains(s))
                    {
                        ignore = true;
                    }
                }
                
                if(!ignore)
                tmp.Add(Md5Summ(file.FullName.Replace(path, ""), file.FullName));
            }
            for (int i = 0; i < fileInfos.ToArray().Length; i++)
            {
                if (fileInfos[i].FullName.Length > maxLengh)
                {
                    maxLengh = fileInfos[i].FullName.Replace(path, "").Length;
                }
            }
            return tmp;

        }

        static hash Md5Summ(string fileRelPath, string path)
        {

            string hash = "";
            using (var md5 = MD5.Create())
            {
                try
                {
                    using (var stream = File.OpenRead(path))
                    {
                        var hashtmp = md5.ComputeHash(stream);
                        hash = BitConverter.ToString(hashtmp).Replace("-", "").ToLowerInvariant();
                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            Console.WriteLine(fileRelPath + ": \n --" + hash);

            return new hash(fileRelPath, hash);
        }
    }
}
